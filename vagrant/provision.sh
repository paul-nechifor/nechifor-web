#!/bin/bash

set -euo pipefail
set -x

main() {
    apt update -y
    # apt upgrade -y
    apt install apt-transport-https ca-certificates curl software-properties-common -y

    if [[ ! -e /usr/share/keyrings/docker-archive-keyring.gpg ]]; then
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg |
        gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi

    if [[ ! -e /etc/apt/sources.list.d/docker.list ]]; then
        echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    fi

    apt update -y
    apt-cache policy docker-ce
    apt install docker-ce -y

    docker network create nechifor-web-network &> /dev/null || true

    cat > /etc/systemd/system/nechifor-net.service <<END
[Unit]
Description=nechifor-net
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
RuntimeMaxSec=86400
ExecStartPre=-/usr/bin/docker stop nechifor-net
ExecStartPre=-/usr/bin/docker rm nechifor-net
ExecStart=/usr/bin/docker run -p 80:80 --name nechifor-net --rm --network=nechifor-web-network nechifor-web
ExecStop=-/usr/bin/docker stop nechifor-net

[Install]
WantedBy=default.target
END
    systemctl enable nechifor-net

    cat > /etc/systemd/system/multilatex.service <<END
[Unit]
Description=multilatex
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
RuntimeMaxSec=1800
ExecStartPre=-/usr/bin/docker stop multilatex
ExecStartPre=-/usr/bin/docker rm multilatex
ExecStart=/usr/bin/docker run -p 3000:3000 --name multilatex --rm --network=nechifor-web-network multilatex
ExecStop=-/usr/bin/docker stop multilatex

[Install]
WantedBy=default.target
END
    systemctl enable multilatex
}

main "$@"
