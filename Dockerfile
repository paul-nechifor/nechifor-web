FROM ubuntu:22.04
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London
ARG NECHIFORNETVERSION

RUN apt-get update && \
  apt-get install -y \
  curl \
  gnupg \
  nginx \
  php-fpm \
  php-sqlite3 \
  supervisor \
  -y && \
  curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee /usr/share/keyrings/nodesource.gpg >/dev/null && \
  echo 'deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x jammy main' > /etc/apt/sources.list.d/nodesource.list && \
  echo 'deb-src [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x jammy main' >> /etc/apt/sources.list.d/nodesource.list && \
  apt-get update && \
  apt-get install -y nodejs && \
  apt-get remove gnupg curl -y  && \
  rm -fr /var/lib/apt/lists/* && \
  ln -sf /dev/stdout /var/log/nginx/access.log && \
  ln -sf /dev/stderr /var/log/nginx/error.log && \
  chmod 777 /run/php && \
  sed -i \
  -e "s/;listen.mode = 0660/listen.mode = 0666/g" \
  /etc/php/*/fpm/pool.d/www.conf

ADD projects/web-base/dist/* /app/
ADD projects/home/dist/* /app/
ADD projects/circuits/dist /app/circuits
ADD projects/pseudoromanian/dist /app/pseudoromana
ADD projects/keygen-radio/dist /app/keygen-radio
ADD projects/papers/dist /app/papers
ADD projects/sibf/dist /app/sibf
ADD projects/chess-puzzles/dist /app/chess-puzzles
ADD projects/identitate-falsa/dist /app/identitate-falsa
ADD projects/horoscop/dist /app/horoscop
ADD projects/jpeg-enricher/dist /app/jpeg-enricher
ADD projects/lemon-cake/dist /app/lemon-cake
ADD projects/software-security-slides/clang-static-analyzer /app/clang-static-analyzer
ADD projects/software-security-slides/executable-code-injection /app/executable-code-injection
ADD projects/software-security-slides/using-openvas /app/using-openvas
ADD projects/software-security-slides/using-metasploit /app/using-metasploit
ADD projects/meet-firefox/dist /app/meet-firefox
ADD projects/italia-fascista/dist /app/italia-fascista
ADD projects/rstsd/dist /app/rstsd
ADD projects/go-concurrency/build /app/go-concurrency
ADD projects/xslt-blog/dist /app/xslt-blog
ADD projects/facetrain/presentation/build /app/facetrain
ADD projects/best-black-metal-albums/build /app/best-black-metal-albums
ADD projects/projects/dist /app/projects
ADD projects/college-website/dist /app/college-website
ADD projects/college-website-2/dist /app/college-website-2
ADD projects/paul-scripts/dist /app/paul-scripts
ADD projects/minimul/dist /app/minimul
ADD projects/torus-cycle/dist /app/torus-cycle
ADD projects/romania/build /app/romania

ADD projects/timr/dist /app/timr
ADD projects/timr/libs/smarty-2.6.33/libs /usr/share/php/Smarty
RUN mkdir /home/www-data
ADD projects/timr/db/db.sqlite3 /home/www-data/db.sqlite3
RUN chown -R www-data:www-data /home/www-data && \
  chmod -R 777 /app/timr/sabloane/compile

ADD projects/rpgadvance/dist /app/rpgadvance
ADD projects/photography/dist /app/photography
ADD projects/oldest-instagram/dist /app/oldest-instagram
ADD projects/phonetic-english/dist /app/phonetic-english
ADD projects/pages/dist /app/
ADD projects/old-instagram/build /app/old-instagram
ADD projects/3d/dist /app/3d
ADD projects/kaomoji/build /app/kaomoji
ADD projects/aoire/dist /app/aoire
ADD projects/webgl-demos/dist /app/webgl-demos
ADD projects/graffiti/client/dist /app/graffiti
ADD projects/graffiti/server /graffiti
ADD projects/film-dev-clock/build /app/film-dev-clock
ADD projects/catenary/build /app/catenary
ADD projects/zome/build /app/zome
ADD projects/computer/build /app/computer

ADD config/nginx.conf /etc/nginx/nginx.conf
ADD config/nginx_multilatex.conf /etc/nginx/nginx_multilatex.conf
ADD config/nginx_entrypoint /nginx_entrypoint
RUN sed -i "s/NECHIFORNETVERSION/$NECHIFORNETVERSION/g" /etc/nginx/nginx.conf
ADD config/supervisord.conf /etc/supervisord.conf

EXPOSE 80

# TODO: Is this required?
STOPSIGNAL SIGQUIT

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
