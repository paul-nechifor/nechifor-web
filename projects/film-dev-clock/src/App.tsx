import React, { useMemo, useState } from "react";

import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import KeyboardDoubleArrowDownIcon from "@mui/icons-material/KeyboardDoubleArrowDown";
import KeyboardDoubleArrowUpIcon from "@mui/icons-material/KeyboardDoubleArrowUp";
import Paper from "@mui/material/Paper";
import PauseIcon from "@mui/icons-material/Pause";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import Stack from "@mui/material/Stack";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TextField from "@mui/material/TextField";

import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

type TimeFunc = (nthTime: number) => number;
type TimeHandle = ReturnType<typeof setInterval>;

function strTime(n: number): string {
  const min = Math.floor(n / 60);
  const sec = n % 60;
  return `${min.toString().padStart(2, "0")}:${sec
    .toString()
    .padStart(2, "0")}`;
}

class Step {
  name: string;
  time: TimeFunc;

  constructor(name: string, time: TimeFunc) {
    this.name = name;
    this.time = time;
  }

  getTime(nthTime: number): number {
    return Math.floor(this.time(nthTime));
  }
}

class TimeTable {
  table: number[];

  constructor(times: number[]) {
    this.table = [0];
    for (let i = 0; i < times.length; i++) {
      this.table.push(this.table[i] + times[i]);
    }
  }

  getStepIndex(time: number): number {
    for (let i = 0; i < this.table.length; i++) {
      if (time < this.table[i]) {
        return i - 1;
      }
    }

    return this.table.length - 1;
  }

  getStepStopTime(time: number): number {
    const stepIndex = Math.min(
      this.table.length - 2,
      Math.max(0, this.getStepIndex(time)),
    );
    return this.table[stepIndex + 1];
  }

  getStepTime(time: number, stepIndex: number) {
    return time - this.table[stepIndex];
  }

  getStepPercent(time: number, stepIndex: number) {
    return (
      ((time - this.table[stepIndex]) /
        (this.table[stepIndex + 1] - this.table[stepIndex])) *
      100
    );
  }

  getMaxTime(): number {
    return this.table[this.table.length - 1];
  }

  getTimeForStep(stepIndex: number): number {
    return this.table[stepIndex];
  }
}

class DevType {
  name: string;
  steps: Step[];

  constructor(name: string, steps: Step[]) {
    this.name = name;
    this.steps = steps;
  }

  getTimeTable(nthTime: number): TimeTable {
    return new TimeTable(this.steps.map((x) => x.getTime(nthTime)));
  }
}

// For the time I used https://goodcalculators.com/quadratic-regression-calculator/
const DEV_TYPES = [
  new DevType("Colortec C-41", [
    new Step("Water", () => 5 * 60),
    new Step("Colour Developer", (x) => 3.75 * x + 195),
    new Step("Water", () => 60),
    new Step("Bleach Fix", (x) => (0.0469 * x * x + 0.3625 * x + 3.95) * 60),
    new Step("Water", () => 3 * 60),
    new Step("Stabilizer", () => 60),
  ]),
  new DevType("Colortec E-6", [
    new Step("Water", () => 5 * 60),
    new Step("First Developer", (x) => 3.75 * x + 375),
    new Step("Water", () => 150),
    new Step("Colour Developer", (x) => 15 * x + 360),
    new Step("Water", () => 150),
    new Step("Bleach Fix", (x) => 15 * x + 360),
    new Step("Water", () => 4 * 60),
    new Step("Stabilizer", () => 60),
  ]),
];

export default function App() {
  const [devType, setDevType] = useState(DEV_TYPES[0]);
  const [nthTime, setNthTime] = useState(0);
  const [time, setTime] = useState(-1);
  const [intervalHandle, setIntervalHandle] = useState<TimeHandle | null>(null);

  const timeTable: TimeTable = useMemo(() => {
    return devType.getTimeTable(nthTime);
  }, [devType, nthTime]);

  const stepIndex = timeTable.getStepIndex(time);

  const stopInterval = () => {
    if (intervalHandle) {
      clearInterval(intervalHandle);
      setIntervalHandle(null);
    }
  };

  return (
    <div
      style={{
        padding: 10,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Box sx={{ minWidth: 250 }}>
        <FormControl fullWidth variant="filled">
          <InputLabel id="dev-type-label">Dev type</InputLabel>
          <Select
            labelId="dev-type-label"
            value={devType.name}
            label="Dev type"
            onChange={(event: SelectChangeEvent) => {
              setDevType(
                DEV_TYPES.find(
                  (x) => x.name === (event.target.value as string),
                ) as DevType,
              );
              setTime(-1);
              stopInterval();
            }}
          >
            {DEV_TYPES.map((dt) => (
              <MenuItem key={dt.name} value={dt.name}>
                {dt.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <TextField
        label="Film Number"
        type="number"
        InputLabelProps={{
          shrink: true,
        }}
        size="small"
        sx={{ minWidth: 250, marginTop: "5px" }}
        variant="filled"
        value={nthTime + 1}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setNthTime(+event.target.value - 1);
          setTime(-1);
          stopInterval();
        }}
      />

      <DevTypeTable
        devType={devType}
        nthTime={nthTime}
        stepIndex={stepIndex}
        time={time}
        timeTable={timeTable}
      />
      <Stack direction="row" spacing={1}>
        <Button
          variant="contained"
          onClick={() => {
            if (intervalHandle) {
              return;
            }
            const stepStopTime = timeTable.getStepStopTime(time);
            let timeValue = time;
            let handle: TimeHandle | undefined;

            const fn = () => {
              const index = timeTable.getStepIndex(timeValue);
              timeValue++;
              setTime(timeValue);
              if (timeValue >= stepStopTime) {
                say(
                  `Step ${index + 1}, ${devType.steps[index].name}, is done.`,
                );
                if (handle) {
                  clearInterval(handle);
                  setIntervalHandle(null);
                }
              }
            };

            handle = setInterval(fn, 1000);
            setIntervalHandle(handle);

            fn();
          }}
          disabled={!!intervalHandle || time >= timeTable.getMaxTime()}
          startIcon={<PlayArrowIcon />}
        >
          Start
        </Button>
        <Button
          startIcon={<PauseIcon />}
          variant="outlined"
          onClick={() => {
            stopInterval();
          }}
          disabled={!intervalHandle}
        >
          Pause
        </Button>
      </Stack>
      <Stack direction="row" spacing={1}>
        <IconButton
          aria-label="down"
          color="primary"
          disabled={stepIndex >= devType.steps.length}
          onClick={() => {
            stopInterval();
            setTime(timeTable.getTimeForStep(stepIndex + 1));
          }}
        >
          <KeyboardDoubleArrowDownIcon />
        </IconButton>
        <IconButton
          aria-label="up"
          disabled={stepIndex < 1}
          onClick={() => {
            stopInterval();
            setTime(timeTable.getTimeForStep(stepIndex - 1));
          }}
        >
          <KeyboardDoubleArrowUpIcon />
        </IconButton>
      </Stack>
      {time >= timeTable.getMaxTime() && (
        <Button
          variant="contained"
          onClick={() => {
            setNthTime(nthTime + 1);
            setTime(-1);
          }}
        >
          Advance by 1 film
        </Button>
      )}
    </div>
  );
}

interface DevTypeTableProps {
  devType: DevType;
  nthTime: number;
  stepIndex: number;
  time: number;
  timeTable: TimeTable;
}

function DevTypeTable({
  devType,
  nthTime,
  stepIndex,
  time,
  timeTable,
}: DevTypeTableProps): JSX.Element {
  return (
    <div style={{ margin: "10px 0", maxWidth: 400 }}>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="dev type">
          <TableHead>
            <TableRow>
              <TableCell>
                <b>Step</b>
              </TableCell>
              <TableCell align="right">
                <b>Time</b>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {devType.steps.map((step: Step, i: number) => (
              <DevTypeRow
                key={`${i}`}
                step={step}
                nthTime={nthTime}
                isPastRow={i < stepIndex}
                isCurrentRow={i === stepIndex}
                rowTime={timeTable.getStepTime(time, i)}
                percent={
                  i === stepIndex ? timeTable.getStepPercent(time, i) : 0
                }
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

interface DevTypeRowProps {
  step: Step;
  nthTime: number;
  isPastRow: boolean;
  isCurrentRow: boolean;
  rowTime: number;
  percent: number;
}

function DevTypeRow({
  step,
  nthTime,
  isPastRow,
  isCurrentRow,
  rowTime,
  percent,
}: DevTypeRowProps): JSX.Element {
  return (
    <TableRow
      sx={{
        "&:last-child td, &:last-child th": { border: 0 },
        background: isPastRow ? "#e7ffe1" : isCurrentRow ? "#fff9e1" : "white",
      }}
    >
      <TableCell
        component="th"
        scope="row"
        sx={{ fontWeight: isCurrentRow ? "bold" : "normal" }}
      >
        {step.name}
      </TableCell>
      <TableCell align="right">
        <Progress n={percent} style={{ opacity: isCurrentRow ? 100 : 0 }} />{" "}
        {isCurrentRow ? (
          `${strTime(rowTime)} / `
        ) : (
          <span style={{ opacity: 0 }}>00:00 / </span>
        )}
        {strTime(step.getTime(nthTime))}
      </TableCell>
    </TableRow>
  );
}

interface ProgressProps {
  n: number;
  style: { [k: string]: number | string };
}

function Progress({ n, style }: ProgressProps): JSX.Element {
  const s = 20;
  return (
    <svg
      style={{
        width: s,
        height: s,
        borderRadius: "50%",
        display: "inline",
        fontSize: 0,
        lineHeight: 0,
        verticalAlign: -5,
        ...style,
      }}
    >
      <circle
        r={s / 2}
        cx={s / 2}
        cy={s / 2}
        stroke="#6dbd62"
        strokeDasharray={`${(n / 100) * Math.PI * s} ${Math.PI * s}`}
        strokeWidth={s}
        fill="#eee"
      />
    </svg>
  );
}

function say(text: string) {
  speechSynthesis.speak(new SpeechSynthesisUtterance(text));
}
