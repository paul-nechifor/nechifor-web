<table border="0" width="500">
  <tr>
    <th>Name</th>
    <th>First Album </th>
    <th>Genre</th>
    <th>Country</th>
  </tr>
  <tr>
    <td>Placebo</td>
    <td>&nbsp;</td>
    <td>Alternative Rock</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>The Smashing Pumpkins</td>
    <td>&nbsp;</td>
    <td>Alternative Rock</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>Nirvana</td>
    <td>&nbsp;</td>
    <td>Grunge</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>Anaal Nathrakh</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>United Kingdom</td>
  </tr>
  <tr>
    <td>Arallu</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Israel</td>
  </tr>
  <tr>
    <td>Burzum</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Norway</td>
  </tr>
  <tr>
    <td>Cradle of Filth</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>United Kingdom</td>
  </tr>
  <tr>
    <td>Dimmu Borgir</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Norway</td>
  </tr>
  <tr>
    <td>Marduk</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Sweden</td>
  </tr>
  <tr>
    <td>Mayhem</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Norway</td>
  </tr>
  <tr>
    <td>Satyricon</td>
    <td>&nbsp;</td>
    <td>Black Metal</td>
    <td>Norway</td>
  </tr>
  <tr>
    <td>Cannibal Corpse</td>
    <td>&nbsp;</td>
    <td>Death Metal</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>Death</td>
    <td>&nbsp;</td>
    <td>Death Metal</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>Deicide</td>
    <td>1990</td>
    <td>Death Metal</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>God Dethroned</td>
    <td>1992</td>
    <td>Blackened Death Metal</td>
    <td>Netherlands</td>
  </tr>
  <tr>
    <td>Hypocrisy</td>
    <td>1992</td>
    <td>Melodic Death Metal</td>
    <td>Sweden</td>
  </tr>
  <tr>
    <td>Six Feet Under</td>
    <td>1995</td>
    <td>Death Metal</td>
    <td>USA</td>
  </tr>
  <tr>
    <td>Katatonia</td>
    <td>1991</td>
    <td>Doom Metal</td>
    <td>Sweden</td>
  </tr>
  <tr>
    <td>Until Death Overtakes Me</td>
    <td>2000</td>
    <td>Funeral Doom Metal</td>
    <td>Belgium</td>
  </tr>
  <tr>
    <td>Betray My Secrets</td>
    <td>&nbsp;</td>
    <td>Gothic Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Darkseed</td>
    <td>&nbsp;</td>
    <td>Gothic Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Black Sabbath</td>
    <td>&nbsp;</td>
    <td>Heavy Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Iron Maiden</td>
    <td>&nbsp;</td>
    <td>Heavy Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Marilyn Manson</td>
    <td>&nbsp;</td>
    <td>Industrial Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Haggard</td>
    <td>&nbsp;</td>
    <td>Symphonic Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Nightwish</td>
    <td>&nbsp;</td>
    <td>Symphonic Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Therion</td>
    <td>&nbsp;</td>
    <td>Symphonic Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Metallica</td>
    <td>&nbsp;</td>
    <td>Thrash Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Slayer</td>
    <td>&nbsp;</td>
    <td>Thrash Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>KoRn</td>
    <td>&nbsp;</td>
    <td>Nu Metal</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Pink Floyd</td>
    <td>&nbsp;</td>
    <td>Progressive Rock</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Jimi Hendrix</td>
    <td>&nbsp;</td>
    <td>Psychedelic Rock</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Rage Against the Machine</td>
    <td>&nbsp;</td>
    <td>Rap-Rock</td>
    <td>&nbsp;</td>
  </tr>       
</table>
