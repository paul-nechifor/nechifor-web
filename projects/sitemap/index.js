const cheerio = require("cheerio");
const URL = require("url");
const axios = require("axios");
const _ = require("lodash");
const { promises: fs } = require("fs");
const stringify = require("json-stable-stringify");

const root = "http://localhost/";

async function main() {
  const scanList = [{ url: root, referer: null }];
  const seen = new Set([root]);
  let siteMap = [
    "http://localhost/buttons/",
    "http://localhost/games/",
    "http://localhost/lemon-cake/",
  ];
  const externalLinks = [];
  const errors = [];

  for (let i = 0; i < scanList.length; i++) {
    const { $, html, contentType, realUrl, response } = await loadPage(
      scanList[i].url,
    );
    if (!contentType.startsWith("text/")) {
      continue;
    }
    if (contentType === "text/html") {
      if (response.status < 300) {
        siteMap.push(realUrl);
      }
      findErrors(errors, response, realUrl, scanList[i].referer, $, html);
    }
    const links = extractUrls($, scanList[i].url, realUrl);

    for (const link of links) {
      if (seen.has(link.href)) {
        continue;
      }

      seen.add(link.href);

      const linkObj = { url: link.href, referer: realUrl };

      if (link.href.startsWith(root)) {
        if (!shouldOmitUrl(link.href)) {
          scanList.push(linkObj);
        }
      } else {
        externalLinks.push(linkObj);
      }
    }
  }

  await Promise.all([
    generateSiteMap(siteMap),
    writeErrors(errors),
    writeExternalLinks(externalLinks),
  ]);
}

function shouldOmitUrl(url) {
  return (
    url.endsWith(".jpg") ||
    url.endsWith(".pdf") ||
    url.startsWith("http://localhost/multilatex/templates/")
  );
}

const errorsTypes = {
  badStatus: "Bad status code",
  missingMeta: 'Missing <meta charset="utf-8">',
  badDoctype: "Bad <!doctype>",
  badTitle: "Bad <title>",
  badHttpEquiv: "Bad http-equiv",
  badMetaViewport: "Bad meta[name=viewport]",
  badAppleIcon: "",
  missingLinkHome: "",
  missingLinkToProjects: "",
  missingLang: "",
  missingDescription: "",
  missingH1: "",
};

function findErrors(errors, response, url, referer, $, html) {
  if (response.status >= 400) {
    errors.push({
      code: "badStatus",
      url,
      args: { status: response.status, referer },
    });
  }

  if (!$('head meta[charset="utf-8"]').length) {
    errors.push({ code: "missingMeta", url });
  }

  if (html.toLowerCase().indexOf("<!doctype html>") !== 0) {
    errors.push({ code: "badDoctype", url });
  }

  const title = $("head title");
  if (!title.length || title.text().trim().length < 5) {
    errors.push({ code: "badTitle", url, args: { title: title.text() } });
  }

  const equiv = $('head meta[http-equiv="x-ua-compatible"]');
  if (!equiv.length || equiv.attr("content") !== "ie=edge") {
    errors.push({ code: "badHttpEquiv", url });
  }

  const viewport = $("head meta[name=viewport]");
  if (
    !viewport.length ||
    viewport.attr("content") !== "width=device-width,initial-scale=1"
  ) {
    errors.push({ code: "badMetaViewport", url });
  }

  const largeIcon = $('head link[rel="apple-touch-icon"]');
  if (!largeIcon.length || largeIcon.attr("href") !== "/favicon152.png") {
    errors.push({ code: "badAppleIcon", url });
  }

  if (
    !doesAnyLinkExist($, ["/", "https://nechifor.net", "https://nechifor.net/"])
  ) {
    errors.push({ code: "missingLinkHome", url });
  }

  if (
    !doesAnyLinkExist($, [
      "/projects",
      "/projects/",
      "https://nechifor.net/projects",
      "https://nechifor.net/projects/",
    ])
  ) {
    errors.push({ code: "missingLinkToProjects", url });
  }

  const lang = $("html").attr("lang");
  if (lang !== "en" && lang !== "ro") {
    errors.push({ code: "missingLang", url });
  }

  const desc = $("meta[name=description]");
  if (!desc.length || desc.attr("content")?.length < 5) {
    errors.push({ code: "missingDescription", url });
  }

  if (!$("h1").length) {
    errors.push({ code: "missingH1", url });
  }
}

function doesAnyLinkExist($, links) {
  return _.some(links, (link) => $(`a[href="${link}"]`).length);
}

async function generateSiteMap(siteMap) {
  siteMap = siteMap.map((x) => x.replace(/\/index.php$/, "/"));
  siteMap = _.sortBy(_.uniq(siteMap));
  siteMap = siteMap.filter((x) => x.indexOf("/name_selection") === -1);
  siteMap = siteMap.map((x) =>
    x.replace(/^http:\/\/localhost\//g, "https://nechifor.net/"),
  );
  const siteMapFile = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${siteMap.map((x) => "  <url><loc>" + x + "</loc></url>").join("\n")}
</urlset>
`;

  await fs.writeFile("../web-base/static/sitemap.xml", siteMapFile);
  await fs.writeFile("../web-base/dist/sitemap.xml", siteMapFile);
}

async function writeErrors(errors) {
  const expectedErrors = new Set(
    JSON.parse(await fs.readFile("expectedErrors.json")).map((x) =>
      stringify(x),
    ),
  );
  const list = _.sortBy(errors, (x) => [x.code, x.url, stringify(x)]).filter(
    (x) => !expectedErrors.has(stringify(x)),
  );
  const text = stringify(list, { space: 2 });
  await fs.writeFile("errors.json", text);
}

async function writeExternalLinks(links) {
  const list = _.sortBy(links, ["referer", "url"]);
  const text = stringify(list, { space: 2 });
  await fs.writeFile("external-links.json", text);
}

async function loadPage(url) {
  if (!url.startsWith(root)) {
    throw new Error(`Don't scan external stuff: ${url}`);
  }
  let response;
  try {
    response = await axios.get(url);
  } catch (error) {
    if (error.response) {
      response = error.response;
    } else {
      throw error;
    }
  }
  const html = removeNonIndexablePart(response.data);
  if (!response.headers["content-type"]) {
    throw new Error(`No content-type: ${url}`);
  }
  const contentType = response.headers["content-type"].split(";")[0].trim();
  const $ = contentType.startsWith("text/") ? cheerio.load(html) : null;
  const realUrl = response.request.res.responseUrl;
  return { $, html, contentType, realUrl, response };
}

function removeNonIndexablePart(html) {
  const start = "googleoff: all";
  const end = "googleon: all";

  for (;;) {
    let startIndex = html.indexOf(start);
    if (startIndex === -1) {
      break;
    }

    let endIndex = html.indexOf(end);
    if (endIndex === -1) {
      throw new Error("Failed to find end.");
    }

    html =
      html.substring(0, startIndex) + html.substring(endIndex + end.length);
  }

  return html;
}

function extractUrls($, url, realUrl) {
  return $("a[href]")
    .filter(function () {
      return $(this).attr("rel") !== "nofollow";
    })
    .map(function () {
      let href = $(this).attr("href");

      href = href.replace(/^https:\/\//, "http://");
      href = href.replace(/^http:\/\/nechifor\.net\//, root);
      href = href.replace(/#.*/, "");

      if (href.indexOf("http://") !== 0) {
        if (href[0] === "/") {
          href = root.replace(/\/$/, "") + href;
        } else {
          href = URL.resolve(realUrl, href);
        }
      }

      return { href, referer: url };
    })
    .toArray();
}

main();
