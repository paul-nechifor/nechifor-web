const fs = require("fs");
const mkdirp = require("mkdirp");
const sharp = require("sharp");
const util = require("util");
const yaml = require("yaml");
const childProcess = require("child_process");

const exec = util.promisify(childProcess.exec);

const distImages = "dist/images";

const images = yaml.parse(fs.readFileSync("data.yml", "utf8"));

async function main() {
  checkNames();

  await mkdirp(distImages);

  await Promise.all(
    images.map(async (image) => {
      const { name, path, quality = 90, width = 1800 } = image;
      const imagePath = `${distImages}/${name}.jpg`;
      const thumbPath = `${distImages}/${name}.thumb.jpg`;
      await Promise.all([
        writeImage(path, imagePath, width, null, quality),
        writeImage(path, thumbPath, 400, 400, quality),
      ]);
    }),
  );
  await exec(`
    cd ${distImages} &&
    exiftool -r -overwrite_original -all= *.jpg &&
    exiftool -r -overwrite_original -author="Paul Nechifor <paul@nechifor.net>" -copyright="Paul Nechifor (nechifor.net)" *.jpg
  `);
}

async function writeImage(src, dst, width, height, quality) {
  if (await checkFileExists(dst)) {
    return;
  }
  console.log("Writing", dst);
  await sharp(src)
    .resize(width, height)
    .sharpen()
    .jpeg({ quality, optimizeScans: true })
    .toFile(dst);
}

function checkFileExists(file) {
  return fs.promises
    .access(file, fs.constants.F_OK)
    .then(() => true)
    .catch(() => false);
}

function checkNames() {
  const seen = {};

  images.forEach((image) => {
    if (image.name in seen) {
      throw new Exception(`Image with name '${image.name}' exists.`);
    }
    seen[image.name] = true;
  });
}

main();
