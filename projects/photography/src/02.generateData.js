const { promises: fs } = require("fs");
const process = require("process");
const _ = require("lodash");
const path = require("path");
const yaml = require("yaml");
const slug = require("slug");
const imageSize = require("image-size");
const { ExifImage } = require("exif");
const { isMonochrome } = require("./monochrome");
const { filmTypes, plates, cameraTags, monthNames } = require("./config");
const promiseAllSettledLimit = require("./promiseAllSettledLimit");

async function main() {
  const dataFileList = JSON.parse(await fs.readFile("dataFileList.json"));

  const raw = (
    await Promise.all(
      dataFileList.map(async (ymlPath) => {
        try {
          const images = yaml.parse(await fs.readFile(ymlPath, "utf8"));
          return { ymlPath, images };
        } catch (e) {
          console.log("ymlPath", ymlPath);
          throw e;
        }
      }),
    )
  ).flat();

  await fs.writeFile("rawData.yml", yaml.stringify(raw));

  const images = raw
    .map(({ ymlPath, images }) => images.map((image) => ({ image, ymlPath })))
    .flat();

  console.log("There are", images.length, "images.");

  const results = await promiseAllSettledLimit(
    1,
    images.map(prepareImageWrapper),
  );

  const errors = results
    .filter((x) => x.status === "rejected")
    .map((x) => x.reason);

  console.log("Finished preparing.");

  dieOnErrors(errors);

  const finalImages = _.orderBy(
    results.map((x) => x.value),
    ["date", "name"],
    ["desc", "desc"],
  );

  removeNonPluralTags(finalImages);

  dieOnErrors(verifyImages(finalImages));

  console.log("Writing data.yml.");
  await fs.writeFile("data.yml", yaml.stringify(finalImages));
}

function prepareImageWrapper({ image, ymlPath }) {
  return async () => {
    try {
      return await prepareImage(image, ymlPath);
    } catch (e) {
      throw new Error(`Error for ${JSON.stringify({ image, ymlPath })}: ${e}`);
    }
  };
}

async function prepareImage(image, ymlPath) {
  if (!image.path) {
    throw new Error(`No path for ${JSON.stringify(image)} at ${ymlPath}`);
  }
  image.path = path.resolve(path.dirname(ymlPath), image.path);

  if (!image.date) {
    const match = image.path.match(/\/(\d{4}-\d{2}-\d{2})/);
    if (!match) {
      throw new Error(`No date and no date in path for ${image.path}`);
    }
    image.date = match[1];
  }

  image.tags = image.tags || [];

  image.tags.push(image.date.substring(0, 4));
  image.tags.push(image.date.substring(0, 7));

  if (image.description) {
    image.tags.push("with-description");
  }

  image.tags.push(monthNames[+image.date.substring(5, 7) - 1]);

  if (image.path.indexOf("/film/") >= 0) {
    // The format is:
    // - date in ISO (month and day may be missing)
    // - camera name
    // - film name
    // - "exp" if the film is expired
    const parts = path.basename(path.dirname(image.path)).split("_");
    for (let part of parts.slice(1, 3)) {
      if (part.length) {
        image.tags.push(part);
      }
    }
    if (plates.includes(parts[2])) {
      image.tags.push("dryplate");
    }
    if (![...plates, "wetplate", "paper"].includes(parts[2])) {
      image.tags.push("film");
    }
    if (parts[3] && parts[3].startsWith("exp")) {
      image.tags.push("expired-film");
    }

    if (parts[1].length) {
      for (let [cameraTag, cameras] of Object.entries(cameraTags)) {
        if (cameras.includes(parts[1])) {
          image.tags.push(cameraTag);
        }
      }
    }

    if (parts[2].length) {
      for (let filmType of filmTypes) {
        if (parts[2].startsWith(filmType)) {
          image.tags.push(filmType);
          break;
        }
      }
    }
  } else {
    image.tags.push("digital");

    let exifData = null;

    try {
      exifData = await getExif(image.path);
    } catch (err) {
      // console.log("exif error", err);
    }

    if (exifData !== null) {
      if (exifData.image.Model) {
        image.tags.push(slug(exifData.image.Model));
      }

      if (exifData.exif.LensInfo && exifData.exif.LensInfo[0]) {
        const [a, b] = exifData.exif.LensInfo;
        image.tags.push(`lens-${a === b ? a : a + "-" + b}mm`);
      }
    }
  }

  const { width, height } = imageSize(image.path);
  const [d1, d2] = [width, height].sort();

  if (d2 / d1 >= 1.95) {
    image.tags.push("panoramas");
  }

  const ratio = getRatioName(d2 / d1);
  if (ratio) {
    image.tags.push(ratio);
  }

  if (image.tags.includes("color")) {
    image.tags = [...image.tags.filter((x) => x == "color"), "colour"];
  }

  if (!(image.tags.includes("monochrome") || image.tags.includes("colour"))) {
    if (await isMonochrome(image.path)) {
      image.tags.push("monochrome");
    } else {
      image.tags.push("colour");
    }
  }

  image.tags = _.sortBy(_.uniq(image.tags));

  image.width = image.width || pickImageWidth(width, height);

  return image;
}

function getExif(path) {
  return new Promise((res, rej) => {
    new ExifImage({ image: path }, (err, exifData) => {
      if (err) {
        return rej(err);
      }
      res(exifData);
    });
  });
}

function getRatioName(ratio) {
  const ratios = [
    [4, 5],
    [3, 4],
    [2, 3],
    [6, 17],
    [3, 7],
    [1, 2],
    [1, 1],
  ];

  for (let [a, b] of ratios) {
    if (Math.abs(ratio - b / a) <= 0.05 * (b / a)) {
      return `ratio-${a}-${b}`;
    }
  }

  return null;
}

function pickImageWidth(width, height) {
  const mp = width * height;
  const expectedMp = 1800 * 1800;
  const scale = Math.sqrt(expectedMp) / Math.sqrt(mp);
  return Math.round((width * scale) / 100) * 100;
}

function verifyImages(images) {
  const errors = [];
  const namesSeen = {};
  const pathsSeen = {};

  images.forEach((image) => {
    if (!image.name || !image.name.length) {
      errors.push(`No name for '${image.path}'.`);
    }
    if (image.name in namesSeen) {
      errors.push(
        `Image with name '${image.name}' exists (${image.path} and ${
          namesSeen[image.name]
        }).`,
      );
    }
    namesSeen[image.name] = image.path;

    if (image.path in pathsSeen) {
      errors.push(`Image with path '${image.path}' exists.`);
    }
    pathsSeen[image.path] = true;
  });

  if (namesSeen.images) {
    errors.push("The name 'images' is reserved.");
  }

  if (namesSeen.tags) {
    errors.push("The name 'tags' is reserved.");
  }

  return errors;
}

function removeNonPluralTags(images) {
  const tagSet = new Set();

  for (const image of images) {
    image.tags.forEach((tag) => tagSet.add(tag));
  }

  for (const image of images) {
    image.tags = image.tags.map((tag) => {
      if (!tag.endsWith("s") && tagSet.has(tag + "s")) {
        return tag + "s";
      }
      return tag;
    });
    image.tags = _.sortBy(_.uniq(image.tags));
  }
}

function dieOnErrors(errors) {
  if (errors.length) {
    console.error("\n\nErrors:\n");
    for (let error of errors) {
      console.error(error);
    }
    console.error("\n");
    process.exit(1);
  }
}

main();
