const fs = require("fs/promises");
const childProcess = require("child_process");
const util = require("util");
const yaml = require("yaml");
const { split } = require("sentence-splitter");
const _ = require("lodash");

const exec = util.promisify(childProcess.exec);

let aiData;

async function main() {
  await exec(`mkdir -p tmp`);

  aiData = yaml.parse(await fs.readFile("aiData.yml", "utf8"));

  for (const aiDatum of aiData) {
    // Keep only first 3 sentences as the rest are a bit halucinatory.
    aiDatum.description = shorten(aiDatum.description);
    // Remove empty and duplicates.
    aiDatum.tags = _.uniq(
      aiDatum.tags
        .map((x) => x.replace(/-+/g, "-").replace(/(^-+|-+$)/g, ""))
        .filter((x) => x.length >= 2),
    );
  }

  // Extract text to be used for similary comparison.
  const texts = aiData.map(
    ({ description, tags }) => `${description} ${tags.join(" ")}`,
  );
  const similarityMatrix = await computeSimilarity(texts);

  // Add the 3 most similar descriptions (excluding itself).
  for (let [i, similar] of similarityMatrix.entries()) {
    aiData[i].similar = _.sortBy([...similar.entries()], (x) => -x[1])
      .filter((x) => x[0] !== i)
      .slice(0, 3)
      .map((x) => aiData[x[0]].path);
  }

  // Only use tags which are used for >= 2 photos.
  const tagCount = _.mapValues(
    _.groupBy(aiData.flatMap((x) => x.tags)),
    (x) => x.length,
  );
  for (const aiDatum of aiData) {
    aiDatum.tags = _.sortBy(aiDatum.tags.filter((x) => tagCount[x] > 1));
  }

  await fs.writeFile("aiDataClean.yml", yaml.stringify(aiData));
}

function shorten(text) {
  return split(text)
    .filter((x) => x.type === "Sentence")
    .map((x) => x.raw)
    .slice(0, 3)
    .join(" ");
}

async function computeSimilarity(texts) {
  return await new Promise((res, rej) => {
    let out = "";
    const subprocess = childProcess.spawn(
      "uv",
      [
        "run",
        "--with",
        "gensim",
        "--with",
        "click",
        "--with",
        "nltk",
        "python",
        "-m",
        "text_similarity",
        "--topics",
        "200",
      ],
      { cwd: "/home/p/pro/text-similarity" },
    );

    subprocess.stdout.on("data", (data) => {
      out += data;
    });

    subprocess.stderr.on("data", (data) => {
      console.error(`stderr: ${data}`);
    });

    subprocess.on("close", (code) => {
      if (code) {
        rej(code);
      } else {
        res(JSON.parse(out));
      }
    });

    subprocess.stdin.write(JSON.stringify(texts));
    subprocess.stdin.end();
  });
}

main();
