const fs = require("fs");
const { promisify } = require("util");
const exec = promisify(require("child_process").exec);

const searchDir = "/ceah/dropbox/fotografii";

async function main() {
  console.log("Finding poze.yml files.");
  const proc = await exec(`find "${searchDir}" -name poze.yml`);
  const list = proc.stdout.trim().split("\n");
  list.sort();
  console.log("Found", list.length, "poze.yml files.");
  fs.writeFileSync("dataFileList.json", JSON.stringify(list, null, 2));
}

main();
