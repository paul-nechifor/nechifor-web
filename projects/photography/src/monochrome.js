const getPixels = require("get-pixels");
const childProcess = require("child_process");
const util = require("util");
const exec = util.promisify(childProcess.exec);
const os = require("os");
const rimraf = require("rimraf");
const sharp = require("sharp");
const convert = require("color-convert");

async function isMonochrome(path) {
  const thumb = `/tmp/a.jpg`;

  try {
    await sharp(path).resize(100).toFile(thumb);
  } catch (err) {
    console.error(err, path);
    process.exit();
  }

  const pixels = await new Promise((res, rej) => {
    getPixels(thumb, (err, pixels) => {
      if (err) {
        rej(err);
      } else {
        res(pixels);
      }
    });
  });

  const [width, height] = pixels.shape;
  const hues = [];
  let count = 0;
  const saturationTheshold = 24;
  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      const [h, s, l] = convert.rgb.hsl(
        pixels.get(i, j, 0),
        pixels.get(i, j, 1),
        pixels.get(i, j, 2),
      );
      if (l >= 10 && l < 90 && s > 10 && s < saturationTheshold) {
        hues.push(h);
      }
      if (s >= saturationTheshold) {
        count++;
      }
    }
  }
  hues.sort();
  const median = hues[(hues.length / 2) | 0];
  const maxDiff = 20;
  for (let h of hues) {
    const dist1 = Math.abs(median - h);
    const dist2 = Math.abs(median - h - 360);
    if (Math.min(dist1, dist2) > maxDiff) {
      count++;
    }
  }
  const percent = (count / (width * height)) * 100;
  return percent <= 7;
}

exports.isMonochrome = isMonochrome;
