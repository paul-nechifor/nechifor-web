const _ = require("lodash");

async function promiseCreatorAllSettledLimit(nParallel, promiseCreators) {
  const groups = Object.values(
    _.groupBy(
      promiseCreators.map((promiseCreator, index) => ({
        promiseCreator,
        index,
      })),
      ({ index }) => index % nParallel,
    ),
  ).map((group) => group.map(({ promiseCreator }) => promiseCreator));

  const results = (await Promise.all(groups.map(processGroup))).flat();

  const [good, bad] = _.partition(results, (x) => x.status === "fulfilled");

  // Retry errors.
  const secondTry = await processGroup(bad.map((x) => x.retryFunction));
  return [good, secondTry].flat();
}

async function processGroup(promiseCreators) {
  const ret = [];

  for (const promiseCreator of promiseCreators) {
    try {
      ret.push({ status: "fulfilled", value: await promiseCreator() });
    } catch (e) {
      ret.push({
        status: "rejected",
        reason: e,
        retryFunction: promiseCreator,
      });
    }
  }

  return ret;
}

module.exports = promiseCreatorAllSettledLimit;
