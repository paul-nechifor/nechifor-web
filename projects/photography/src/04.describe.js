const fs = require("fs/promises");
const childProcess = require("child_process");
const sharp = require("sharp");
const util = require("util");
const yaml = require("yaml");
const _ = require("lodash");

const exec = util.promisify(childProcess.exec);

let images;
let aiData;

async function main() {
  await exec(`mkdir -p tmp`);
  await getLlava();

  images = yaml.parse(await fs.readFile("data.yml", "utf8"));
  try {
    aiData = yaml.parse(await fs.readFile("aiData.yml", "utf8"));
  } catch (e) {
    aiData = [];
  }

  const realImagePaths = new Set(_.map(images, "path"));
  // Remove ai data for image which have been removed.
  aiData = aiData.filter((image) => realImagePaths.has(image.path));
  await fs.writeFile("aiData.yml", yaml.stringify(aiData));

  const paths = _.difference(_.map(images, "path"), _.map(aiData, "path"));
  console.log("Left to analize", paths.length);

  for (const path of paths) {
    const tmpImage = "tmp/image.jpg";
    await sharp(path).resize(700).jpeg({ quality: 90 }).toFile(tmpImage);
    console.log("Analyzing", path);
    await write({
      path,
      description: await getDescription(tmpImage),
      tags: await getTags(tmpImage),
    });
  }

  await exec(`rm -fr tmp`);
}

async function getLlava() {
  console.log("Getting llava.");
  await exec(`
    if ! [ -e llava.llamafile ]; then
      wget https://huggingface.co/jartine/llava-v1.5-7B-GGUF/resolve/main/llava-v1.5-7b-q4-main.llamafile -qO- > llava.llamafile && 
      chmod +x llava.llamafile
    fi
  `);
}

async function write(newEntry) {
  aiData = _.sortBy(
    [...aiData.filter((x) => x.path !== newEntry.path), newEntry],
    "path",
  );
  await fs.writeFile("aiData.yml", yaml.stringify(aiData));
}

async function getDescription(imagePath) {
  return (await llava(imagePath, "What do you see?")).replace(/\n+/g, "\n\n");
}

async function getTags(imagePath) {
  const tags = await llava(
    imagePath,
    "Create as many tags as possible which describe this image. The tags should be separated by commas.",
  );
  return _.uniq(
    tags
      .split(/,/)
      .map((x) =>
        x
          .trim()
          .toLowerCase()
          .replace(/[^\p{L}]/gu, "-"),
      )
      .filter((x) => x.length > 0),
  );
}

async function llava(imagePath, prompt) {
  const { stdout } = await exec(
    `./llava.llamafile --image '${imagePath}' --temp 0 \ -p "### User: ${prompt}\n### Assistant:" --silent-prompt 2>/dev/null`,
  );
  return stdout.trim();
}

main();
