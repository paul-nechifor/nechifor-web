# Paul Nechifor's Photogoraphy

Start the service:

    yarn start

Build it:

    yarn build

## TODO

- Add links to photographers:

  - https://www.oldparson.art/filmtypes
  - Nick Dvoracek (pinholica): http://pinholica.blogspot.com/
  - Joe van Cleave: http://joevancleave.com/
  - https://www.cameradactyl.com/
  - borut peterlin
  - Lost Light Art

- Podcasts:

  - Homemade camera podcast: http://homemadecamera.com/
