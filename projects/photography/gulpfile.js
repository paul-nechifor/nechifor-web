const fs = require("fs");
const path = require("path");
const lodash = require("lodash");
const gulp = require("gulp");
const rename = require("gulp-rename");
const htmlmin = require("gulp-htmlmin");
const pug = require("gulp-pug");
const webserver = require("gulp-webserver");
const yaml = require("yaml");
const md = require("jstransformer")(require("jstransformer-markdown-it"));
const { myCameras, tagTypes } = require("./src/config");

const nonCapitalizedWords = new Set([
  "a",
  "an",
  "the",
  "and",
  "but",
  "for",
  "nor",
  "or",
  "at",
  "by",
  "in",
  "of",
  "on",
  "to",
  "with",
]);

function unslugify(name) {
  return name
    .replace(/-/g, " ")
    .split(" ")
    .map((x, i) =>
      i > 0 && nonCapitalizedWords.has(x)
        ? x
        : x[0].toUpperCase() + x.substring(1),
    )
    .join(" ");
}

function loadImages() {
  const images = yaml.parse(fs.readFileSync("data.yml", "utf8"));
  let aiData = yaml.parse(fs.readFileSync("aiDataClean.yml", "utf8"));

  aiData = Object.fromEntries(aiData.map((x) => [x.path, x]));
  const pathToName = Object.fromEntries(images.map((x) => [x.path, x.name]));

  for (let image of images) {
    image.title = image.title || unslugify(image.name);
    if (image.cameraInfo) {
      image.cameraLinks = [];
      for (const [linkKey, linkName] of [
        ["cameraWiki", "Camera Wiki"],
        ["wikipedia", "Wikipedia"],
        ["myPrintables", "My 3D Printables"],
        ["otherLink", "link"],
      ]) {
        const url = image.cameraInfo[linkKey];
        if (url) {
          image.cameraLinks.push({ name: linkName, url });
        }
      }
    }
    if (image.description) {
      image.htmlDescription = md.render(image.description, {
        typographer: true,
      }).body;
    }

    const aiDatum = aiData[image.path];
    if (aiDatum) {
      image.aiDescription = aiDatum.description;
      image.aiSimilar = aiDatum.similar.map((x) => pathToName[x]);
      image.aiTags = aiDatum.tags;
    }

    if (image.aiDescription) {
      image.alt = image.aiDescription;
    } else {
      image.alt = image.title;
      if (image.tags.length) {
        image.alt += ` (${image.tags.map((x) => `#${x}`).join(" ")})`;
      }
    }
  }

  return images;
}

function getTags(images) {
  const tags = {};

  for (let image of images) {
    const includesMyCameras = image.tags.includes("my-cameras");
    for (let tag of image.tags) {
      // Don't include camera images in tags other than "my-cameras".
      if (includesMyCameras && tag !== "my-cameras") {
        continue;
      }
      tags[tag] = tags[tag] || [];
      tags[tag].push(image);
    }
  }

  // Add "meta"-tags. These contain no actual images, just custom content.
  tags["my-films"] = [];

  return tags;
}

function getTagType(tag, tagTypes, myFilms) {
  if (myFilms.includes(tag)) {
    return "films";
  }

  for (const [typeName, matchList] of Object.entries(tagTypes)) {
    for (const match of matchList) {
      if (match instanceof RegExp) {
        if (tag.match(match)) {
          return typeName;
        }
      } else {
        if (match === tag) {
          return typeName;
        }
      }
    }
  }

  return "handmade";
}

gulp.task("html", (done) => {
  const images = loadImages();
  const tags = getTags(images);
  const myFilms = images
    .filter((image) => image.path.indexOf("/film/") >= 0)
    .map((image) => path.basename(path.dirname(image.path)).split("_")[2]);
  const tagList = Object.entries(tags).map(([k, vs]) => ({
    name: k,
    count: vs.length,
    tagType: getTagType(k, tagTypes, myFilms),
  }));
  const tagTypeForTag = Object.fromEntries(
    tagList.map(({ name, tagType }) => [name, tagType]),
  );

  const imageByName = Object.fromEntries(images.map((x) => [x.name, x]));

  gulp
    .src("templates/index.pug")
    .pipe(
      pug({
        locals: {
          images: images.filter((x) => !x.tags.includes("my-cameras")),
        },
      }),
    )
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("dist"));

  gulp
    .src("templates/tags.pug")
    .pipe(
      pug({
        locals: {
          tags: lodash.sortBy(tagList, ["name"]),
        },
      }),
    )
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(
      rename((path) => {
        path.basename = "index";
      }),
    )
    .pipe(gulp.dest("dist/tags"));

  for (let i = 0; i < images.length; i++) {
    gulp
      .src("templates/image.pug")
      .pipe(
        pug({
          locals: {
            image: images[i],
            prev: i > 0 ? images[i - 1] : null,
            next: i < images.length - 1 ? images[i + 1] : null,
            isImageOfCamera: myCameras.includes(images[i].name),
            isCameraWithPictures: tags[images[i].name]?.length > 0,
            tagTypeForTag: tagTypeForTag,
          },
        }),
      )
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(
        rename((path) => {
          path.basename = "index";
        }),
      )
      .pipe(gulp.dest(`dist/${images[i].name}`));
  }

  for (let [tag, images] of Object.entries(tags)) {
    const locals = {
      tag,
      images,
      isACamera: myCameras.includes(tag),
      thisCamera: imageByName[tag],
      myCameras,
      isAFilm: myFilms.includes(tag),
      myFilms,
      tags,
      allTags: lodash.orderBy(tagList, ["count", "name"], ["desc", "asc"]),
    };
    if (tag === "my-cameras") {
      locals.images = lodash.orderBy(
        locals.images,
        [(x) => tags[x.name]?.length || 0, "name"],
        ["desc", "asc"],
      );
    }
    gulp
      .src("templates/tag.pug")
      .pipe(pug({ locals }))
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(
        rename((path) => {
          path.basename = "index";
        }),
      )
      .pipe(gulp.dest(`dist/tags/${tag}`));
  }

  done();
});

gulp.task("webserver", (done) => {
  const port = parseInt(process.env.port || "8080", 10);
  gulp
    .src("dist")
    .pipe(webserver({ livereload: true, open: true, port, host: "0.0.0.0" }));
  done();
});

gulp.task("watch", (done) => {
  gulp.watch("index.pug", gulp.series("html"));
  done();
});

gulp.task("build", gulp.series("html"));

gulp.task("default", gulp.series("html", "webserver", "watch"));
