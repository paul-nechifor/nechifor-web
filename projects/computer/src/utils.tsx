import { State, Point } from "./types";
import { MouseEvent } from "react";

export function getPointFromSvgEvent(
  event: MouseEvent<Element>,
  svg: SVGSVGElement,
  state: State,
): Point {
  const point = svg.createSVGPoint();

  point.x = event.clientX;
  point.y = event.clientY;

  const ctm = svg.getScreenCTM();
  if (!ctm) {
    throw Error("No CTM");
  }
  const svgPoint = point.matrixTransform(ctm.inverse());

  return {
    x: (svgPoint.x - state.translate.x) / state.scale,
    y: (svgPoint.y - state.translate.y) / state.scale,
  };
}

interface RectAttributes {
  x: number;
  y: number;
  width: number;
  height: number;
}

export function twoPointsToRectAttributes(a: Point, b: Point): RectAttributes {
  return {
    x: Math.min(a.x, b.x),
    y: Math.min(a.y, b.y),
    width: Math.abs(a.x - b.x),
    height: Math.abs(a.y - b.y),
  };
}

export function getSelectedItems(state: State): Set<string> | null {
  if (!state.selectionPoint || !state.selectionPointEnd) {
    return null;
  }
  const sx = Math.min(state.selectionPoint.x, state.selectionPointEnd.x);
  const sy = Math.min(state.selectionPoint.y, state.selectionPointEnd.y);
  const tx = Math.max(state.selectionPoint.x, state.selectionPointEnd.x);
  const ty = Math.max(state.selectionPoint.y, state.selectionPointEnd.y);

  const ret = new Set<string>();

  for (const comp of state.circuit.components) {
    if (comp.x >= sx && comp.x <= tx && comp.y >= sy && comp.y <= ty) {
      ret.add(comp.id);
    }
  }
  for (const line of state.circuit.lines) {
    if (
      (line.start.x >= sx &&
        line.start.x <= tx &&
        line.start.y >= sy &&
        line.start.y <= ty) ||
      (line.end.x >= sx &&
        line.end.x <= tx &&
        line.end.y >= sy &&
        line.end.y <= ty)
    ) {
      ret.add(line.id);
    }
  }

  if (ret.size) {
    return ret;
  }

  return null;
}
