import { MouseEventHandler, Dispatch } from "react";
import classNames from "classnames";

import { Point, Action } from "./types";
import "./Line.scss";

interface Props {
  id: string;
  start: Point;
  end: Point;
  powered: Set<string>;
  selected: Set<string> | null;
  selectionOffset: Point;
  dispatch: Dispatch<Action>;
  onMouseDown: MouseEventHandler<SVGPathElement>;
}

export default function Line({
  id,
  start,
  end,
  powered,
  selected,
  dispatch,
  onMouseDown,
  selectionOffset,
}: Props) {
  const isSelected = selected && selected.has(id);
  const ox = isSelected ? selectionOffset.x : 0;
  const oy = isSelected ? selectionOffset.y : 0;
  return (
    <g
      transform={`translate(${ox}, ${oy})`}
      className={classNames("lineParent", {
        powered: powered.has(id),
        selected: selected && selected.has(id),
      })}
      onMouseDown={onMouseDown}
    >
      <line x1={start.x} y1={start.y} x2={end.x} y2={end.y} className="line" />
      <circle
        r={1.2}
        cx={start.x}
        cy={start.y}
        className="lineEnd"
        onMouseDown={() => {
          dispatch({ type: "startLineMove", which: "start", point: start });
        }}
      />
      <circle
        r={1.2}
        cx={end.x}
        cy={end.y}
        className="lineEnd"
        onMouseDown={() => {
          dispatch({ type: "startLineMove", which: "end", point: end });
        }}
      />
    </g>
  );
}
