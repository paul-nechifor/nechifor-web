import { State, Connection, ComponentType, LineType } from "./types";
import { gateByName, gateOutWire } from "./Gate";

export default function computePowered(state: State): Set<string> {
  const powered = new Set<string>();
  const mapping = getMapping(state);
  const seen = new Set<string>();
  const componentById = Object.fromEntries(
    state.circuit.components.map((x) => [x.id, x]),
  );
  const lineById = Object.fromEntries(
    state.circuit.lines.map((x) => [x.id, x]),
  );

  const left: Connection[] = [{ type: "component", id: "one", wire: "in" }];

  while (left.length > 0) {
    const conn = left.shift() as Connection;

    if (conn.type === "component") {
      const comp = componentById[conn.id];
      switch (comp.type) {
        case "and":
          if (powered.has(comp.id + "a") && powered.has(comp.id + "b")) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "or":
          if (powered.has(comp.id + "a") || powered.has(comp.id + "b")) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "nand":
          if (!(powered.has(comp.id + "a") && powered.has(comp.id + "b"))) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "nor":
          if (!(powered.has(comp.id + "a") || powered.has(comp.id + "b"))) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "not":
          if (!powered.has(comp.id + "in")) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "xor":
          if (powered.has(comp.id + "a") !== powered.has(comp.id + "b")) {
            activate(seen, left, mapping, comp, powered);
          }
          break;
        case "one":
          activate(seen, left, mapping, comp, powered);
          break;

        default:
          throw new Error("Component not implemented.");
      }
    } else if (conn.type === "line") {
      activateLine(seen, left, mapping, lineById[conn.id], conn.wire, powered);
    } else {
      throw new Error("Connection not implemented.");
    }

    seen.add(conn.id);
  }

  return powered;
}

function activateLine(
  seen: Set<string>,
  left: Connection[],
  mapping: Map<string, Connection[]>,
  line: LineType,
  which: string,
  powered: Set<string>,
) {
  powered.add(line.id);
  const key =
    which === "start"
      ? `${line.end.x},${line.end.y}`
      : `${line.start.x},${line.start.y}`;
  activateKey(seen, left, mapping, key, powered);
}

function activate(
  seen: Set<string>,
  left: Connection[],
  mapping: Map<string, Connection[]>,
  comp: ComponentType,
  powered: Set<string>,
) {
  powered.add(comp.id);
  const out = gateOutWire[comp.type];
  if (!out) {
    return;
  }
  const key = `${comp.x + out.x},${comp.y + out.y}`;
  activateKey(seen, left, mapping, key, powered);
}

function activateKey(
  seen: Set<string>,
  left: Connection[],
  mapping: Map<string, Connection[]>,
  key: string,
  powered: Set<string>,
) {
  const conns = mapping.get(key);
  if (!conns) {
    return;
  }
  for (const conn of conns) {
    if (conn.type === "component") {
      powered.add(conn.id + conn.wire);
    }
    if (seen.has(conn.id)) {
      continue;
    }
    left.push(conn);
  }
}

function getMapping(state: State): Map<string, Connection[]> {
  const ret = new Map<string, Connection[]>();

  for (const comp of state.circuit.components) {
    for (const wire of gateByName[comp.type].wires) {
      const key = `${comp.x + wire.x},${comp.y + wire.y}`;
      const what = { type: "component" as const, id: comp.id, wire: wire.name };
      if (ret.has(key)) {
        ret.get(key)?.push(what);
      } else {
        ret.set(key, [what]);
      }
    }
  }

  for (const line of state.circuit.lines) {
    let key = `${line.start.x},${line.start.y}`;
    let what = { type: "line" as const, id: line.id, wire: "start" };
    if (ret.has(key)) {
      ret.get(key)?.push(what);
    } else {
      ret.set(key, [what]);
    }
    key = `${line.end.x},${line.end.y}`;
    what = { type: "line" as const, id: line.id, wire: "end" };
    if (ret.has(key)) {
      ret.get(key)?.push(what);
    } else {
      ret.set(key, [what]);
    }
  }

  return ret;
}
