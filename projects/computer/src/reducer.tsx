import update from "immutability-helper";

import { IndexEnd, Point, State, Action, ComponentType } from "./types";
import { gateByName, gateOutWireAlignment } from "./Gate";
import { getSelectedItems } from "./utils";

export default function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "scaleTranslate":
      return update(state, {
        scale: { $set: action.scale },
        translate: { $set: action.translate },
      });
    case "newComponent":
      return update(state, {
        circuit: { components: { $push: [offsetToOutput(action.component)] } },
        activeButton: { $set: null },
      });
    case "setStartPoint":
      return update(state, {
        startPoint: { $set: { x: action.x, y: action.y } },
      });
    case "dragMove":
      return update(state, {
        translate: { $set: action.translate },
        startPoint: { $set: action.startPoint },
      });
    case "setActiveButton":
      return update(state, {
        activeButton: { $set: action.activeButton },
        startLine: { $set: false },
      });
    case "setComponentMove": {
      const point =
        action.itemType === "component"
          ? state.circuit.components[action.itemIndex]
          : state.circuit.lines[action.itemIndex].start;
      return update(state, {
        itemMoveIndex: { $set: action.itemIndex },
        itemMoveType: { $set: action.itemType },
        itemMoveOrigin: { $set: action.point },
        itemMovePreviousPosition: { $set: { x: point.x, y: point.y } },
        selectionPoint: { $set: null },
      });
    }
    case "moveComponent": {
      if (state.selectedItems) {
        // Simulate movement of all selected components.
        return update(state, {
          selectionOffset: {
            $set: {
              x: alignNumber(action.point.x - state.itemMoveOrigin.x, 5),
              y: alignNumber(action.point.y - state.itemMoveOrigin.y, 5),
            },
          },
        });
      } else {
        if (state.itemMoveType === "component") {
          const item = state.circuit.components[state.itemMoveIndex];
          const offset = gateOutWireAlignment[item.type];
          return update(state, {
            circuit: {
              components: {
                [state.itemMoveIndex]: {
                  x: {
                    $set:
                      alignNumber(
                        state.itemMovePreviousPosition.x -
                          state.itemMoveOrigin.x +
                          action.point.x,
                        5,
                      ) - offset.x,
                  },
                  y: {
                    $set:
                      alignNumber(
                        state.itemMovePreviousPosition.y -
                          state.itemMoveOrigin.y +
                          action.point.y,
                        5,
                      ) - offset.y,
                  },
                },
              },
            },
          });
        } else {
          const line = state.circuit.lines[state.itemMoveIndex];
          return update(state, {
            circuit: {
              lines: {
                [state.itemMoveIndex]: {
                  start: {
                    x: {
                      $set: alignNumber(
                        state.itemMovePreviousPosition.x -
                          state.itemMoveOrigin.x +
                          action.point.x,
                        5,
                      ),
                    },
                    y: {
                      $set: alignNumber(
                        state.itemMovePreviousPosition.y -
                          state.itemMoveOrigin.y +
                          action.point.y,
                        5,
                      ),
                    },
                  },
                  end: {
                    x: {
                      $set: alignNumber(
                        state.itemMovePreviousPosition.x -
                          state.itemMoveOrigin.x +
                          action.point.x -
                          line.start.x +
                          line.end.x,
                        5,
                      ),
                    },
                    y: {
                      $set: alignNumber(
                        state.itemMovePreviousPosition.y -
                          state.itemMoveOrigin.y +
                          action.point.y -
                          line.start.y +
                          line.end.y,
                        5,
                      ),
                    },
                  },
                },
              },
            },
          });
        }
      }
    }
    case "onMouseUp":
      // TODO: change the position of every component and line by state.selectionOffset if selectItems is truthy.
      return update(state, {
        itemMoveIndex: { $set: -1 },
        itemMoveOrigin: { $set: { x: 0, y: 0 } },
        itemMovePreviousPosition: { $set: { x: 0, y: 0 } },
        lineMove: { $set: false },
        lineMoveIndices: { $set: [] },
        lineMovePoint: { $set: { x: 0, y: 0 } },
        selectionPoint: { $set: null },
        selectionPointEnd: { $set: null },
        selectedItems: { $set: getSelectedItems(state) },
      });
    case "setLineStart":
      return update(state, {
        startLine: { $set: true },
        activeButton: { $set: null },
      });
    case "addLine": {
      const point = {
        x: alignNumber(action.point.x, 5),
        y: alignNumber(action.point.y, 5),
      };
      return update(state, {
        circuit: {
          lines: { $push: [{ id: action.id, start: point, end: point }] },
        },
        startLine: { $set: false },
        lineMove: { $set: true },
        lineMoveIndices: { $set: [[state.circuit.lines.length, "end"]] },
        lineMovePoint: { $set: point },
      });
    }
    case "startLineMove":
      return update(state, {
        lineMove: { $set: true },
        lineMoveIndices: { $set: findEnds(action.point, state) },
        lineMovePoint: { $set: action.point },
      });
    case "lineUpdate": {
      const point = {
        x: alignNumber(action.point.x, 5),
        y: alignNumber(action.point.y, 5),
      };
      return update(state, {
        lineMove: { $set: true },
        lineMovePoint: { $set: point },
        circuit: {
          lines: getLineUpdate(state, point),
        },
      });
    }
    case "toggleGrid":
      return update(state, { $toggle: ["grid"] });

    case "setPowered":
      return update(state, { powered: { $set: action.powered } });

    case "setSelectionPoint":
      return update(state, { selectionPoint: { $set: action.point } });

    case "selectionMove":
      return update(state, { selectionPointEnd: { $set: action.point } });

    case "moveSelectionOffset":
      return update(state, { selectionOffset: { $set: action.point } });
  }
}

function getLineUpdate(state: State, point: Point): any {
  const ret: any = {};

  for (const [index, which] of state.lineMoveIndices) {
    ret[index] = { [which]: { $set: point } };
  }

  return ret;
}

function findEnds(point: Point, state: State): IndexEnd[] {
  const ret: IndexEnd[] = [];

  for (const [i, { start, end }] of state.circuit.lines.entries()) {
    if (start.x === point.x && start.y === point.y) {
      ret.push([i, "start"]);
    } else if (end.x === point.x && end.y === point.y) {
      ret.push([i, "end"]);
    }
  }

  return ret;
}

function offsetToOutput(component: ComponentType): ComponentType {
  const { id, type, x, y } = component;
  const gate = gateByName[type];
  let offsetX = 0;
  let offsetY = 0;

  const outWire = gate.wires.find((x) => x.name === "out");

  if (outWire) {
    offsetX = -outWire.x;
    offsetY = -outWire.y;
  }

  return {
    id,
    type,
    x: alignNumber(x, 5) + offsetX,
    y: alignNumber(y, 5) + offsetY,
  };
}

function alignNumber(n: number, size: number): number {
  return Math.round(n / size) * size;
}
