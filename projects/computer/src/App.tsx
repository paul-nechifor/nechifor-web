import { useReducer, useEffect, Reducer } from "react";
import update from "immutability-helper";

import { CircuitType, State, Action } from "./types";
import reducer from "./reducer";
import Panel from "./Panel";
import { CIRCUIT_KEY } from "./constants";
import DrawArea from "./DrawArea";
import computePowered from "./computePowered";

const defaultCircuit: CircuitType = {
  size: {
    width: 1000,
    height: 1000,
  },
  components: [
    {
      id: "one",
      type: "one",
      x: 20,
      y: 20,
    },
    {
      id: "g1",
      type: "or",
      x: 78,
      y: 20,
    },
  ],
  lines: [{ id: "l1", start: { x: 60, y: 30 }, end: { x: 75, y: 25 } }],
};

const defaultState = {
  circuit: {
    size: {
      width: 1000,
      height: 1000,
    },
    components: [],
    lines: [],
  },
  scale: 1,
  translate: { x: 0, y: 0 },
  startPoint: { x: 0, y: 0 },
  activeButton: null,
  itemMoveOrigin: { x: 0, y: 0 },
  itemMoveIndex: -1,
  itemMoveType: null,
  itemMovePreviousPosition: { x: 0, y: 0 },
  startLine: false,
  lineMove: false,
  lineMoveIndices: [],
  lineMovePoint: { x: 0, y: 0 },
  grid: true,
  powered: new Set<string>(["one", "l1", "oneout"]),
  selectionPoint: null,
  selectionPointEnd: null,
  selectionOffset: { x: 0, y: 0 },
  selectedItems: null,
};

export default function App() {
  const [state, dispatch] = useReducer<Reducer<State, Action>, State>(
    reducer,
    defaultState,
    (state: State): State => {
      const item = localStorage.getItem(CIRCUIT_KEY);
      return update(state, {
        circuit: { $set: item ? JSON.parse(item) : defaultCircuit },
      });
    },
  );

  useEffect(() => {
    localStorage.setItem(CIRCUIT_KEY, JSON.stringify(state.circuit));

    dispatch({ type: "setPowered", powered: computePowered(state) });
  }, [state.circuit]);

  return (
    <div style={{ display: "flex" }}>
      <Panel
        activeButton={state.activeButton}
        startLine={state.startLine}
        dispatch={dispatch}
      />
      <DrawArea state={state} dispatch={dispatch} />
    </div>
  );
}
