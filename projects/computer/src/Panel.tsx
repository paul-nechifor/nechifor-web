import { Dispatch } from "react";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";

import { Action } from "./types";
import { gates } from "./Gate";
import { PANEL_WIDTH } from "./constants";

export default function Panel({
  activeButton,
  startLine,
  dispatch,
}: {
  activeButton: string | null;
  startLine: boolean;
  dispatch: Dispatch<Action>;
}) {
  return (
    <div
      style={{
        width: PANEL_WIDTH,
        height: "100vh",
        padding: 10,
        boxSizing: "border-box",
      }}
    >
      <h2>Computer</h2>
      <Stack direction="column" spacing={2}>
        {gates.map((gate) => {
          if (gate.name === "battery") {
            return null;
          }
          return (
            <Button
              key={gate.name}
              variant="contained"
              color={activeButton === gate.name ? "success" : "primary"}
              onClick={() => {
                dispatch({ type: "setActiveButton", activeButton: gate.name });
              }}
            >
              {gate.name}
            </Button>
          );
        })}
        <Button
          variant="contained"
          color={startLine ? "success" : "primary"}
          onClick={() => {
            dispatch({ type: "setLineStart" });
          }}
        >
          Line
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            dispatch({ type: "toggleGrid" });
          }}
        >
          Grid
        </Button>
      </Stack>
    </div>
  );
}
