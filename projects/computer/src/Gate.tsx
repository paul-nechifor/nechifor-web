import { ComponentType, Point } from "./types";
import { MouseEventHandler } from "react";
import classNames from "classnames";
import "./Gate.scss";

const h = 20;
const w = 13;

interface ComponentWire {
  name: string;
  x: number;
  y: number;
  length: number;
}

interface ComponentBuilder {
  name: string;
  wires: ComponentWire[];
  body: string;
  unfilledBody?: string;
}

const notCircle = ["a", 2.5, 2.5, 0, 1, 0, 5, 0, "a", 2.5, 2.5, 0, 1, 0, -5, 0];

export const gates: ComponentBuilder[] = [
  {
    name: "and",
    wires: [
      {
        name: "a",
        x: -5,
        y: 5,
        length: 5,
      },
      {
        name: "b",
        x: -5,
        y: 15,
        length: 5,
      },
      {
        name: "out",
        x: 30,
        y: 10,
        length: -7,
      },
    ],
    body: [
      "M",
      0,
      0,
      "l",
      w,
      0,
      "A",
      h / 2,
      h / 2,
      0,
      0,
      1,
      w,
      h,
      "l",
      -w,
      0,
      "l",
      0,
      -h,
      "z",
    ].join(" "),
  },
  {
    name: "or",
    wires: [
      {
        name: "a",
        x: -3,
        y: 5,
        length: 5,
      },
      {
        name: "b",
        x: -3,
        y: 15,
        length: 5,
      },
      {
        name: "out",
        x: 32,
        y: 10,
        length: -5,
      },
    ],
    body: [
      "M",
      0,
      0,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      2 * w,
      0.5 * h,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      0,
      h,
      "Q",
      6,
      10,
      0,
      0,
      "z",
    ].join(" "),
  },
  {
    name: "nand",
    wires: [
      {
        name: "a",
        x: -5,
        y: 5,
        length: 5,
      },
      {
        name: "b",
        x: -5,
        y: 15,
        length: 5,
      },
      {
        name: "out",
        x: 35,
        y: 10,
        length: -5,
      },
    ],
    body: [
      "M",
      0,
      0,
      "l",
      w,
      0,
      "A",
      h / 2,
      h / 2,
      0,
      0,
      1,
      w,
      h,
      "l",
      -w,
      0,
      "l",
      0,
      -h,
      "z",
      "M",
      w * 1.89,
      h / 2,
      ...notCircle,
    ].join(" "),
  },
  {
    name: "nor",
    wires: [
      {
        name: "a",
        x: -3,
        y: 5,
        length: 5,
      },
      {
        name: "b",
        x: -3,
        y: 15,
        length: 5,
      },
      {
        name: "out",
        x: 37,
        y: 10,
        length: -4,
      },
    ],
    body: [
      "M",
      0,
      0,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      2 * w,
      0.5 * h,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      0,
      h,
      "Q",
      6,
      10,
      0,
      0,
      "z",
      "M",
      w * 2.1,
      h / 2,
      ...notCircle,
    ].join(" "),
  },
  {
    name: "not",
    wires: [
      {
        name: "in",
        x: -5,
        y: 10,
        length: 5,
      },
      {
        name: "out",
        x: 35,
        y: 10,
        length: -7,
      },
    ],
    body: [
      "M",
      0,
      0,
      "l",
      1.5 * 14,
      0.5 * h,
      "l",
      -1.5 * 14,
      0.5 * h,
      "l",
      0,
      -h,
      "z",
      "M",
      w * 1.72,
      h / 2,
      ...notCircle,
    ].join(" "),
  },
  {
    name: "xor",
    wires: [
      {
        name: "a",
        x: -7,
        y: 5,
        length: 9,
      },
      {
        name: "b",
        x: -7,
        y: 15,
        length: 9,
      },
      {
        name: "out",
        x: 38,
        y: 10,
        length: -5,
      },
    ],
    body: [
      "M",
      0,
      0,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      2 * w,
      0.5 * h,
      "A",
      h * 1.5,
      h * 1.5,
      0,
      0,
      1,
      0,
      h,
      "Q",
      6,
      10,
      0,
      0,
      "z",
      "M",
      w * 2.1,
      h / 2,
      ...notCircle,
    ].join(" "),
    unfilledBody: ["M", -4, h, "Q", 2, 10, -4, 0].join(" "),
  },
  {
    name: "one",
    wires: [
      {
        name: "out",
        x: 40,
        y: 10,
        length: -7,
      },
    ],
    body: ["M", 10, 0, "l", 25, 0, 0, 20, -25, 0, "z"].join(" "),
    unfilledBody: ["M", 20.5, 9, "l", 3, -3, 0, 10].join(" "),
  },
];
export const gateByName = Object.fromEntries(gates.map((x) => [x.name, x]));

function getOutWireAlignment(gate: ComponentBuilder): Point {
  const outWire = gate.wires.find((x) => x.name === "out");
  if (!outWire) {
    return { x: 0, y: 0 };
  }

  return { x: outWire.x % 5, y: outWire.y % 5 };
}

export const gateOutWireAlignment = Object.fromEntries(
  gates.map((x) => [x.name, getOutWireAlignment(x)]),
);

export const gateOutWire = Object.fromEntries(
  gates.map((x) => [x.name, x.wires.find((x) => x.name === "out")]),
);

interface GateProps extends ComponentType {
  onMouseDown: MouseEventHandler<SVGPathElement>;
  powered: Set<string>;
  selected: Set<string> | null;
  selectionOffset: Point;
}

export default function Gate({
  id,
  type,
  x,
  y,
  powered,
  selected,
  selectionOffset,
  onMouseDown,
}: GateProps) {
  const builder = gateByName[type];
  const isSelected = selected && selected.has(id);
  const ox = isSelected ? selectionOffset.x : 0;
  const oy = isSelected ? selectionOffset.y : 0;

  return (
    <g
      transform={`translate(${x + ox}, ${y + oy})`}
      className={classNames("component", {
        powered: powered.has(id),
        selected: isSelected,
      })}
    >
      {builder.wires.map((wire) => {
        return (
          <line
            x1={wire.x}
            y1={wire.y}
            x2={wire.x + wire.length}
            y2={wire.y}
            className={classNames("gateWire", {
              powered: powered.has(id + wire.name),
            })}
          />
        );
      })}
      <path d={builder.body} className="gate" onMouseDown={onMouseDown} />
      {builder.unfilledBody && (
        <path d={builder.unfilledBody} className="gate unfilledGate" />
      )}
    </g>
  );
}
