import React, { Component, createRef } from "react";
import { getPointFromSvgEvent, twoPointsToRectAttributes } from "./utils";
import { v4 as uuidv4 } from "uuid";

import { State, Action } from "./types";
import CircuitDisplay from "./CircuitDisplay";
import { PANEL_WIDTH } from "./constants";
import "./DrawArea.scss";

interface DrawAreaProps {
  state: State;
  dispatch: React.Dispatch<Action>;
}

interface DrawAreaState {
  componentSize: { width: number; height: number };
  dragging: boolean;
}

export default class DrawArea extends Component<DrawAreaProps, DrawAreaState> {
  private svgRef = createRef<SVGSVGElement>();

  constructor(props: DrawAreaProps) {
    super(props);
    this.state = {
      componentSize: { width: 0, height: 0 },
      dragging: false,
    };
  }

  handleResize = () => {
    this.setState({
      componentSize: {
        width: window.innerWidth - PANEL_WIDTH,
        height: window.innerHeight,
      },
    });
  };

  componentDidMount() {
    this.handleResize();
    window.addEventListener("resize", this.handleResize);

    const elem = this.svgRef.current;
    if (elem) {
      elem.addEventListener("wheel", this.handleWheel);
      elem.addEventListener("mousedown", this.handleMouseDown);
      elem.addEventListener("mousemove", this.handleMouseMove);
      elem.addEventListener("mouseup", this.handleMouseUp);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);

    const elem = this.svgRef.current;
    if (elem) {
      elem.removeEventListener("wheel", this.handleWheel);
      elem.removeEventListener("mousedown", this.handleMouseDown);
      elem.removeEventListener("mousemove", this.handleMouseMove);
      elem.removeEventListener("mouseup", this.handleMouseUp);
    }
  }

  handleWheel = (e: WheelEvent): void => {
    const { state, dispatch } = this.props;
    const { svgRef } = this;

    if (!svgRef.current) {
      return;
    }
    e.preventDefault();
    const zoomFactor = 0.1;
    const newScale =
      e.deltaY < 0
        ? state.scale * (1 + zoomFactor)
        : state.scale * (1 - zoomFactor);

    const svgRect = svgRef.current.getBoundingClientRect();

    const mouseX = e.clientX - svgRect.left;
    const mouseY = e.clientY - svgRect.top;

    const newTranslate = {
      x: mouseX - (mouseX - state.translate.x) * (newScale / state.scale),
      y: mouseY - (mouseY - state.translate.y) * (newScale / state.scale),
    };

    if (newScale > 10 || newScale < 0.2) {
      return;
    }

    dispatch({
      type: "scaleTranslate",
      scale: newScale,
      translate: newTranslate,
    });
  };

  handleMouseDown = (e: MouseEvent): void => {
    if (e.button === 0) {
      const point = getPointFromSvgEvent(
        e as unknown as React.MouseEvent<Element>,
        e.currentTarget as unknown as SVGSVGElement,
        this.props.state,
      );
      this.props.dispatch({ type: "setSelectionPoint", point });
    } else if (e.button === 1) {
      this.setState({ dragging: true });
      this.props.dispatch({
        type: "setStartPoint",
        x: e.clientX,
        y: e.clientY,
      });
    }
  };

  handleMouseMove = (e: MouseEvent): void => {
    const { state, dispatch } = this.props;
    const { dragging } = this.state;

    if (state.selectionPoint) {
      const point = getPointFromSvgEvent(
        e as unknown as React.MouseEvent<Element>,
        e.currentTarget as unknown as SVGSVGElement,
        state,
      );
      dispatch({ type: "selectionMove", point });
      return;
    }

    if (state.lineMove) {
      const point = getPointFromSvgEvent(
        e as unknown as React.MouseEvent<Element>,
        e.currentTarget as unknown as SVGSVGElement,
        state,
      );
      dispatch({ type: "lineUpdate", point });
      return;
    }

    if (state.itemMoveIndex > -1) {
      const point = getPointFromSvgEvent(
        e as unknown as React.MouseEvent<Element>,
        e.currentTarget as unknown as SVGSVGElement,
        state,
      );
      dispatch({ type: "moveComponent", point });
      return;
    }

    // Dragging background.
    if (dragging) {
      const dx = e.clientX - state.startPoint.x;
      const dy = e.clientY - state.startPoint.y;
      dispatch({
        type: "dragMove",
        translate: {
          x: state.translate.x + dx,
          y: state.translate.y + dy,
        },
        startPoint: { x: e.clientX, y: e.clientY },
      });
    }
  };

  handleMouseUp = (): void => {
    this.setState({ dragging: false });
    this.props.dispatch({ type: "onMouseUp" });
  };

  render() {
    const { state, dispatch } = this.props;
    const { componentSize, dragging } = this.state;

    return (
      <svg
        ref={this.svgRef}
        style={{
          backgroundColor: "white",
          display: "block",
          cursor: dragging ? "grabbing" : "auto",
          width: componentSize.width,
          height: componentSize.height,
        }}
        onClick={(event) => {
          const { x, y } = getPointFromSvgEvent(
            event,
            event.currentTarget,
            state,
          );

          if (state.activeButton) {
            dispatch({
              type: "newComponent",
              component: { id: uuidv4(), type: state.activeButton, x, y },
            });
          } else if (state.startLine) {
            dispatch({ type: "addLine", point: { x, y }, id: uuidv4() });
          }
        }}
      >
        <defs>
          <pattern
            id="smallGrid"
            width="5"
            height="5"
            patternUnits="userSpaceOnUse"
          >
            <path
              d="M 5 0 L 0 0 0 5"
              fill="none"
              stroke="gray"
              strokeWidth="0.5"
            />
          </pattern>
        </defs>

        <g
          transform={`translate(${state.translate.x}, ${state.translate.y}) scale(${state.scale})`}
        >
          {state.grid && (
            <rect
              width={state.circuit.size.width}
              height={state.circuit.size.height}
              fill="url(#smallGrid)"
            />
          )}
          <CircuitDisplay state={state} dispatch={dispatch} />
          {state.selectionPoint && state.selectionPointEnd && (
            <rect
              {...twoPointsToRectAttributes(
                state.selectionPoint,
                state.selectionPointEnd,
              )}
              className="selection"
            />
          )}
        </g>
      </svg>
    );
  }
}
