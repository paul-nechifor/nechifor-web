import { Dispatch, MouseEvent as ReactMouseEvent } from "react";
import { getPointFromSvgEvent } from "./utils";

import { ComponentType, State, Action } from "./types";
import Gate from "./Gate";
import Line from "./Line";

export default function CircuitDisplay({
  state,
  dispatch,
}: {
  state: State;
  dispatch: Dispatch<Action>;
}) {
  return (
    <g>
      {state.circuit.components.map((component: ComponentType) => {
        return (
          <Gate
            key={component.id}
            {...component}
            powered={state.powered}
            selected={state.selectedItems}
            selectionOffset={state.selectionOffset}
            onMouseDown={(event: ReactMouseEvent<SVGPathElement>) => {
              if (!event.currentTarget.ownerSVGElement) {
                throw new Error("Bad SVG.");
              }
              event.preventDefault();
              event.stopPropagation();
              const itemIndex = state.circuit.components.findIndex(
                (x) => x.id === component.id,
              );
              const point = getPointFromSvgEvent(
                event,
                event.currentTarget.ownerSVGElement,
                state,
              );
              dispatch({
                type: "setComponentMove",
                itemIndex,
                point,
                itemType: "component",
              });
            }}
          />
        );
      })}
      {state.circuit.lines.map((line) => {
        return (
          <Line
            id={line.id}
            key={line.id}
            start={line.start}
            end={line.end}
            powered={state.powered}
            selected={state.selectedItems}
            selectionOffset={state.selectionOffset}
            dispatch={dispatch}
            onMouseDown={(event: ReactMouseEvent<SVGPathElement>) => {
              if (!event.currentTarget.ownerSVGElement) {
                throw new Error("Bad SVG.");
              }
              event.preventDefault();
              event.stopPropagation();
              const itemIndex = state.circuit.lines.findIndex(
                (x) => x.id === line.id,
              );
              const point = getPointFromSvgEvent(
                event,
                event.currentTarget.ownerSVGElement,
                state,
              );
              dispatch({
                type: "setComponentMove",
                itemIndex,
                point,
                itemType: "line",
              });
            }}
          />
        );
      })}
    </g>
  );
}
