export interface ComponentType {
  id: string;
  type: string;
  x: number;
  y: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface LineType {
  id: string;
  start: Point;
  end: Point;
}

export interface SizeType {
  width: number;
  height: number;
}

export interface CircuitType {
  components: ComponentType[];
  lines: LineType[];
  size: SizeType;
}

export type Which = "start" | "end";
export type ItemType = "line" | "component";

export type IndexEnd = [number, Which];

export interface Connection {
  id: string;
  type: ItemType;
  wire: string;
}

export interface State {
  circuit: CircuitType;
  scale: number;
  translate: Point;
  startPoint: Point;
  activeButton: string | null;
  itemMoveIndex: number;
  itemMoveOrigin: Point;
  itemMoveType: ItemType | null;
  itemMovePreviousPosition: Point;
  startLine: boolean;
  lineMove: boolean;
  lineMoveIndices: IndexEnd[];
  lineMovePoint: Point;
  grid: boolean;
  powered: Set<string>;
  selectionPoint: Point | null;
  selectionPointEnd: Point | null;
  selectionOffset: Point;
  selectedItems: Set<string> | null;
}

export type Action =
  | { type: "scaleTranslate"; scale: number; translate: Point }
  | { type: "newComponent"; component: ComponentType }
  | { type: "setStartPoint"; x: number; y: number }
  | { type: "dragMove"; translate: Point; startPoint: Point }
  | { type: "setActiveButton"; activeButton: string }
  | {
      type: "setComponentMove";
      point: Point;
      itemIndex: number;
      itemType: ItemType;
    }
  | { type: "moveComponent"; point: Point }
  | { type: "onMouseUp" }
  | { type: "setLineStart" }
  | { type: "addLine"; id: string; point: Point }
  | { type: "startLineMove"; which: Which; point: Point }
  | { type: "lineUpdate"; point: Point }
  | { type: "toggleGrid" }
  | { type: "setPowered"; powered: Set<string> }
  | { type: "setSelectionPoint"; point: Point }
  | { type: "selectionMove"; point: Point }
  | { type: "moveSelectionOffset"; point: Point };
