const util = require("util");
const { promises: fs } = require("fs");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const tinutok = require("tinutok");
const tinutokConfig = require("./files/tinutok/config");

async function main() {
  await exec(`rm -fr tmp && mkdir -p tmp dist`);
  try {
    await Promise.all([buildTinutok(), buildSidrem(), buildJurfo()]);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

async function buildTinutok() {
  await exec(`mkdir -p tmp/tinutok/alphabet`);
  await tinutokConfig.alphabet.map(async (letter) => {
    await fs.writeFile(
      `tmp/tinutok/alphabet/${letter}.svg`,
      tinutok.getSvg({
        padding: 2,
        string: letter,
        scale: 8,
        noBackground: true,
      }).svg,
    );
  });
  await fs.writeFile(
    `tmp/tinutok/quote.svg`,
    tinutok.getSvg({
      padding: 10,
      string: tinutokConfig.quote,
      scale: 2,
      noBackground: true,
      wrapHeight: 300,
    }).svg,
  );
  await exec(
    `webpack --entry ./files/tinutok/script.js -o tmp/tinutok --mode production`,
  );
}

async function buildSidrem() {
  await exec(`mkdir -p tmp/sidrem`);
  await exec(
    `webpack --entry ./files/sidrem/script.js -o tmp/sidrem --mode production`,
  );
}

async function buildJurfo() {
  await exec(`mkdir -p tmp/jurfo`);
  await exec(
    `webpack --entry ./files/jurfo/script.js -o tmp/jurfo --mode production`,
  );
}

main();
