const gulp = require("gulp");
const path = require("path");
const fs = require("fs");
const htmlmin = require("gulp-htmlmin");
const pug = require("gulp-pug");
const webserver = require("gulp-webserver");
const tinutokConfig = require("./files/tinutok/config");

gulp.task("default", ["html", "webserver", "watch"]);

gulp.task("build", ["static", "html"]);

function doPug(src, opts = {}) {
  return gulp
    .src(src)
    .pipe(pug(opts))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(path.dirname(src).replace(/^pages/g, "dist")));
}

gulp.task("html", [
  "website-map",
  "software-security-slides",
  "multilatex-dissertation",
  "buttons",
  "tinutok",
  "conscripts",
  "sidrem",
  "jurfo",
  "assembly-cheat-sheet",
  "games",
  "home-2014",
  "blog",
]);

gulp.task("website-map", () => {
  let areas;
  try {
    areas = JSON.parse(fs.readFileSync("../website-map/areas.json", "utf8"));
  } catch (e) {
    console.log("Skipping the website map because the areas don't exist.");
    areas = [];
  }
  return doPug("pages/website-map/index.pug", { locals: { areas } });
});

gulp.task("software-security-slides", () =>
  doPug("pages/software-security-slides/index.pug"),
);

gulp.task("multilatex-dissertation", () =>
  doPug("pages/multilatex-dissertation/index.pug"),
);

gulp.task("buttons", () => {
  return doPug("pages/buttons/index.pug", {
    locals: {
      images88x31: fs.readdirSync("pages/buttons/88x31"),
      images80x15: fs.readdirSync("pages/buttons/80x15"),
    },
  });
});

gulp.task("tinutok", () => {
  const dir = "tmp/tinutok";
  return doPug("pages/tinutok/index.pug", {
    locals: {
      svgs: tinutokConfig.alphabet.map((letter) => ({
        letter,
        svg: fs.readFileSync(
          path.join(dir, "alphabet", letter + ".svg"),
          "utf8",
        ),
      })),
      config: tinutokConfig,
      quote: fs.readFileSync(path.join(dir, "quote.svg"), "utf8"),
      js: fs.readFileSync(path.join(dir, "main.js"), "utf8"),
    },
  });
});

gulp.task("conscripts", () => doPug("pages/conscripts/index.pug"));

gulp.task("sidrem", () => {
  const dir = "tmp/sidrem";
  return doPug("pages/sidrem/index.pug", {
    locals: {
      js: fs.readFileSync(path.join(dir, "main.js"), "utf8"),
    },
  });
});

gulp.task("jurfo", () => {
  const dir = "tmp/jurfo";
  return doPug("pages/jurfo/index.pug", {
    locals: {
      js: fs.readFileSync(path.join(dir, "main.js"), "utf8"),
    },
  });
});

gulp.task("assembly-cheat-sheet", () =>
  doPug("pages/assembly-cheat-sheet/index.pug"),
);

gulp.task("games", () => doPug("pages/games/index.pug"));

gulp.task("home-2014", () => doPug("pages/home-2014/index.pug"));

gulp.task("blog", () => doPug("pages/blog/index.pug"));

gulp.task("static", () => {
  return gulp.src(["pages/**/*", "!pages/**/*.pug"]).pipe(gulp.dest("dist"));
});

gulp.task("webserver", () => {
  const port = parseInt(process.env.port || "3000", 10);
  return gulp.src(".").pipe(
    webserver({
      livereload: true,
      open: true,
      directoryListing: true,
      port,
      host: "localhost",
    }),
  );
});

gulp.task("watch", () => {
  return gulp.watch("pages/*/*.pug", ["html"]);
});
