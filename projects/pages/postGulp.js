const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);

async function main() {
  await Promise.all([buildDotfiles(), buildMultilatexDissertation()]);
  await exec(`rm -fr tmp`);
}

async function buildDotfiles() {
  await exec(`
    mkdir -p tmp/dotfiles
    cd tmp/dotfiles
    cp ../../../dotfiles/config/vim/vimrc.vim .
    cp ../../../dotfiles/config/bash/bashrc.sh .
    sed -i '/shellcheck disable/d' bashrc.sh

    echo '# # Dotfiles
#
# You can see [my dotfiles repo on GitLab](https://gitlab.com/paul-nechifor/dotfiles).
#
# I publish some of them as [literate programming](https://en.wikipedia.org/wiki/Literate_programming) files here:
#
# * [\`.bashrc\`](bashrc.html)
# * [\`.vimrc\`](vimrc.html)
' >index.sh

    echo '
# ---
#
# Created by [Paul Nechifor](/). See [my projects](/projects/).
' > footer
    cat footer >> bashrc.sh
    cat footer >> index.sh
    cat footer | sed 's/^#/"/g' >> vimrc.vim

    ../../node_modules/.bin/docco index.sh bashrc.sh vimrc.vim

    sed -i 's/index.sh/Dotfiles/g' docs/*.html
    mv docs ../../dist/dotfiles
  `);
}

async function buildMultilatexDissertation() {
  await exec(`
    cp ../multilatex-dissertation/lucrare.pdf ../multilatex-dissertation/rezumat.pdf  dist/multilatex-dissertation
    cp -r ../multilatex-dissertation/presentation/build dist/multilatex-dissertation/presentation
  `);
}

main();
