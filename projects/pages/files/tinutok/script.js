import {
  createElement as h,
  render,
  Component,
  useState,
  useRef,
  useEffect
} from "preact/compat";
import { getSvg, drawOnCanvas } from "tinutok";

class App extends Component {
  render() {
    return h(
      "div",
      null,
      h("section", null, h("h2", null, "Try it")),
      h(Sample),
      h("section", null, h("h2", null, "Draw")),
      h(Draw)
    );
  }
}

function NumberField({ label, value, min, max, onChange }) {
  return h(
    "div",
    { className: "field" },
    h("label", null, label + ": "),
    h("input", {
      value,
      type: "number",
      min,
      max,
      onChange: e => {
        const value = e.target.value.trim();
        if (!value.length) {
          return;
        }
        const n = parseFloat(value);
        if (n >= min && n <= max) {
          onChange(n);
        }
      }
    })
  );
}

function BooleanField({ label, value, onChange }) {
  return h(
    "div",
    { className: "field" },
    h("label", null, label + ":"),
    " ",
    h("input", {
      value,
      type: "checkbox",
      checked: value,
      onChange: e => {
        onChange(!value);
      }
    })
  );
}

function TextField({ value, onChange }) {
  return h(
    "div",
    null,
    h("textarea", { rows: 4, onChange: e => onChange(e.target.value) }, value)
  );
}

function Sample() {
  const [text, setText] = useState(
    "Ana are mere, dar cred că poate să aibă și pere. The quick brown fox did some jumping."
  );
  const [height, setHeight] = useState(800);
  const [scale, setScale] = useState(4);
  const [thickness, setThickness] = useState(4);

  const { svg } = getSvg({
    string: text,
    wrapHeight: height,
    scale,
    padding: 10,
    addSize: true,
    strokeWidth: thickness
  });

  return h(
    "div",
    null,
    h(
      "div",
      { className: "inputFields" },
      h(NumberField, {
        label: "Height",
        value: height,
        min: 200,
        max: 10000,
        onChange: setHeight
      }),
      h(NumberField, {
        label: "Scale",
        value: scale,
        min: 1,
        max: 16,
        onChange: setScale
      }),
      h(NumberField, {
        label: "Thickness",
        value: thickness,
        min: 1,
        max: 16,
        onChange: setThickness
      })
    ),
    h(
      "div",
      { className: "inputOutput" },
      h(TextField, { value: text, onChange: setText }),
      h("div", { dangerouslySetInnerHTML: { __html: svg } })
    )
  );
}

function Draw() {
  const canvasRef = useRef();
  const drawRef = useRef();
  const [text, setText] = useState(
    "Ana are mere, dar cred că poate să aibă și pere. The quick brown fox did some jumping."
  );
  const [height, setHeight] = useState(800);
  const [scale, setScale] = useState(4);
  const [speedAdvance, setSpeedAdvance] = useState(1);
  const [speedSteps, setSpeedSteps] = useState(5);
  const [nibWidth, setNibWidth] = useState(2);
  const [nibHeight, setNibHeight] = useState(1);
  const [showPath, setShowPath] = useState(false);

  return h(
    "div",
    null,
    h(
      "div",
      { className: "inputFields" },
      h(NumberField, {
        label: "Height",
        value: height,
        min: 200,
        max: 10000,
        onChange: setHeight
      }),
      h(NumberField, {
        label: "Scale",
        value: scale,
        min: 1,
        max: 16,
        onChange: setScale
      }),
      h(NumberField, {
        label: "Speed Advance",
        value: speedAdvance,
        min: 0.1,
        max: 20,
        onChange: setSpeedAdvance
      }),
      h(NumberField, {
        label: "Speed Steps",
        value: speedSteps,
        min: 1,
        max: 50,
        onChange: setSpeedSteps
      }),
      h(NumberField, {
        label: "Nib Width",
        value: nibWidth,
        min: 1,
        max: 16,
        onChange: setNibWidth
      }),
      h(NumberField, {
        label: "Nib Height",
        value: nibHeight,
        min: 1,
        max: 16,
        onChange: setNibHeight
      }),
      h(BooleanField, {
        label: "Show Path",
        value: showPath,
        onChange: setShowPath
      })
    ),
    h(
      "div",
      { className: "inputOutput" },
      h(TextField, { value: text, onChange: setText }),
      h(
        "button",
        {
          type: "button",
          onClick: () => {
            if (drawRef.current) {
              drawRef.current();
            }
            drawRef.current = drawOnCanvas({
              canvas: canvasRef.current,
              string: text,
              wrapHeight: height,
              showPath,
              scale,
              speedAdvance,
              speedSteps,
              nibWidth,
              nibHeight
            });
          }
        },
        "Draw"
      ),
      h("canvas", { ref: canvasRef })
    )
  );
}

render(h(App), document.getElementById("root"));
