function copyText(fromEl, toEl) {
  fromEl.addEventListener('keyup', () => {
    toEl.innerHTML = fromEl.innerHTML;
  }, false);
}

function byId(id) {
  return document.getElementById(id);
}

const jurfo = byId('jurfo-block');
const english = byId('english-block');

copyText(jurfo, english);
copyText(english, jurfo);
