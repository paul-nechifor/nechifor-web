(function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == "function" && require;
        if (!u && a) return a(o, !0);
        if (i) return i(o, !0);
        throw new Error("Cannot find module '" + o + "'");
      }
      var f = (n[o] = { exports: {} });
      t[o][0].call(
        f.exports,
        function (e) {
          var n = t[o][1][e];
          return s(n ? n : e);
        },
        f,
        f.exports,
        e,
        t,
        n,
        r,
      );
    }
    return n[o].exports;
  }
  var i = typeof require == "function" && require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
})(
  {
    1: [
      function (require, module, exports) {
        var addListeners, expandQuickContact, main, showProjects;

        main = function () {
          addListeners();
        };

        addListeners = function () {
          document
            .getElementById("expand-quick-contact")
            .addEventListener("mousedown", expandQuickContact);
        };

        expandQuickContact = function () {
          var elems, eqc, i, len;
          elems = document.getElementById("quick-contact").childNodes;
          i = 0;
          len = elems.length;
          while (i < len) {
            if (elems[i].nodeType === Node.ELEMENT_NODE) {
              elems[i].removeAttribute("class");
            }
            i++;
          }
          eqc = document.getElementById("expand-quick-contact");
          return eqc.parentNode.removeChild(eqc);
        };

        showProjects = function () {
          var btn;
          btn = document.getElementById("show-projects-btn");
          btn.parentNode.removeChild(btn);
          document.querySelectorAll(".project-row.hidden").forEach((x) => {
            x.classList.remove("hidden");
          });
        };

        main();
      },
      {},
    ],
  },
  {},
  [1],
);
