# Software Security Projects

My presentations from the Software Security course.

1. [Exploring C vulnerabilities with the Clang Static Analyzer][ss01]

2. [Executable Code Injection][ss02]

3. [Using OpenVAS][ss03]

4. [Using Metasploit][ss04]

Presentations 1, 2, and 3 were created in collaboration with Valentin Schipor.

[ss01]: https://nechifor.net/clang-static-analyzer/
[ss02]: https://nechifor.net/executable-code-injection/
[ss03]: https://nechifor.net/using-openvas/
[ss04]: https://nechifor.net/using-metasploit/
