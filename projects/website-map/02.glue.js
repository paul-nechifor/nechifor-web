const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const sizeOf = util.promisify(require("image-size"));
const { promises: fs } = require("fs");
const _ = require("lodash");

const standardSize = 128;

async function main() {
  const urls = JSON.parse(await fs.readFile("urls.json"));
  await Promise.all(urls.map((x, imageId) => resize(imageId)));
  await join(urls);
  console.log("Done gluing.");
}

async function resize(imageId) {
  await exec(
    `convert screenshots/original/${imageId}.png -resize '${standardSize}x' screenshots/small/${imageId}.png`
  );
}

function splitByHeight(height, items) {
  let used = 0;
  let nGroup = 0;
  const groups = _.groupBy(items, item => {
    if (used + item.height < height) {
      used += item.height;
    } else {
      used = item.height;
      nGroup++;
    }
    return nGroup;
  });
  const heights = Object.values(groups).map(group =>
    _.sum(group.map(x => x.height))
  );
  const maxHeight = _.max(heights);
  const waste = _.sum(heights.map(height => maxHeight - height));
  return { waste, groups };
}

function splitIntoGroups(items) {
  const sum = _.sumBy(items, "height");
  const area = sum * standardSize;
  const startHeight = Math.sqrt(area) | 0;
  const range = _.range(startHeight, (startHeight / 4) | 0, -1);
  const optimal = _.minBy(range, height => splitByHeight(height, items).waste);
  const { groups } = splitByHeight(optimal, items);
  return groups;
}

async function join(urls) {
  const items = await Promise.all(
    urls.map(async (url, imageId) => {
      const { height } = await sizeOf(`screenshots/small/${imageId}.png`);
      return { imageId, url, height };
    })
  );

  const groups = splitIntoGroups(items);

  await Promise.all(
    Object.entries(groups).map(([groupId, items]) => {
      const images = items
        .map(x => `screenshots/small/${x.imageId}.png`)
        .join(" ");
      return exec(
        `convert -append ${images} screenshots/glued/group${groupId}.png`
      );
    })
  );

  const groupImages = _.sortBy(Object.keys(groups), Number)
    .map(x => `screenshots/glued/group${x}.png`)
    .join(" ");
  await exec(`
    convert +append ${groupImages} screenshots/map.png
    convert screenshots/map.png -resize '50%' ../pages/pages/website-map/map.jpg
  `);

  const areas = Object.entries(groups)
    .map(([groupId, items]) => {
      const x = groupId * standardSize;
      let y = 0;
      return items.map(item => {
        const coords = [x, y, x + standardSize, y + item.height];
        y += item.height;
        return {
          href: item.url.replace(/^http:\/\/localhost/g, ""),
          coords: coords.map(x => (x / 2) | 0)
        };
      });
    })
    .flat();

  await fs.writeFile("areas.json", JSON.stringify(areas, null, 2));
}

main();
