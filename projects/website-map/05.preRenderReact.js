const { promises: fs } = require("fs");
const puppeteer = require("puppeteer");
const _ = require("lodash");

async function main() {
  const sites = [
    {
      url: "http://localhost/catenary/",
      file: "../catenary/build/index.html",
    },
    {
      url: "http://localhost/computer/",
      file: "../computer/build/index.html",
    },
    {
      url: "http://localhost/kaomoji/",
      file: "../kaomoji/build/index.html",
    },
    {
      url: "http://localhost/old-instagram/",
      file: "../old-instagram/build/index.html",
    },
    {
      url: "http://localhost/zome/",
      file: "../zome/build/index.html",
    },
  ];

  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();

  for (const { url, file } of sites) {
    await update(page, url, file);
  }

  await browser.close();

  console.log("Done pre rendering react.");
}

async function update(page, url, file) {
  await page.goto(url, { waitUntil: "networkidle0" });
  await new Promise((res) => setTimeout(res, 1000));
  const outerHtml = await page.evaluate(() => {
    return document.querySelector("#root").outerHTML;
  });

  let fileData = await fs.readFile(file, "utf8");
  const replace = /<div id="root">.*<\/div><noscript>/g;
  if (!replace.test(fileData)) {
    throw new Error(`Could not find ${replace} in ${file}`);
  }
  fileData = fileData.replace(replace, outerHtml + "<noscript>");

  await fs.writeFile(file, fileData);
}

main();
