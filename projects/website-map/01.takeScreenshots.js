const puppeteer = require("puppeteer");
const axios = require("axios");
const merge = require("merge-img");
const Jimp = require("jimp");
const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const { promises: fs } = require("fs");
const { parseString } = require("xml2js");
const config = require("./config");
const _ = require("lodash");

const nPresentationPages = 16;
const nParallelPages = 4;

async function main() {
  console.log("Starting", __filename);
  const urls = (await createAndWriteUrls()).map((url, imageId) => ({
    url,
    imageId,
  }));
  await exec(`
    mkdir -p screenshots
    cd screenshots
    mkdir -p original small glued
  `);

  const [photoUrls, pageUrls] = _.partition(urls, ({ url }) =>
    /^http:\/\/localhost\/photography\/./.test(url),
  );
  await Promise.all([takeScreenshots(pageUrls), copyPhotos(photoUrls)]);

  console.log("Done taking.");
}

async function createAndWriteUrls() {
  const siteMapFile = await fs.readFile("../web-base/static/sitemap.xml");
  let urls = (
    await new Promise((res, rej) => {
      parseString(siteMapFile, (err, result) => {
        if (err) {
          rej(err);
        } else {
          res(result);
        }
      });
    })
  ).urlset.url
    .map((x) =>
      x.loc[0].replace(/^https:\/\/nechifor.net/g, "http://localhost"),
    )
    .filter((x) => {
      const path = x.replace(/^http:\/\/localhost/g, "");
      if (path.startsWith("/timr/") && path.indexOf("?") >= 0) {
        return false;
      }
      if (path.startsWith("/sibf/") && path.endsWith(".html")) {
        return false;
      }
      if (
        path.startsWith("/photography/ai-tags/") ||
        path.startsWith("/photography/tags/") ||
        path.startsWith("/rpgadvance/scripts") ||
        path.startsWith("/college-website/proiecte/gravitatie/?")
      ) {
        return false;
      }

      if (
        path.startsWith("/aoire/") &&
        !["/aoire/", "/aoire/59b9271ff55d72023ecd4243.html"].includes(path)
      ) {
        return false;
      }

      return true;
    });

  urls = _.sortBy(urls, (url) => {
    let split = url.split("/");

    if (url.startsWith("http://localhost/rpgadvance")) {
      split.splice(4, 0, split.length, url.indexOf("style=ba_chess") > 0);
    }

    split = [
      url.startsWith("http://localhost/photography/") && split.length >= 6,
      ...split,
    ];

    return split;
  });

  await fs.writeFile("urls.json", JSON.stringify(urls, null, 2));

  return urls;
}

async function resetViewport(page) {
  await page.setViewport({ width: config.pageWidth, height: 1000 });
}

async function resetViewportForPresentation(page) {
  await page.setViewport({ width: 1280, height: 720 });
}

async function takeScreenshots(urls) {
  const chunks = Object.values(
    _.groupBy(urls, ({ imageId }) => imageId % nParallelPages),
  );
  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });

  try {
    await Promise.all(
      chunks.map(async (urls) => {
        const page = await browser.newPage();
        await resetViewport(page);
        await takeScreenshotsBrowser(browser, page, urls);
      }),
    );
  } finally {
    await browser.close();
  }
}

async function takeScreenshotsBrowser(browser, page, urls) {
  const done = new Set(
    (await fs.readdir("screenshots/original"))
      .filter((x) => x.endsWith(".png"))
      .map((x) => parseInt(x, 10)),
  );
  for (const { imageId, url } of urls) {
    if (done.has(imageId)) {
      continue;
    }
    await takeScreenshot(page, url, imageId);
    done.add(imageId);
  }
}

function sleep(ms) {
  return new Promise((res) => setTimeout(res, ms));
}

async function copyPhotos(photos) {
  await Promise.all(photos.map(copyPhoto));
}

async function copyPhoto({ url, imageId }) {
  const parts = url.split("/");
  const name = decodeURI(parts[parts.length - 2]);
  await exec(`
    convert ../photography/dist/images/${name}.jpg -resize 480x -border 16x16 screenshots/original/${imageId}.png
  `);
}

async function takeScreenshot(page, url, imageId) {
  await page.goto(url);
  const html = (await axios.get(url)).data;

  // Wait for Flash to render something.
  if (html.indexOf("@ruffle-rs/ruffle") >= 0) {
    await sleep(3000);
  }

  try {
    await fullPageScreenshot(page, {
      path: `screenshots/original/${imageId}.png`,
    });
  } catch (e) {
    console.error(e);
  }
}

async function fullPageScreenshot(page, options = {}) {
  const { pagesCount, extraPixels, viewport, ruffleEmbed } =
    await page.evaluate(() => {
      window.scrollTo(0, 0);

      const pageHeight = document.documentElement.scrollHeight;

      // If it uses Flash, load the player.
      const ruffleEmbed = document.querySelector("ruffle-embed");
      if (ruffleEmbed) {
        ruffleEmbed.shadowRoot.querySelector("#play-button").click();
      }

      return {
        pagesCount: Math.min(
          Math.ceil(pageHeight / window.innerHeight),
          1 + Math.ceil(6000 / window.innerHeight),
        ),
        extraPixels:
          (pageHeight % window.innerHeight) * window.devicePixelRatio,
        viewport: {
          height: window.innerHeight * window.devicePixelRatio,
          width: window.innerWidth * window.devicePixelRatio,
        },
        ruffleEmbed: !!ruffleEmbed,
      };
    });

  // Wait a little for Flash.
  if (ruffleEmbed) {
    await sleep(3000);
  }

  const images = [];

  const isRevealJs = await page.evaluate(() => {
    return (
      !!document.querySelectorAll(".navigate-right.enabled").length &&
      !!document.querySelectorAll(".reveal").length
    );
  });

  if (isRevealJs) {
    await resetViewportForPresentation(page);
    let canContinue = true;
    for (let i = 0; i < nPresentationPages && canContinue; i++) {
      await sleep(1000);
      images.push(await page.screenshot({ fullPage: false }));
      canContinue = await page.evaluate(() => {
        const ret = !!document.querySelectorAll(".navigate-right.enabled")
          .length;
        if (ret) {
          document.querySelector(".navigate-right.enabled").click();
        }
        return ret;
      });
    }
    await resetViewport(page);
  } else {
    for (let index = 0; index < pagesCount; index += 1) {
      if (options.delay) {
        await sleep(options.delay);
      }
      const image = await page.screenshot({ fullPage: false });
      await pageDown(page);
      images.push(image);
    }
  }

  if (images.length === 1) {
    const image = await Jimp.read(images[0]);
    if (options.path) {
      image.write(options.path);
    }
    return image;
  }

  if (!isRevealJs) {
    // Crop the extra pixels from the last image.
    const cropped = await Jimp.read(images.pop())
      .then((image) =>
        image.crop(
          0,
          viewport.height - extraPixels,
          viewport.width,
          extraPixels,
        ),
      )
      .then((image) => image.getBufferAsync(Jimp.AUTO));
    images.push(cropped);
  }

  let mergedImage = await merge(images, { direction: true });

  if (options.path) {
    await new Promise((resolve) => {
      mergedImage.write(options.path, () => {
        resolve();
      });
    });

    if (!isRevealJs) {
      await exec(`
        mogrify -crop ${viewport.width}x${config.maxHeight}+0+0 ${options.path}
      `);
    }
  }

  return mergedImage;
}

async function pageDown(page) {
  const isEnd = await page.evaluate(() => {
    window.scrollBy(0, window.innerHeight);
    return (
      window.scrollY >=
      document.documentElement.scrollHeight - window.innerHeight
    );
  });

  return isEnd;
}

main();
