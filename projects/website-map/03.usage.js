const { promises: fs } = require("fs");
const puppeteer = require("puppeteer");
const _ = require("lodash");
const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const nParallelPages = 8;

async function main() {
  const urls = JSON.parse(await fs.readFile("urls.json"));

  const chunks = Object.values(
    _.groupBy(
      urls.map((url, index) => ({ url, index })),
      ({ index }) => index % nParallelPages
    )
  ).map(group => group.map(({ url }) => url));

  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
  });

  let results;

  try {
    results = await Promise.all(
      chunks.map(async urls => {
        const page = await browser.newPage();
        return await processUrls(page, urls);
      })
    );
  } catch (e) {
    console.log(e);
  } finally {
    await browser.close();
  }

  const errors = _.uniq(results.map(({ errors }) => errors).flat());

  if (errors.length) {
    for (const error of errors) {
      console.log(error);
    }
  }

  const [internal, external] = _.partition(
    _.sortBy(
      Object.entries(
        _.groupBy(
          results.map(({ requestedUrls }) => requestedUrls).flat(),
          "url"
        )
      ).map(([url, xs]) => [url, _.sortBy(_.uniq(xs.map(x => x.from)))]),
      x => x[0]
    ),
    x => x[0].startsWith("http://localhost/")
  );

  const requestedUrls = internal
    .map(x => x[0])
    .filter(
      x =>
        !(
          x.startsWith("http://localhost/identitate-falsa/avatare/") ||
          x.startsWith("http://localhost/keygen-radio/tunes/")
        )
    );

  await fs.writeFile(
    "requestedUrls.json",
    JSON.stringify(requestedUrls, null, 2)
  );
  await fs.writeFile("externalUrls.json", JSON.stringify(external, null, 2));

  console.log("Done writting requestedUrls.json.");

  const texts = _.sortBy(
    results.map(({ texts }) => texts).flat(),
    "url"
  ).filter(
    ({ url }) =>
      ![
        "http://localhost/college-website/proiecte/se_termina_in/escu.html",
        "http://localhost/pseudoromana/"
      ].includes(url)
  );

  await fs.writeFile("misspelt.json", JSON.stringify(texts, null, 2));

  const misspeltUnique = _.sortBy(_.uniq(texts.map(x => x.words).flat()));
  await fs.writeFile(
    "misspeltUnique.json",
    JSON.stringify(misspeltUnique, null, 2)
  );
}

async function processUrls(page, urls) {
  const requestedUrls = [];
  const errors = [];
  const texts = [];

  await page.setRequestInterception(true);

  page.on("request", request => {
    if (
      request.url().indexOf("youtube.com/") >= 0 ||
      request.url().indexOf("/@ruffle-rs") >= 0
    ) {
      request.abort("blockedbyclient");
      return;
    }
    const from = page.url().replace(/#\/$/, ""); // Remove #/
    requestedUrls.push({ url: request.url(), from });
    request.continue();
  });

  for (const url of urls) {
    try {
      await page.goto(url, { waitUntil: "networkidle0" });
      const { lang, words } = await page.evaluate(() => {
        function removeNonIndexablePart(html) {
          const start = "googleoff: all";
          const end = "googleon: all";

          for (;;) {
            let startIndex = html.indexOf(start);
            if (startIndex === -1) {
              break;
            }

            let endIndex = html.indexOf(end);
            if (endIndex === -1) {
              throw new Error("Failed to find end.");
            }

            html =
              html.substring(0, startIndex) +
              html.substring(endIndex + end.length);
          }

          return html;
        }

        return {
          lang: document.querySelector("html").getAttribute("lang"),
          words: Array.from(
            new Set(
              removeNonIndexablePart(document.body.innerText)
                .match(/(\d|\p{L}|'|’|-)+/gu)
                .map(x => x.replace(/^['\-]+/g, "").replace(/['\-]+$/g, ""))
            )
          ).sort()
        };
      });
      const allText = words.join(" ").replace('"', "");
      const proc = await exec(
        `echo "${allText}" | aspell --lang=${lang || "en"} list`
      );
      texts.push({
        url,
        lang,
        words: _.sortBy(_.uniq(proc.stdout.trim().split("\n")))
      });
    } catch (e) {
      errors.push(`Error for ${url}: ${e}`);
    }
  }

  return { requestedUrls, errors, texts };
}

main();
