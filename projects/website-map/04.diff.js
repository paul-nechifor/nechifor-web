const util = require("util");
const path = require("path");
const { promises: fs } = require("fs");
const _ = require("lodash");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);

async function main() {
  const proc = await exec(`
    docker run -i --rm nechifor-web find /app -type f
  `);

  const realFiles = _.sortBy(
    proc.stdout
      .trim()
      .split("\n")
      .map(x => x.replace(/^\/app/g, ""))
      .filter(x => {
        if (
          x.startsWith("/identitate-falsa/avatare/") ||
          x.startsWith("/keygen-radio/tunes/")
        ) {
          return false;
        }
        return true;
      })
  );
  const accessedFiles = _.sortBy(
    JSON.parse(await fs.readFile("requestedUrls.json")).map(x =>
      x.replace(/^http:\/\/localhost/g, "")
    )
  );
  const unused = _.difference(realFiles, accessedFiles).filter(x => {
    if (["index.html", "index.php"].includes(path.basename(x))) {
      return false;
    }
    return true;
  });

  await fs.writeFile("unused.json", JSON.stringify(_.sortBy(unused), null, 2));
  console.log("Done writing unused.json.");
}

main();
