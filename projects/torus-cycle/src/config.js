const config = {
    "MOVE_SPEED": 15,
    "CELL_SIZE": 0.1,
    "Computers": {
        "Green": {
            "cells": 1280,
            "circuitTexture": "textures/circuit.png",
            "gridTexture": "textures/grid.png",
            "lightCycleTextures": [
                "textures/light_cycle_01.png",
                "textures/light_cycle_02.png",
                "textures/light_cycle_03.png",
                "textures/light_cycle_04.png"
            ]
        }
    }
};

export {config as default};
