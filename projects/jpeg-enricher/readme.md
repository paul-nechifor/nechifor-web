# JPEG Enricher

Web app for accruing JPEG artifacts. [See it live.][live]

![JPEG Enricher cover.](screenshot.jpg)

## Usage

Start the server and watch for changes:

    npm start

## License

ISC

[live]: http://nechifor.net/jpeg-enricher
