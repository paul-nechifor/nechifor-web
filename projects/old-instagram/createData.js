const { promises: fs } = require("fs");
const mkdirp = require("mkdirp");
const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const spawn = util.promisify(childProcess.spawn);
const sharp = require("sharp");
const _ = require("lodash");

const width = 300;
const height = 300;
const quality = 85;

async function main() {
  const photos = _.orderBy(
    JSON.parse(await fs.readFile("private/media.json")).photos,
    ["taken_at"],
    ["desc"]
  );
  const good = photos.map(({ caption, taken_at, path }) => [
    caption,
    new Date(taken_at).valueOf() / 1000,
  ]);
  await exec("rm -fr public/images && mkdir public/images");
  await Promise.all([
    fs.writeFile("src/images.json", JSON.stringify(good)),
    ...photos.map((x, i) => copyImage(x, i)),
  ]);

  await exec(`
    cd public/images &&
    exiftool -r -overwrite_original -all= *.jpg &&
    exiftool -r -overwrite_original -author="Paul Nechifor <paul@nechifor.net>" -copyright="Paul Nechifor (nechifor.net)" *.jpg
  `);
}

async function copyImage(photo, index) {
  const out = `public/images/${index}.jpg`;
  await exec(`cp private/${photo.path} ${out}`);
  await sharp(out)
    .resize(width, height)
    .sharpen()
    .jpeg({ quality })
    .toFile(`public/images/${index}.thumb.jpg`);
}

main();
