import React, { useRef, useEffect, useReducer, Dispatch, FC } from "react";
import "./App.css";
import rawImages from "./images.json";
import classnames from "classnames";
import { LazyLoadImage } from "react-lazy-load-image-component";

type RawImage = [string, number];

const images = (rawImages as RawImage[]).map(
  (image: RawImage, index: number) => ({
    id: index,
    caption: image[0],
    date: image[1] * 1000,
  })
);

interface Image {
  id: number;
  caption: string;
  date: number;
}

interface State {
  selectedImageId?: number;
}

type Action =
  | { type: "selectImage"; selectedImageId: number }
  | { type: "deselected" };

function chunkify<T extends object>(array: T[], perChunk: number): T[][] {
  return array.reduce((acc: T[][], one: T, i: number) => {
    const ch = Math.floor(i / perChunk);
    acc[ch] = [].concat(acc[ch] || [], one);
    return acc;
  }, []);
}

function getThumbPath(imageId: number) {
  return `images/${imageId}.thumb.jpg`;
}

function getPath(imageId: number) {
  return `images/${imageId}.jpg`;
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "selectImage":
      return { ...state, selectedImageId: action.selectedImageId };
    case "deselected":
      return { ...state, selectedImageId: undefined };
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, {});

  useEffect(() => {
    document.body.classList.toggle(
      "faded",
      state.selectedImageId !== undefined
    );
    return () => {
      document.body.classList.remove("faded");
    };
  }, [state.selectedImageId]);

  return (
    <div className="app">
      <ThumbList images={images} state={state} dispatch={dispatch} />
      {state.selectedImageId !== undefined && (
        <LargeImage image={images[state.selectedImageId]} dispatch={dispatch} />
      )}
    </div>
  );
}

interface LargeImageProps {
  image: Image;
  dispatch: Dispatch<Action>;
}

const LargeImage: FC<LargeImageProps> = ({ dispatch, image }) => {
  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const date = new Date(image.date);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        (ref1.current && !ref1.current.contains(event.target)) ||
        (ref2.current && !ref2.current.contains(event.target))
      ) {
        dispatch({ type: "deselected" });
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [dispatch]);

  return (
    <div className="largeImage">
      <div className="largeImageInner">
        <img
          className="largeImageImg"
          src={getPath(image.id)}
          alt={image.caption}
          ref={ref1}
        />
        <p ref={ref2} className="description">
          {date.toLocaleDateString("en-GB", {
            year: "numeric",
            month: "long",
            day: "numeric",
          })}
          <br />
          {image.caption}
        </p>
      </div>
    </div>
  );
};

interface ThumbListProps {
  images: Image[];
  state: State;
  dispatch: Dispatch<Action>;
}

const ThumbList: FC<ThumbListProps> = ({ images, state, dispatch }) => {
  return (
    <div
      className={classnames("thumbListOuter", {
        blurred: state.selectedImageId !== undefined,
      })}
    >
      <div className="thumbList">
        {chunkify(images, 4).map((chunk) => (
          <ThumbChunk
            key={chunk.map((x) => x.id).join(",")}
            images={chunk}
            dispatch={dispatch}
          />
        ))}
      </div>
    </div>
  );
};

interface ThumbChunkProps {
  images: Image[];
  dispatch: Dispatch<Action>;
}

const ThumbChunk: FC<ThumbChunkProps> = ({ images, dispatch }) => {
  return (
    <div className="thumbChunk">
      {images.map((image) => (
        <Thumb key={image.id} image={image} dispatch={dispatch} />
      ))}
    </div>
  );
};

interface ThumbProps {
  image: Image;
  dispatch: Dispatch<Action>;
}

const Thumb: FC<ThumbProps> = ({ image, dispatch }) => {
  return (
    <div className="thumbOuter">
      <div
        className="thumb"
        onClick={() => {
          dispatch({ type: "selectImage", selectedImageId: image.id });
        }}
      >
        <LazyLoadImage
          src={getThumbPath(image.id)}
          alt={image.caption}
          loading="lazy"
          className="image"
        />
      </div>
    </div>
  );
};

export default App;
