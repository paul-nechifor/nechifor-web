import data from "./data.json";
import sortBy from "lodash/sortBy";
import groupBy from "lodash/groupBy";
import { Kaomoji, RawKaomoji } from "./types";

interface RawStruct {
  [label: string]: RawStruct | string[];
}

function unwrap(thing: RawStruct | string[], labels: string[]): RawKaomoji[] {
  if (thing instanceof Array) {
    return thing.map((value) => ({ value, labels }));
  }

  return Object.entries(thing)
    .map(([label, value]) => unwrap(value, [...labels, label]))
    .flat();
}

function getScore(kaomoji: RawKaomoji): number {
  const { value } = kaomoji;
  let score = value.length;
  const n1 = value.length - 1;
  if (value[0] === value[n1]) {
    score -= 1;
  }
  if (
    (value[0] === "(" || value[1] === "(") &&
    (value[n1 - 1] === ")" || value[n1] === ")")
  ) {
    score -= 1.2;
  }

  // Penalize if there are more than 2 identical characters.
  Object.values(groupBy(value)).forEach((chars) => {
    if (chars.length > 2) {
      score -= 0.3 * chars.length;
    }
  });

  return score;
}

function getSearchString(kaomoji: RawKaomoji): string {
  return kaomoji.labels.join(" ").toLowerCase();
}

export const list: Kaomoji[] = sortBy(
  unwrap(data, []).map((x, id) => ({
    ...x,
    id,
    score: getScore(x),
    search: getSearchString(x),
  })),
  "score"
);

export const chars = sortBy(
  Object.entries(
    groupBy(
      list
        .map((x) => x.value)
        .join()
        .split("")
    )
  )
    .map(
      ([ch, occ]: [string, string[]]) => [ch, occ.length] as [string, number]
    )
    .filter(([ch, occ]) => {
      if (/\w/.test(ch)) {
        return occ > 40;
      }
      return occ >= 4;
    })
    .map((xs) => xs[0])
) as string[];
