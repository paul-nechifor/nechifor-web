import React, {
  MouseEvent,
  useMemo,
  useEffect,
  useReducer,
  useState,
  FC,
} from "react";
import "./App.css";
import { list, chars } from "./kaomoji";
import TabSet from "./TabSet";
import { Kaomoji } from "./types";
import { State, Action } from "./types";

const MAX_ITEMS = 800;

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "changeActiveTab":
      return { ...state, activeTab: action.index };
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, { activeTab: 0 });
  return (
    <div className="app">
      <TabSet
        list={["All", "Search by description", "Search by characters"]}
        active={state.activeTab}
        dispatch={dispatch}
      />
      {state.activeTab === 0 && <KaomojiList list={list} />}
      {state.activeTab === 1 && <ByDescription list={list} />}
      {state.activeTab === 2 && <ByCharacters list={list} chars={chars} />}
    </div>
  );
}

function selectValue(event: MouseEvent<HTMLDivElement>) {
  window.getSelection()?.selectAllChildren(event.currentTarget);
}

interface KaomojiListProps {
  list: Kaomoji[];
}

const KaomojiList: FC<KaomojiListProps> = ({ list }) => {
  const [loadAll, setLoadAll] = useState(() => list.length <= MAX_ITEMS);

  const displayList = useMemo(() => {
    if (loadAll) {
      return list;
    }
    return list.slice(0, MAX_ITEMS);
  }, [list, loadAll]);

  useEffect(() => {
    setLoadAll(list.length <= MAX_ITEMS);
  }, [list]);

  return (
    <>
      <div className="kaomojiList">
        {displayList.map(({ id, value, labels }) => (
          <span key={id}>
            <div
              className="kaomoji"
              onClick={selectValue}
              title={labels.join(", ")}
            >
              {value}
            </div>{" "}
          </span>
        ))}
      </div>
      {!loadAll && (
        <div className="loadMoreButtonHolder">
          <button
            type="button"
            onClick={() => {
              setLoadAll(true);
            }}
          >
            Load the rest
          </button>
        </div>
      )}
    </>
  );
};

interface ByDescriptionProps {
  list: Kaomoji[];
}

const ByDescription: FC<ByDescriptionProps> = ({ list }) => {
  const [results, setResults] = useState<Kaomoji[]>([]);
  return (
    <div>
      <div className="searchBarHolder">
        <input
          type="text"
          placeholder="Search..."
          autoFocus
          className="searchBar"
          onChange={(e) => {
            const value = e.target.value.trim().toLowerCase();
            if (value.length <= 1) {
              setResults([]);
              return;
            }
            const regex = new RegExp(`\\b${value}`);
            setResults(list.filter((x) => regex.test(x.search)));
          }}
        />
      </div>
      <KaomojiList list={results} />
    </div>
  );
};

interface ByCharactersProps {
  list: Kaomoji[];
  chars: string[];
}

const ByCharacters: FC<ByCharactersProps> = ({ list, chars }) => {
  const [results, setResults] = useState<Kaomoji[]>([]);
  return (
    <div>
      <div className="searchBarHolder">
        <input
          type="text"
          placeholder="Search..."
          autoFocus
          className="searchBar"
          onChange={(e) => {
            const value = e.target.value.trim().toLowerCase();
            if (value.length === 0) {
              setResults([]);
              return;
            }
            setResults(
              list.filter((x) =>
                value.split("").every((ch) => x.value.indexOf(ch) >= 0)
              )
            );
          }}
        />
      </div>
      <KaomojiList list={results} />
      <div className="chars">
        Common characters:
        {chars.map((ch) => (
          <span key={ch}>
            <div className="char">{ch}</div>{" "}
          </span>
        ))}
      </div>
    </div>
  );
};

export default App;
