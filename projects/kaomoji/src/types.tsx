export interface RawKaomoji {
  value: string;
  labels: string[];
}
export interface Kaomoji extends RawKaomoji {
  id: number;
  score: number;
  search: string;
}

export interface State {
  activeTab: number;
}

export type Action = { type: "changeActiveTab"; index: number };
