import "./TabSet.css";
import classnames from "classnames";
import React, { Dispatch, FC } from "react";

import { Action } from "./types";

interface TabSetProps {
  list: string[];
  active: number;
  dispatch: Dispatch<Action>;
}

const TabSet: FC<TabSetProps> = ({ list, active, dispatch }) => {
  return (
    <div className="tabSet">
      {list.map((label, index) => (
        <div
          className={classnames("tab", { tabActive: active === index })}
          key={label}
          onClick={() => {
            dispatch({ type: "changeActiveTab", index });
          }}
        >
          {label}
        </div>
      ))}
    </div>
  );
};

export default TabSet;
