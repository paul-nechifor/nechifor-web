# Fake Identity

Plausible fake identity generator (in Romanian).

![Screenshot of identitate falsa (fake identity) generator.](screenshot.png)

## Usage

Install the packages:

    yarn

Start the server and watch for changes:

    yarn start

Build it:

    yarn build

## License

ISC
