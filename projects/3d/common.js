const { promises: fs } = require("fs");
const glob = require("glob");
const path = require("path");
const yaml = require("yaml");
const _ = require("lodash");

async function getInfos() {
  const infoPaths = await glob('objects/**/info.yml');
  const infos = await Promise.all(infoPaths.map(async (yml) => ({
    ...yaml.parse(await fs.readFile(yml, 'utf8')),
    slug: path.basename(path.dirname(yml))
  })));
  return _.orderBy(infos, info => info.date, ['desc']);
}

exports.getInfos = getInfos;
