const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);
const sharp = require("sharp");
const { getInfos } = require("./common");

async function main() {
  await exec(`
    rm -fr tmp &&
    mkdir -p tmp dist/download dist/images dist/thumbs dist/covers
  `);
  const infos = await getInfos();
  await Promise.all([buildFiles(), buildObjects(infos)]);
  await exec(`rm -fr tmp`);
}

async function buildFiles() {
  await exec(`
    cd objects
    for x in *; do
      (
        cd "$x";
        (
          cd "download"
          touch -t 199001010000  *
          find . -type f -print0 | sort -z | xargs -0 zip -rX ../../../dist/download/"$x".zip
        )
        if [ -e images ]; then
          cp -r images ../../dist/images/"$x"
        fi
      )
    done
  `);
}

async function buildObjects(infos) {
  await Promise.all(
    infos.map(async (info) => {
      await writeImage(
        `objects/${info.slug}/background.jpg`,
        `dist/thumbs/${info.slug}.jpg`,
        400,
        300,
        85,
      );
      await writeImage(
        `objects/${info.slug}/background.jpg`,
        `dist/covers/${info.slug}.jpg`,
        null,
        null,
        90,
      );
    }),
  );
}

async function writeImage(src, dst, width, height, quality) {
  await sharp(src)
    .resize(width, height)
    .sharpen()
    .jpeg({ quality, optimizeScans: true })
    .toFile(dst);
}

main();
