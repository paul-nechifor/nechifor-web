const gulp = require("gulp");
const path = require("path");
const { promises: fs } = require("fs");
const htmlmin = require("gulp-htmlmin");
const rename = require("gulp-rename");
const pug = require("gulp-pug");
const webserver = require("gulp-webserver");
const yaml = require("yaml");
const { getInfos } = require("./common");

gulp.task("default", ["html", "webserver", "watch"]);

gulp.task("build", ["html"]);

gulp.task("html", async () => {
  const infos = await getInfos();
  gulp.src("index.pug")
    .pipe(pug({ locals: { infos }}))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
  gulp.src("cameras.pug")
    .pipe(pug({ locals: { infos: infos.filter(x => x.isCamera) }}))
    .pipe(rename('index.html'))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist/cameras'));

  infos.forEach(info => {
    const src = `objects/${info.slug}/index.pug`;
    return gulp.src(src)
      .pipe(pug({ locals: { info } }))
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest(path.dirname(src).replace(/^objects/g, "dist")));
    });
});


gulp.task("webserver", () => {
  const port = parseInt(process.env.port || "3000", 10);
  return gulp.src(".").pipe(
    webserver({
      livereload: true,
      open: true,
      directoryListing: true,
      port,
      host: "localhost"
    })
  );
});

gulp.task("watch", () => {
  return gulp.watch("objects/*/*.pug", ["html"]);
});
