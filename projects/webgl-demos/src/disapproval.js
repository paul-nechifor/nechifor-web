import webglWorld from "webgl-world";
const { Disapproval } = webglWorld.examples;
Disapproval.loadResources("../disapproval", () => {
  new Disapproval({}).start();
});
