const { promises: fs } = require("fs");
const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);

async function main() {
  const proc = await exec(`
    rm -fr dist builtJs
    mkdir -p builtJs
    cp -r static dist
    for name in src/*; do
      code=$(basename $name .js)
      webpack --entry "./src/$code.js" -o "builtJs/$code" --mode production
    done
  `);
  console.log(proc.stdout);
  console.log(proc.stderr);
}

main();
