const gulp = require("gulp");
const path = require("path");
const fs = require("fs");
const htmlmin = require("gulp-htmlmin");
const rename = require("gulp-rename");
const pug = require("gulp-pug");
const webserver = require("gulp-webserver");


const demos = [
  {code: "disapproval", name: "Disapproval"},
  {code: "moire", name: "Moire"},
  {code: "plasma", name: "Plasma"},
  {code: "triangles", name: "Triangles"},
];

gulp.task("default", async () => {
  gulp.src("index.pug")
    .pipe(pug({ locals: { demos }}))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));

  demos.forEach(({code, name}) => {
    const js =  fs.readFileSync(`builtJs/${code}/main.js`, 'utf8');
    return gulp.src('demo.pug')
      .pipe(pug({ locals: { js } }))
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(rename('index.html'))
      .pipe(gulp.dest(`dist/${code}`));
  });
});
