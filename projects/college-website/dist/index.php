<?php
require 'include/antet.php';
afiseazaAntet('Prima pagină', '', '');
?>

<h2 id="t_prima_pagina">Prima Pagină</h2>

<p><span class="B"><span>B</span></span>ine ai venit pe <a href="http://sudociteste.blogspot.com/2008/10/despre-cuvntul-sait.html">saitul</a> personal a lui Paul Nechifor. Aici poţi afla informaţii despre mine şi programele mele în ActionScript, PHP şi altele. Poţi să vezi o colecţie de imagini (găsite pe internet) cu păduri şi copaci. Dacă vrei să afli şi alte chestii poţi să vorbeşti cu <a href="proiecte/paulbot/">PaulBot</a>, purtătorul meu de cuvânt online. Sau poţi să aflii cum să mă contactezi (promit că nu sunt greu de abordat :D ).</p>
<h4>Ce-i nou</h4>
<ul style="list-style:none">
	<li><b>19.11.2008</b> - am terminat tot, la limită</li>
	<li><b>18.11.2008</b> - am început şi terminat de făcut galeria de imagini cu CSS</li>
	<li><b>17.11.2008</b> - am adăugat aproape toate proiectele pe care le aveam pe fostul meu sait personal</li>
	<li><b>16.11.2008</b> - am început să-l învăţ pe PaulBot</li>
    <li><b>01.11.2008</b> - mi-am făcut un server pe calculator pentru a începe lucrul în PHP</li>
	<li><b>27.10.2008</b> - am început să scriu codul XHTML pentru sait şi să fac o primă pagină drept exemplu</li>
	<li><b>19.10.2008</b> - am început să fac primul template pentru sait în Photoshop</li>
	<li><b>10.10.2008</b> - am creat blogul şi am scris primul articol</li>
</ul>

<?php
require 'include/subsol.php';
afiseazaSubsol();
?>
