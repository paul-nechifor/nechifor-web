<?php
require 'include/antet.php';
afiseazaAntet('Proiecte', '', '');
?>

<h2 id="t_proiecte">Proiecte</h2>

<p><span class="P"><span>P</span></span>rogramarea este pasiunea mea. &#350;tiu cateva limbaje de programare (ActionScript, Python, PHP, C/C++, Pascal), dar nici unul foarte bine. &Icirc;mi place s&#259; aflu cum func&#355;ioneaz&#259; lucrurile &#351;i c&acirc;nd v&#259;d ceva vreau s&#259; &icirc;ncerc s&#259; fac &#351;i eu (de asta am imitat jocuri de gen Bomberman, Breakout, etc). Deseori &icirc;mi vin idei de programe, jocuri, scripturi, imagini pe care s&#259; le fac. Problema e c&#259;-mi vin prea multe idei &#351;i dac&#259; &icirc;ncep un proiect &#351;i sunt un pic satisf&#259;cut -- renun&#355;. Majoritatea lucrurilor pe care le &icirc;ncep nu le termin, &icirc;n multe cazuri pentru c&#259; nu am timp sau nu am timp s&#259; &icirc;nv&#259;&#355; ce &icirc;mi trebuie ca s&#259; contiuu. Nu v&#259; a&#351;tepta&#355;i ca ceva de aici s&#259; mearg&#259; perfect.</p>
<h4>ActionScript 2.0</h4>
<ul class="fara">
<li><a href="proiecte/sisyphusudoku/">SisyphuSudoku</a> - g&#259;se&#351;te solu&#355;ia unui joc de Sudoku.</li>
<li><a href="proiecte/gravitatie/">Gravitatie</a> - o simpl&#259; simulare de gravita&#355;ie, cum se atrag planetele</li>
</ul>

<h4>PHP</h4>
<ul class="fara">
<li><a rel="nofollow" href="proiecte/name_selection/">Name selection</a> - genereaz&#259; aleatoriu o list&#259; &#351;i un arbore de nume</li>
<li><a href="proiecte/name_generator/">Name generator</a> - genereaz&#259; aleatoriu o list&#259; mai simpl&#259; de nume</li>
<li><a href="proiecte/shuffle_letters/">Shuffle letters</a> - amestec&#259; literele dintr-un cuv&acirc;nt</li>
</ul>

<h4>Python</h4>
<ul class="fara">
<li><a href="proiecte/se_termina_in/">Se termin&#259; &icirc;n...</a> - o list&#259; de nume de familie &#351;i prenume &#351;i un script cum s&#259; alegi nume dup&#259; sufix</li>
<li><a href="proiecte/phpbb_users/">phpbb Users</a> - scoate numele de utilizator de pe un forum de tip phpbb</li>
</ul>

<h4>Alte saituri</h4>
<ul class="fara">
<li><a href="/italia-fascista/introducere.html">Italia fascist&#259;</a> - Saitul care l-am folosit la atestat &icirc;n 2008, dar l-am creat &icirc;n 2007 pentru un concurs cu saituri despre istorie</li>
</ul>

<h4>Imagini</h4>
<ul class="fara">
<li><a href="proiecte/bancnota666/">Bancnota de 666 lei</a> - o bancnot&#259;  f&#259;cut&#259; &icirc;n glum&#259; dup&#259; ce a fost scoas&#259; bancnota de 200 de lei</li>
</ul>

<h4>Chestii neterminate</h4>
<ul class="fara">
<li><a href="proiecte/buggy/bombman/">Bombman</a> - un joc de tip Bomberman</li>
<li><a href="proiecte/buggy/paddle/">Paddle</a> - un joc de tip Breakout</li>
<li><a href="proiecte/buggy/tetravex/">Tetravex</a> - un joc de tip Tetravex</li>
<li><a href="proiecte/buggy/friendlymaze/">Friendly Maze</a> - un joc &icirc;n care trebuie s&#259; navighezi un labirint cu mausul</li>
</ul>

<?php
require 'include/subsol.php';
afiseazaSubsol();
?>
