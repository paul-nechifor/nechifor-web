const fs = require('fs');
const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const pug = require('gulp-pug');
const webserver = require('gulp-webserver');

gulp.task('html', done => {
  const images = fs.readdirSync('images').map(x => x.split('.')[0]);
  const imageGroups = [...new Array(((images.length / 7) | 0 ) + 1)].map(
    (_, i) => images.slice(7 * i, 7 * (i + 1))
  );
  gulp.src('index.pug')
    .pipe(pug({locals: {imageGroups}}))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
  done();
});

gulp.task('webserver', done => {
  const port = parseInt(process.env.port || '8080', 10);
  gulp.src('dist')
    .pipe(webserver({ livereload: true, open: true, port, host: '0.0.0.0' }));
  done();
});

gulp.task('watch', done => {
  gulp.watch('index.pug', gulp.series('html'));
  done();
});

gulp.task('build', gulp.series('html'));

gulp.task('default', gulp.series('html', 'webserver', 'watch'));
