import { useEffect, useRef } from "react";
import { Info, NumberValues } from "./types";
import World from "./World";
import renderZome from "./renderZome";

interface ViewerProps {
  panelSize: number;
  numberValues: NumberValues;
  setInfo(info: Info): void;
}

export default function Viewer({
  panelSize,
  numberValues,
  setInfo,
}: ViewerProps) {
  const ref = useRef<HTMLDivElement>(null);
  const worldRef = useRef<World | null>(null);

  useEffect(() => {
    function handleResize() {
      if (worldRef.current) {
        worldRef.current.updateSize(
          window.innerWidth - panelSize,
          window.innerHeight,
        );
      }
    }

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [panelSize]);

  useEffect(() => {
    const div = ref.current;
    if (div) {
      const world = new World(
        window.innerWidth - panelSize,
        window.innerHeight,
      );
      worldRef.current = world;
      world.start(div);
    }

    return () => {
      if (worldRef.current) {
        worldRef.current.stop();
      }
      if (div) {
        div.innerHTML = "";
      }
    };
  }, [panelSize]);

  useEffect(() => {
    if (worldRef.current) {
      worldRef.current.clearScene();
      // TODO render stuff
      renderZome({ setInfo, numberValues, world: worldRef.current });
      worldRef.current.triggerRender();
    }
  }, [numberValues, setInfo]);

  return <div ref={ref} />;
}
