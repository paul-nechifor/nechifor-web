import { RenderArgs } from "./types";

import {
  BufferGeometry,
  BufferAttribute,
  LineBasicMaterial,
  Line,
  Vector3,
} from "three";
import World from "./World";
import { zomesFromHelices, getHelices } from "./func";

export default function renderZome({
  setInfo,
  numberValues: nv,
  world,
}: RenderArgs) {
  if (nv.n % 2 === 0) {
    nv.n++;
  }

  if (nv.nUse > nv.n) {
    nv.nUse = nv.n;
  }

  const originalHeight = getHelices({
    d: nv.diagonal,
    nSamples: nv.n,
    nUseSamples: nv.nUse,
    multi: 1,
  })[0][0].y;

  const helices = getHelices({
    d: nv.diagonal,
    nSamples: nv.n,
    nUseSamples: nv.nUse,
    multi: nv.height / originalHeight,
  });

  setInfo({
    "actual n": nv.n,
    "actual n use": nv.nUse,
    "total full faces": (nv.nUse - 2) * (nv.n - 1),
    "total half faces": nv.n - 1,
    "face length": helices[0][0].distanceTo(helices[0][1]).toFixed(3),
  });

  if (nv.showSkeleton) {
    drawHelices(world, helices);
  }

  const zomes = zomesFromHelices(nv, helices, world.textures);
  for (const zome of zomes) {
    world.addObject(zome);
  }
}

function drawHelices(world: World, helices: Vector3[][]) {
  for (let i = 0; i < helices.length; i++) {
    const helix = helices[i];
    for (let j = 0; j < helix.length - 1; j++) {
      drawLine(world, [helix[j], helix[j + 1]], 0xff0000);
      if (j > 0) {
        drawLine(
          world,
          [helix[j], helices[(i + 1) % helices.length][j + 1]],
          0xff0000,
        );
      }
    }
  }
}

function drawLine(world: World, points: Vector3[], color: number) {
  const geometry = new BufferGeometry();
  const vertices = new Float32Array(points.flatMap((p) => [p.x, p.y, p.z]));
  geometry.setAttribute("position", new BufferAttribute(vertices, 3));
  const material = new LineBasicMaterial({ color, depthTest: false });
  const line = new Line(geometry, material);
  world.addObject(line);
}
