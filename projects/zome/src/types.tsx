import World from "./World";

export interface Info {
  [key: string]: number | string;
}

interface NumberValue {
  initial: number;
  min: number;
  max: number;
  defaultValue: number;
}

interface BooleanValue {
  initial: boolean;
  defaultValue: boolean;
}

export interface ConfigItem {
  id: string;
  name: string;
  value: NumberValue | BooleanValue;
}

export interface NumberValues {
  [key: string]: number;
}

export interface RenderArgs {
  setInfo(info: Info): void;
  numberValues: NumberValues;
  world: World;
}
