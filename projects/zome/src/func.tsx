import {
  BufferGeometry,
  Mesh,
  Material,
  MeshPhongMaterial,
  MeshStandardMaterial,
  BufferAttribute,
  Vector3,
} from "three";
import { NumberValues } from "./types";

export function getHelices({
  d,
  nSamples,
  nUseSamples,
  multi,
}: {
  d: number;
  nSamples: number;
  nUseSamples: number;
  multi: number;
}): Vector3[][] {
  if (nSamples % 2 !== 1) {
    throw new Error("nSamples mush be odd.");
  }

  const n = nSamples - 1;

  return [...new Array(n)].map((_, i) => {
    return [...new Array(nUseSamples)].map((_, j) => {
      const alpha = Math.PI - (Math.PI / Math.ceil((nSamples - 1) / 2)) * j;
      const beta = ((2 * Math.PI) / n) * i;
      return new Vector3(
        (Math.sin(alpha + beta) * d) / 4 + (Math.sin(beta) * d) / 4,
        multi *
          (((alpha / Math.PI) * d) / 2 +
            d / 2 -
            (d / (nSamples - 1)) * (nSamples - nUseSamples)),
        (Math.cos(alpha + beta) * d) / 4 + (Math.cos(beta) * d) / 4,
      );
    });
  });
}

export function getNormal(p0: Vector3, p1: Vector3, p2: Vector3): Vector3 {
  const normal = new Vector3();
  const v1 = p1.clone().sub(p0);
  const v2 = p2.clone().sub(p0);
  normal.crossVectors(v1, v2).normalize();
  return normal;
}

export function extrudeZome(
  p: Vector3[],
  extrusionLength: number,
): BufferGeometry {
  const normal = getNormal(p[0], p[1], p[2]);

  const vertices = new Float32Array([
    p[0].x,
    p[0].y,
    p[0].z,

    p[1].x,
    p[1].y,
    p[1].z,

    p[2].x,
    p[2].y,
    p[2].z,

    p[3].x,
    p[3].y,
    p[3].z,

    p[0].x + normal.x * extrusionLength,
    p[0].y + normal.y * extrusionLength,
    p[0].z + normal.z * extrusionLength,

    p[1].x + normal.x * extrusionLength,
    p[1].y + normal.y * extrusionLength,
    p[1].z + normal.z * extrusionLength,

    p[2].x + normal.x * extrusionLength,
    p[2].y + normal.y * extrusionLength,
    p[2].z + normal.z * extrusionLength,

    p[3].x + normal.x * extrusionLength,
    p[3].y + normal.y * extrusionLength,
    p[3].z + normal.z * extrusionLength,
  ]);

  const indices = new Uint16Array([
    0, 2, 1, 0, 3, 2,

    4, 5, 6, 4, 6, 7,

    0, 1, 4, 4, 1, 5,

    1, 2, 5, 5, 2, 6,

    2, 3, 6, 6, 3, 7,

    3, 4, 7, 3, 0, 4,
  ]);

  const uvs = new Float32Array(
    new Array(12).fill(0).flatMap(() => [0.5, 0, 0, 0.5, 0.5, 1, 1, 0.5]),
  );

  const geometry = new BufferGeometry();
  geometry.setAttribute("position", new BufferAttribute(vertices, 3));
  geometry.setIndex(new BufferAttribute(indices, 1));
  geometry.setAttribute("uv", new BufferAttribute(uvs, 2));
  geometry.computeVertexNormals();
  return geometry;
}

export function extrudeHalfZome(
  p: Vector3[],
  extrusionLength: number,
): BufferGeometry {
  const normal = getNormal(p[0], p[1], p[2]);

  const vertices = new Float32Array([
    p[0].x,
    p[0].y,
    p[0].z,

    p[1].x,
    p[1].y,
    p[1].z,

    p[2].x,
    p[2].y,
    p[2].z,

    p[0].x + normal.x * extrusionLength,
    p[0].y + normal.y * extrusionLength,
    p[0].z + normal.z * extrusionLength,

    p[1].x + normal.x * extrusionLength,
    p[1].y + normal.y * extrusionLength,
    p[1].z + normal.z * extrusionLength,

    p[2].x + normal.x * extrusionLength,
    p[2].y + normal.y * extrusionLength,
    p[2].z + normal.z * extrusionLength,
  ]);

  const indices = new Uint16Array([
    0, 2, 1,

    3, 4, 5,

    0, 1, 3, 3, 1, 4,

    1, 2, 4, 4, 2, 5,

    2, 3, 5, 2, 0, 3,
  ]);

  const uvs = new Float32Array(
    new Array(12).fill(0).flatMap(() => [0.5, 0, 0, 0.5, 0.5, 1, 1, 0.5]),
  );

  const geometry = new BufferGeometry();
  geometry.setAttribute("position", new BufferAttribute(vertices, 3));
  geometry.setIndex(new BufferAttribute(indices, 1));
  geometry.setAttribute("uv", new BufferAttribute(uvs, 2));
  geometry.computeVertexNormals();
  return geometry;
}

function towards(p1: Vector3, p2: Vector3, d: number): Vector3 {
  let direction = new Vector3().subVectors(p2, p1).normalize();
  direction.multiplyScalar(d);
  return new Vector3().addVectors(p1, direction);
}

function extrude4Braces(
  zome: Vector3[],
  nv: NumberValues,
  materials: Material[],
): Mesh[] {
  const w = nv.woodWidth / Math.sin(zomeAngle(zome));
  const z = zome;

  const z03 = towards(z[0], z[3], w);
  const z12 = towards(z[1], z[2], w);

  const z10 = towards(z[1], z[0], w);
  const z23 = towards(z[2], z[3], w);

  const z21 = towards(z[2], z[1], w);
  const z30 = towards(z[3], z[0], w);

  const z32 = towards(z[3], z[2], w);
  const z01 = towards(z[0], z[1], w);

  const brace1 = [z01, z[1], z12, towards(z03, z12, w)];
  const brace2 = [z12, z[2], z23, towards(z10, z23, w)];
  const brace3 = [z23, z[3], z30, towards(z21, z30, w)];
  const brace4 = [z01, towards(z32, z01, w), z30, z[0]];

  return [
    new Mesh(extrudeZome(brace1, nv.woodHeight), materials[0]),
    new Mesh(extrudeZome(brace2, nv.woodHeight), materials[1]),
    new Mesh(extrudeZome(brace3, nv.woodHeight), materials[2]),
    new Mesh(extrudeZome(brace4, nv.woodHeight), materials[3]),
  ];
}

function extrude3Braces(
  zome: Vector3[],
  nv: NumberValues,
  materials: Material[],
): Mesh[] {
  const angle = zomeAngle(zome);
  const w = nv.woodWidth / Math.sin(angle);
  const w2 = 2 * nv.woodWidth * Math.sin(angle / 2);

  const z = zome;

  const p0 = towards(z[0], z[1], w);
  const p1 = towards(z[1], z[2], w2);
  const p2 = towards(towards(z[0], z[2], w), p1, w);
  const p3 = towards(z[2], z[1], w2);

  const brace1 = [p0, z[1], p1, p2];
  const brace2 = [p1, p3, towards(p3, p2, w2), towards(p1, p2, w2)];
  const brace3 = [p0, p3, z[2], z[0]];
  return [
    new Mesh(extrudeZome(brace1, nv.woodHeight), materials[0]),
    new Mesh(extrudeZome(brace2, nv.woodHeight), materials[1]),
    new Mesh(extrudeZome(brace3, nv.woodHeight), materials[3]),
  ];
}

export function zomesFromHelices(
  nv: NumberValues,
  helices: Vector3[][],
  textures: any,
): Mesh[] {
  const boardMaterial = new MeshStandardMaterial({
    map: textures.osb,
    transparent: true,
    opacity: nv.opacity,
  });
  const bracesMaterials = [0x63462d, 0xb88c2f, 0x9a6e4a, 0xb47449].map(
    (color) =>
      new MeshPhongMaterial({
        color,
        flatShading: true,
      }),
  );
  const wireMaterial = new MeshPhongMaterial({
    color: 0xffffff,
    wireframe: true,
  });
  const zomes = [];

  // 4 point zomes,
  for (let i = 0; i < nv.nUse - 2; i++) {
    for (let j = 0; j < nv.n - 1; j++) {
      zomes.push([
        helices[j][i],
        helices[j][i + 1],
        helices[(j + 1) % (nv.n - 1)][i + 2],
        helices[(j + 1) % (nv.n - 1)][i + 1],
      ]);
    }
  }

  let i = nv.nUse - 2;

  // Half zomes (3 point zomes).
  for (let j = 0; j < nv.n - 1; j++) {
    zomes.push([
      helices[j][i],
      helices[j][i + 1],
      helices[(j + 1) % (nv.n - 1)][i + 1],
    ]);
  }

  const ret = zomes.flatMap((zome) =>
    toFullZome(zome, nv, boardMaterial, wireMaterial, bracesMaterials),
  );

  ret.push(
    ...toFlatZomes(zomes, nv, boardMaterial, wireMaterial, bracesMaterials),
  );

  return ret;
}

function toFlatZomes(
  zomes: Vector3[][],
  nv: NumberValues,
  boardMaterial: Material,
  wireMaterial: Material,
  bracesMaterials: Material[],
): Mesh[] {
  const ret = [];
  const flatStart = new Vector3(nv.diagonal / 2 + 1, 0, 0);

  let startX = 0;
  let startY = 0;
  let restartX = 0;
  let restartY = 0;

  for (let i = 0; i < nv.nUse - 2; i++) {
    for (let j = 0; j < nv.n - 1; j++) {
      const zome = zomes[i * (nv.n - 1)];
      const angle = zomeAngle(zome);
      const dist = zome[0].distanceTo(zome[1]);

      const ps = [[startX, startY]];

      const ang1 = -angle;
      ps.push([
        ps[0][0] + Math.cos(ang1) * dist,
        ps[0][1] + Math.sin(ang1) * dist,
      ]);
      const ang2 = 0;
      ps.push([
        ps[1][0] + Math.cos(ang2) * dist,
        ps[1][1] + Math.sin(ang2) * dist,
      ]);
      const ang3 = -angle + Math.PI;
      ps.push([
        ps[2][0] + Math.cos(ang3) * dist,
        ps[2][1] + Math.sin(ang3) * dist,
      ]);

      const flatZome = [];

      for (let p of ps) {
        const z = new Vector3(p[0], 0, p[1]);
        z.addVectors(z, flatStart);
        flatZome.push(z);
      }

      ret.push(
        ...toFullZome(
          flatZome,
          nv,
          boardMaterial,
          wireMaterial,
          bracesMaterials,
        ),
      );

      startX = ps[3][0];
      startY = ps[3][1];

      if (j === 0) {
        restartX = ps[1][0];
        restartY = ps[1][1];
      }
    }

    startX = restartX;
    startY = restartY;
  }

  for (let j = 0; j < nv.n - 1; j++) {
    const zome = zomes[(nv.nUse - 2) * (nv.n - 1) + j];

    const angle = zomeAngle(zome);
    const dist = zome[0].distanceTo(zome[1]);

    const ps = [[startX, startY]];

    const ang1 = -angle;
    ps.push([
      ps[0][0] + Math.cos(ang1) * dist,
      ps[0][1] + Math.sin(ang1) * dist,
    ]);
    const ang2 = 0;
    ps.push([
      ps[1][0] + Math.cos(ang2) * dist,
      ps[1][1] + Math.sin(ang2) * dist,
    ]);
    const ang3 = -angle + Math.PI;
    ps.push([
      ps[2][0] + Math.cos(ang3) * dist,
      ps[2][1] + Math.sin(ang3) * dist,
    ]);

    ps.splice(2, 1);

    const flatZome = [];

    for (let p of ps) {
      const z = new Vector3(p[0], 0, p[1]);
      z.addVectors(z, flatStart);
      flatZome.push(z);
    }

    ret.push(
      ...toFullZome(flatZome, nv, boardMaterial, wireMaterial, bracesMaterials),
    );

    startX = ps[2][0];
    startY = ps[2][1];
  }

  return ret;
}

function zomeAngle(zome: Vector3[]) {
  let A, B, C;
  if (zome.length === 4) {
    A = zome[0];
    B = zome[1]; // The middle.
    C = zome[2];
  } else {
    A = zome[1];
    B = zome[0]; // The middle.
    C = zome[2];
  }

  const AB = new Vector3().subVectors(A, B).normalize();
  const CB = new Vector3().subVectors(C, B).normalize();
  const dotProduct = AB.dot(CB); // Ensure -1 <= dotProduct <= 1 to avoid NaN
  return Math.PI - Math.acos(Math.max(-1, Math.min(1, dotProduct)));
}

function toFullZome(
  zome: Vector3[],
  nv: NumberValues,
  boardMaterial: Material,
  wireMaterial: Material,
  bracesMaterials: Material[],
): Mesh[] {
  const offset = getNormal(zome[0], zome[1], zome[2]).multiplyScalar(
    nv.woodHeight,
  );
  // Move the zome OSB up because the frame is on the inside and the OSB on the outside.
  const offsetZome = zome.map((x) => new Vector3().addVectors(x, offset));
  const geom =
    zome.length === 4
      ? extrudeZome(offsetZome, nv.panelThickness)
      : extrudeHalfZome(offsetZome, nv.panelThickness);

  const ret: Mesh[] = [new Mesh(geom, boardMaterial)];

  if (nv.showSkeleton) {
    ret.push(new Mesh(geom, wireMaterial));
  }

  const meshes =
    zome.length === 4
      ? extrude4Braces(zome, nv, bracesMaterials)
      : extrude3Braces(zome, nv, bracesMaterials);

  for (let mesh of meshes) {
    ret.push(mesh);
  }

  return ret;
}
