import {
  Line,
  Group,
  AmbientLight,
  Color,
  DirectionalLight,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  Clock,
  Mesh,
  MeshPhongMaterial,
} from "three";

import * as THREE from "three";
import CameraControls from "camera-controls";
import threeCsg from "./threeCsg.js";
import { STLLoader } from "three/examples/jsm/loaders/STLLoader.js";

CameraControls.install({ THREE: THREE });

var cylinder = threeCsg.toCSG(
  new THREE.CylinderGeometry(1.0, 1.0, 2.0, 16, 4, false),
  new THREE.Vector3(0, -1.0, 0),
);
var sphere = threeCsg.toCSG(new THREE.SphereGeometry(1.0, 16, 12));
var geometry = cylinder.union(sphere);
var mesh = new THREE.Mesh(
  threeCsg.fromCSG(geometry),
  new MeshPhongMaterial({ color: 0xffceb4 }),
);

const textureLoader = new THREE.TextureLoader();

export default class World {
  scene: Scene;

  camera: PerspectiveCamera;

  renderer: WebGLRenderer;

  cameraControls: CameraControls;

  clock: Clock;

  parts: (Mesh | Group | Line)[];

  shouldStop: boolean = false;

  man: Mesh | null = null;

  textures = {
    osb: textureLoader.load("osb.jpg"),
  };

  constructor(width: number, height: number) {
    this.scene = new Scene();
    this.scene.background = new Color(0xe1e1e1);

    const fov = 35;
    const aspect = width / height;
    const near = 0.3;
    const far = 1000;
    this.camera = new PerspectiveCamera(fov, aspect, near, far);
    this.camera.position.set(0, 6, 8);
    this.camera.rotateZ(Math.PI * 0.2);

    this.renderer = new WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.scene.add(new AmbientLight(0xffffff, 1));
    const light2 = new DirectionalLight(0xffffff, 1.4);
    light2.position.set(0, 10, 0);
    light2.target.position.set(-5, -4, -3);
    this.scene.add(light2);
    this.scene.add(light2.target);

    this.renderer.render(this.scene, this.camera);

    this.cameraControls = new CameraControls(
      this.camera,
      this.renderer.domElement,
    );

    this.clock = new Clock();
    this.parts = [];

    this.loadMan();
  }

  start(div: HTMLDivElement) {
    div.append(this.renderer.domElement);

    const anim = () => {
      if (this.shouldStop) {
        return;
      }
      const delta = this.clock.getDelta();
      const hasControlsUpdated = this.cameraControls.update(delta);
      requestAnimationFrame(anim);
      if (hasControlsUpdated) {
        this.renderer.render(this.scene, this.camera);
      }
    };

    anim();
  }

  stop() {
    this.shouldStop = true;
  }

  addObject(object: Mesh | Group | Line) {
    this.parts.push(object);
    this.scene.add(object);
  }

  clearScene() {
    for (const part of this.parts) {
      this.scene.remove(part);
    }
  }

  updateSize(width: number, height: number) {
    this.renderer.setSize(width, height, true);
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.renderer.render(this.scene, this.camera);
  }

  triggerRender() {
    this.renderer.render(this.scene, this.camera);
  }

  loadMan() {
    const loader = new STLLoader();
    loader.load(
      "man.stl",
      (geometry) => {
        const material = new MeshPhongMaterial({ color: 0xffceb4 });
        this.man = new Mesh(geometry, material);
        this.scene.add(this.man);
        this.man.rotateX(-Math.PI / 2);
        this.triggerRender();
      },
      undefined,
      (error) => {
        console.error("An error happened", error);
      },
    );
  }
}
