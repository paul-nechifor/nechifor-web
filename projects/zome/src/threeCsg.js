/*
	THREE.CSG
	@author Chandler Prall <chandler.prall@gmail.com> http://chandler.prallfamily.com
	
	Wrapper for Evan Wallace's CSG library (https://github.com/evanw/csg.js/)
	Provides CSG capabilities for Three.js models.
	
	Provided under the MIT License
*/

import CSG from "./csg.js";
import * as THREE from "three";

export default {
  toCSG: function (three_model, offset, rotation) {
    let geometry,
      polygons = [],
      rotation_matrix;

    if (!CSG) {
      throw "CSG library not loaded. Please get a copy from https://github.com/evanw/csg.js";
    }

    if (three_model instanceof THREE.Mesh) {
      geometry = three_model.geometry;
      offset = offset || three_model.position;
      rotation = rotation || three_model.rotation;
    } else if (three_model instanceof THREE.BufferGeometry) {
      geometry = three_model;
      offset = offset || new THREE.Vector3(0, 0, 0);
      rotation = rotation || new THREE.Euler(0, 0, 0);
    } else {
      throw "Model type not supported.";
    }

    rotation_matrix = new THREE.Matrix4().makeRotationFromEuler(rotation);

    const positionAttribute = geometry.attributes.position;
    const normalAttribute = geometry.attributes.normal;
    const index = geometry.index ? geometry.index.array : null;

    for (
      let i = 0;
      i < (index ? index.length : positionAttribute.count);
      i += 3
    ) {
      const vertices = [];
      for (let j = 0; j < 3; j++) {
        const idx = index ? index[i + j] : i + j;
        const pos = new THREE.Vector3()
          .fromBufferAttribute(positionAttribute, idx)
          .add(offset)
          .applyMatrix4(rotation_matrix);
        const normal = normalAttribute
          ? new THREE.Vector3().fromBufferAttribute(normalAttribute, idx)
          : new THREE.Vector3(0, 0, 1); // fallback normal if not present
        vertices.push(new CSG.Vertex(pos, [normal.x, normal.y, normal.z]));
      }
      polygons.push(new CSG.Polygon(vertices));
    }

    return CSG.fromPolygons(polygons);
  },

  fromCSG: function (csg_model) {
    const polygons = csg_model.toPolygons();
    const positions = [];
    const normals = [];
    const indices = [];
    let index = 0;

    for (let i = 0; i < polygons.length; i++) {
      const polygon = polygons[i];
      const vertices = polygon.vertices;

      if (vertices.length >= 3) {
        const normal = polygon.plane.normal;

        for (let j = 2; j < vertices.length; j++) {
          positions.push(
            vertices[0].pos.x,
            vertices[0].pos.y,
            vertices[0].pos.z,
          );
          positions.push(
            vertices[j - 1].pos.x,
            vertices[j - 1].pos.y,
            vertices[j - 1].pos.z,
          );
          positions.push(
            vertices[j].pos.x,
            vertices[j].pos.y,
            vertices[j].pos.z,
          );

          normals.push(normal.x, normal.y, normal.z);
          normals.push(normal.x, normal.y, normal.z);
          normals.push(normal.x, normal.y, normal.z);

          indices.push(index, index + 1, index + 2);
          index += 3;
        }
      }
    }

    const three_geometry = new THREE.BufferGeometry();
    three_geometry.setAttribute(
      "position",
      new THREE.Float32BufferAttribute(positions, 3),
    );
    three_geometry.setAttribute(
      "normal",
      new THREE.Float32BufferAttribute(normals, 3),
    );
    three_geometry.setIndex(indices);

    three_geometry.computeBoundingBox();
    three_geometry.computeVertexNormals();

    return three_geometry;
  },
  getGeometryVertice: function (geometry, vertice_position) {
    var i;
    for (i = 0; i < geometry.vertices.length; i++) {
      if (
        geometry.vertices[i].x === vertice_position.x &&
        geometry.vertices[i].y === vertice_position.y &&
        geometry.vertices[i].z === vertice_position.z
      ) {
        // Vertice already exists
        return i;
      }
    }

    geometry.vertices.push(
      new THREE.Vector3(
        vertice_position.x,
        vertice_position.y,
        vertice_position.z,
      ),
    );
    return geometry.vertices.length - 1;
  },
};
