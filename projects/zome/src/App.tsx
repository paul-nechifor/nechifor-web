import { useState, useMemo } from "react";
import TextField from "@mui/material/TextField";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import Viewer from "./Viewer";
import { ConfigItem, Info } from "./types";
import InfoBox from "./InfoBox";

const panelSize = 350;
const panelPadding = 15;
const panelItemSize = panelSize - 2 * panelPadding;

const config: ConfigItem[] = [
  {
    id: "n",
    name: "n",
    value: {
      initial: 11,
      min: 5,
      max: 30,
      defaultValue: 5,
    },
  },
  {
    id: "nUse",
    name: "n use",
    value: {
      initial: 8,
      min: 5,
      max: 30,
      defaultValue: 5,
    },
  },
  {
    id: "height",
    name: "Height",
    value: {
      initial: 3,
      min: 1,
      max: 30,
      defaultValue: 3,
    },
  },
  {
    id: "diagonal",
    name: "Diagonal",
    value: {
      initial: 5,
      min: 1,
      max: 30,
      defaultValue: 3,
    },
  },
  {
    id: "woodWidth",
    name: "Wood width",
    value: {
      initial: 0.036,
      min: 0.01,
      max: 0.1,
      defaultValue: 0.01,
    },
  },
  {
    id: "woodHeight",
    name: "Wood height",
    value: {
      initial: 0.062,
      min: 0.01,
      max: 0.1,
      defaultValue: 0.01,
    },
  },
  {
    id: "panelThickness",
    name: "Panel Thickness",
    value: {
      initial: 0.02,
      min: 0.001,
      max: 1,
      defaultValue: 0.02,
    },
  },
  {
    id: "opacity",
    name: "Opacity",
    value: {
      initial: 0.8,
      min: 0.0,
      max: 1,
      defaultValue: 0.0,
    },
  },
  {
    id: "showSkeleton",
    name: "Show Skeleton",
    value: {
      initial: true,
      defaultValue: true,
    },
  },
];

export default function App() {
  const [info, setInfo] = useState<Info>({});
  const [values, setValues] = useState(() => resetValues(config));
  const numberValues = useMemo(
    () =>
      Object.fromEntries(
        config.map((x) => [
          x.id,
          typeof x.value.defaultValue === "boolean"
            ? +values[x.id]
              ? 1
              : 0
            : Math.min(
                x.value.max,
                Math.max(x.value.min, +values[x.id] || x.value.defaultValue),
              ),
        ]),
      ),
    [values],
  );

  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          width: panelSize,
          flexGrow: 0,
          overflow: "scroll",
          height: "100vh",
          overflowX: "hidden",
        }}
      >
        <header>
          <h1>
            <a href=".">Zome Planner</a>
          </h1>
          <p style={{ margin: `10px ${panelPadding}px 0` }}>
            Helps out to plan building a zome.
          </p>
        </header>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {config.map((x) =>
            typeof x.value.defaultValue !== "boolean" ? (
              <TextField
                key={x.id}
                label={x.name}
                InputLabelProps={{ shrink: true }}
                size="small"
                sx={{ minWidth: panelItemSize, paddingBottom: "5px" }}
                variant="filled"
                value={values[x.id]}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  setValues({ ...values, [x.id]: event.target.value });
                }}
              />
            ) : (
              <FormControlLabel
                key={x.id}
                control={
                  <Switch
                    checked={!!+values[x.id]}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setValues({
                        ...values,
                        [x.id]: event.target.checked ? "1" : "0",
                      });
                    }}
                  />
                }
                label={x.name}
              />
            ),
          )}
          <InfoBox info={info} width={panelItemSize} />
        </div>
        <footer>
          <p>
            Created by <a href="/">Paul Nechifor</a>.
          </p>
          <small>
            <a href="/">Home</a> • <a href="/projects/">Projects</a> •{" "}
            <a href="https://gitlab.com/paul-nechifor/nechifor-web/-/tree/master/projects/zome/">
              Git source
            </a>
          </small>
        </footer>
      </div>
      <div style={{ flexGrow: 1 }}>
        <Viewer
          panelSize={panelSize}
          numberValues={numberValues}
          setInfo={setInfo}
        />
      </div>
    </div>
  );
}

function resetValues(conf: ConfigItem[]) {
  return Object.fromEntries(conf.map((x) => [x.id, `${x.value.initial}`]));
}
