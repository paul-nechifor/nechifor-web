import { useEffect, useRef } from "react";
import partial from "lodash/partial";
import { catenary, findA } from "./func";
import { NumberValues } from "./types";

interface MiniMapProps {
  size: number;
  numberValues: NumberValues;
}

export default function MiniMap({
  size,
  numberValues: { wantedWidth, wantedHeight, manHeight, brickWidth },
}: MiniMapProps) {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const w = size;
  const h = size;

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) {
      return;
    }
    const ctx = canvas.getContext("2d");
    if (!ctx) {
      return;
    }

    const a = findA(wantedWidth, wantedHeight);
    const func = partial(catenary, a);

    const read = wantedWidth / 2;
    const scale = 0.9 * Math.min(h / wantedHeight, w / wantedWidth);
    const offsetX = w / 2;
    const offsetY = (h - wantedHeight * scale) / 2;

    ctx.fillStyle = "#e1e1e1";
    ctx.fillRect(0, 0, w, h);

    const manPixelHeight = manHeight * scale;
    const manPixelWidth = manPixelHeight / 3;
    drawMan(
      ctx,
      offsetX - manPixelWidth / 2,
      offsetY + wantedHeight * scale - manPixelHeight,
      manPixelWidth,
      manPixelHeight,
    );

    ctx.strokeStyle = "#000";
    ctx.lineWidth = scale * brickWidth;
    ctx.beginPath();
    const increment = 0.1;
    for (let x = -read; x <= read + increment; x += increment) {
      const y = func(x);
      const px = x * scale + offsetX;
      const py = y * scale + offsetY;
      if (x === -read) {
        ctx.moveTo(px, py);
      } else {
        ctx.lineTo(px, py);
      }
    }
    ctx.stroke();
  }, [wantedWidth, wantedHeight, manHeight, brickWidth, w, h]);

  return (
    <canvas
      ref={canvasRef}
      width={w}
      height={h}
      style={{ display: "block", margin: "0 auto 5px", borderRadius: 5 }}
    />
  );
}

function drawMan(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  w: number,
  h: number,
): void {
  const headRadius = h * 0.08;
  const bodyTop = headRadius * 2.5;
  ctx.beginPath();
  ctx.fillStyle = "#000";
  ctx.arc(x + w / 2, y + headRadius, headRadius, 0, 2 * Math.PI, false);
  ctx.fill();

  const armWidth = 0.16;
  const legWidth = 0.22;
  const legOffset = 0.23;
  const armLength = 0.4;

  ctx.fillRect(x, y + bodyTop, w * armWidth, h * 0.4);
  ctx.fillRect(
    x + w * (1 - armWidth),
    y + bodyTop,
    w * armWidth,
    h * armLength,
  );

  ctx.fillRect(x + w * legOffset, y + bodyTop, w * legWidth, h - bodyTop);
  ctx.fillRect(
    x + w * (1 - legOffset - legWidth),
    y + bodyTop,
    w * legWidth,
    h - bodyTop,
  );
  ctx.fillRect(x, y + bodyTop, w, h * 0.08);
  ctx.fillRect(
    x + w * legOffset,
    y + bodyTop,
    w - 2 * w * legOffset,
    h * armLength,
  );
}
