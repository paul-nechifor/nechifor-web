import { BoxGeometry, Mesh, MeshPhongMaterial, Group } from "three";

import { getAngleAt, findA, getArchPositions } from "./func";
import { RenderBricksArgs, NumberValues } from "./types";
import World from "./World";

export function renderBricksAlternating({
  setInfo,
  numberValues: nv,
  world,
}: RenderBricksArgs) {
  world.setManHeight(nv.manHeight, nv.subterraneanDepth);
  addGround(world, nv);

  const a = findA(nv.wantedWidth, nv.wantedHeight);
  const nLengthBricks = Math.ceil(
    (nv.wantedLength + nv.brickSpacing) / (nv.brickLength + nv.brickSpacing),
  );
  const pos = getArchPositions(
    nv.wantedWidth,
    nv.wantedHeight,
    a,
    nv.brickHeight,
    nv.brickSpacing,
    nv.brickWidth,
    true,
  );

  for (let k = 0; k < nLengthBricks; k++) {
    let i = 0;
    for (const { x, y, r } of pos) {
      let z =
        (nv.brickLength + nv.brickSpacing) * k + (nv.brickLength / 2) * (i % 2);
      const isFirstShortBrick = k === 0 && !(i % 2);
      if (isFirstShortBrick) {
        z += nv.brickLength / 4;
      }
      const isLastShortBrick = k === nLengthBricks - 1 && !!(i % 2);
      if (isLastShortBrick) {
        z -= nv.brickLength / 4;
      }

      let brickWidth = nv.brickWidth;
      if (nv.brickTaper < 1) {
        brickWidth *= 1 - (y / nv.wantedHeight) * (1 - nv.brickTaper);
      }
      world.addObject(
        makeBrick({
          x,
          y,
          z,
          brickWidth,
          brickHeight: nv.brickHeight,
          brickLength: nv.brickLength,
          r,
          isHalf: isFirstShortBrick || isLastShortBrick,
        }),
      );
      i++;
    }
  }

  const brickVolume = nv.brickHeight * nv.brickWidth * nv.brickLength;
  const nBricks = (nLengthBricks - 0.5) * pos.length;

  const info: { [k: string]: string | number } = {
    a: a,
    "Number of bricks": nBricks,
    "Actual length":
      ((nv.brickLength + nv.brickSpacing) * (nLengthBricks - 0.5)).toFixed(2) +
      "\xa0m",
    "Bricks in arch": pos.length,
    "Number of arches": nLengthBricks,
    "Base brick angle": (
      -getAngleAt(a, -nv.wantedWidth / 2) *
      (180 / Math.PI)
    ).toFixed(2),
  };

  if (nv.brickTaper >= 1) {
    info["Brick volume"] = brickVolume.toFixed(4) + "\xa0m³";
    info["Total brick volume"] = (brickVolume * nBricks).toFixed(4) + "\xa0m³";
  }

  setInfo(info);
}

export function renderBricksNubian({
  setInfo,
  numberValues: nv,
  world,
}: RenderBricksArgs) {
  world.setManHeight(nv.manHeight, nv.subterraneanDepth);
  addGround(world, nv);

  const incline = nv.inclination * (Math.PI / 180);
  const archHeight = nv.wantedHeight / Math.cos(incline);
  const archAdvance = archHeight * Math.sin(incline);
  const a = findA(nv.wantedWidth, archHeight);
  const brickZUsage = nv.brickLength / Math.cos(incline) + nv.brickSpacing;
  let nLengthBricks = Math.ceil(nv.wantedLength / brickZUsage);
  if (nLengthBricks % 2 === 1) {
    nLengthBricks++;
  }
  const posMiddle = getArchPositions(
    nv.wantedWidth,
    archHeight,
    a,
    nv.brickHeight,
    nv.brickSpacing,
    nv.brickWidth,
    true,
  );
  const posNoMiddle = getArchPositions(
    nv.wantedWidth,
    archHeight,
    a,
    nv.brickHeight,
    nv.brickSpacing,
    nv.brickWidth,
    false,
  );

  for (let k = 0; k < nLengthBricks; k++) {
    const g = new Group();
    const g2 = new Group();
    const pos = k % 2 === 0 ? posMiddle : posNoMiddle;
    for (const { x, y, r } of pos) {
      let group = g;

      if (y * Math.sin(incline) + brickZUsage * k < archAdvance) {
        group = g2;
      }
      group.add(
        makeBrick({
          x,
          y,
          z: 0,
          brickWidth: nv.brickWidth,
          brickHeight: nv.brickHeight,
          brickLength: nv.brickLength,
          r,
          isHalf: false,
        }),
      );
    }
    g.position.setZ(brickZUsage * k - archAdvance);
    g.rotateX(incline);
    g2.position.setZ(
      brickZUsage * k + brickZUsage * nLengthBricks - archAdvance,
    );
    g2.rotateX(incline);
    world.addObject(g);
    world.addObject(g2);
  }

  const brickVolume = nv.brickHeight * nv.brickWidth * nv.brickLength;
  const nBricks =
    (nLengthBricks / 2) * posMiddle.length +
    (nLengthBricks / 2) * posNoMiddle.length;

  setInfo({
    a: a,
    "Number of bricks": nBricks,
    "Actual length": (brickZUsage * nLengthBricks).toFixed(2) + "\xa0m",
    "Bricks in even arch": posNoMiddle.length,
    "Bricks in odd arch": posMiddle.length,
    "Number of arches": nLengthBricks,
    "Brick volume": brickVolume.toFixed(4) + "\xa0m³",
    "Total brick volume": (brickVolume * nBricks).toFixed(4) + "\xa0m³",
  });
}

interface BrickArg {
  x: number;
  y: number;
  z: number;
  brickWidth: number;
  brickHeight: number;
  brickLength: number;
  r?: number;
  isHalf: boolean;
}

function addGround(world: World, nv: NumberValues) {
  const cement = new MeshPhongMaterial({ color: 0x777777 });
  const floor = new Mesh(
    new BoxGeometry(
      nv.wantedWidth + nv.brickWidth,
      nv.brickWidth,
      nv.wantedLength,
    ),
    cement,
  );
  floor.position.set(
    0,
    -nv.subterraneanDepth - nv.brickWidth / 2,
    nv.wantedLength / 2,
  );
  world.addObject(floor);

  const wall1 = new Mesh(
    new BoxGeometry(nv.brickWidth, nv.subterraneanDepth, nv.wantedLength),
    cement,
  );
  wall1.position.set(
    nv.wantedWidth / 2,
    -nv.subterraneanDepth / 2,
    nv.wantedLength / 2,
  );
  world.addObject(wall1);

  const wall2 = new Mesh(
    new BoxGeometry(nv.brickWidth, nv.subterraneanDepth, nv.wantedLength),
    cement,
  );
  wall2.position.set(
    -nv.wantedWidth / 2,
    -nv.subterraneanDepth / 2,
    nv.wantedLength / 2,
  );
  world.addObject(wall2);

  const grassHeight = 0.05;
  const grassWidth = 2;
  const grass = new MeshPhongMaterial({ color: 0x00aa00 });
  const grass1 = new Mesh(
    new BoxGeometry(grassWidth, grassHeight, nv.wantedLength),
    grass,
  );
  grass1.position.set(
    -(nv.wantedWidth + grassWidth + nv.brickWidth) / 2,
    -grassHeight / 2,
    nv.wantedLength / 2,
  );
  world.addObject(grass1);
  const grass2 = new Mesh(
    new BoxGeometry(grassWidth, grassHeight, nv.wantedLength),
    grass,
  );
  grass2.position.set(
    (nv.wantedWidth + grassWidth + nv.brickWidth) / 2,
    -grassHeight / 2,
    nv.wantedLength / 2,
  );
  world.addObject(grass2);
}

function makeBrick({
  x,
  y,
  z,
  brickWidth,
  brickHeight,
  brickLength,
  r,
  isHalf,
}: BrickArg) {
  const geometry = new BoxGeometry(
    brickWidth,
    brickHeight,
    isHalf ? brickLength / 2 : brickLength,
  );
  const material = new MeshPhongMaterial({ color: randomBrickColor() });
  const cube = new Mesh(geometry, material);
  cube.position.set(x, y, z);
  cube.rotateZ(r || 0);
  return cube;
}

function randomBrickColor() {
  const h = 15 + Math.random() * 30;
  const s = 30 + Math.random() * 40;
  const l = 35 + Math.random() * 30;
  return `hsl(${h}, ${s}%, ${l}%)`;
}
