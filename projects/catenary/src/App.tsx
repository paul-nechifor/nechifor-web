import { useState, useMemo } from "react";
import InputLabel from "@mui/material/InputLabel";

import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Select, { SelectChangeEvent } from "@mui/material/Select";

import MenuItem from "@mui/material/MenuItem";
import Viewer from "./Viewer";
import { ConfigItem, Info } from "./types";
import InfoBox from "./InfoBox";
import MiniMap from "./MiniMap";
import { renderBricksAlternating, renderBricksNubian } from "./brickLayouts";

const panelSize = 350;
const panelPadding = 15;
const panelItemSize = panelSize - 2 * panelPadding;

type VaultType = "alternating" | "nubian";

const renderTypes = {
  alternating: renderBricksAlternating,
  nubian: renderBricksNubian,
};

const common: ConfigItem[] = [
  {
    id: "wantedWidth",
    name: "Wanted width",
    initial: 7,
    min: 1,
    max: 100,
    defaultValue: 1,
  },
  {
    id: "wantedHeight",
    name: "Wanted height",
    initial: 1,
    min: 1,
    max: 100,
    defaultValue: 1,
  },
  {
    id: "wantedLength",
    name: "Wanted length",
    initial: 6,
    min: 1,
    max: 100,
    defaultValue: 1,
  },
  {
    id: "manHeight",
    name: "Man height",
    initial: 1.75,
    min: 0.5,
    max: 3,
    defaultValue: 1,
  },
  {
    id: "brickSpacing",
    name: "Brick spacing",
    initial: 0.0,
    min: 0.0,
    max: 0.2,
    defaultValue: 0.0,
  },
  {
    id: "subterraneanDepth",
    name: "Subterranean Depth",
    initial: 1.0,
    min: 0.0,
    max: 10,
    defaultValue: 0.0,
  },
];

const config: { alternating: ConfigItem[]; nubian: ConfigItem[] } = {
  alternating: [
    ...common,
    {
      id: "brickWidth",
      name: "Brick width",
      initial: 0.4,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "brickHeight",
      name: "Brick height",
      initial: 0.1,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "brickLength",
      name: "Brick length",
      initial: 0.4,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "brickTaper",
      name: "Brick taper",
      initial: 1,
      min: 0.2,
      max: 1,
      defaultValue: 1,
    },
  ],
  nubian: [
    ...common,
    {
      id: "brickWidth",
      name: "Brick width",
      initial: 0.3,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "brickHeight",
      name: "Brick height",
      initial: 0.3,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "brickLength",
      name: "Brick length",
      initial: 0.05,
      min: 0.02,
      max: 1,
      defaultValue: 0.2,
    },
    {
      id: "inclination",
      name: "Inclination",
      initial: 25,
      min: 0,
      max: 60,
      defaultValue: 0,
    },
  ],
};

export default function App() {
  const [info, setInfo] = useState<Info>({});
  const [type, setType] = useState<VaultType>("alternating");
  const [values, setValues] = useState(() => resetValues(config[type]));
  const numberValues = useMemo(
    () =>
      Object.fromEntries(
        config[type].map((x) => [
          x.id,
          Math.min(x.max, Math.max(x.min, +values[x.id] || x.defaultValue)),
        ]),
      ),
    [values, type],
  );

  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          width: panelSize,
          flexGrow: 0,
          overflow: "scroll",
          height: "100vh",
          overflowX: "hidden",
        }}
      >
        <header>
          <h1>
            <a href=".">Catenary Vault Planner</a>
          </h1>
          <p style={{ margin: `10px ${panelPadding}px 0` }}>
            Helps out to plan building a vault made out of mudbricks.
          </p>

          <FormControl sx={{ m: 1, marginTop: 2 }} size="small">
            <InputLabel id="vault-type">Vault Type</InputLabel>
            <Select
              id="valut-type"
              value={type}
              label="Vault Type"
              sx={{ minWidth: panelItemSize, paddingBottom: "5px" }}
              onChange={(event: SelectChangeEvent) => {
                const newType = event.target.value as VaultType;
                setType(newType);
                setValues(resetValues(config[newType]));
              }}
            >
              <MenuItem value="alternating">Alternating</MenuItem>
              <MenuItem value="nubian">Nubian</MenuItem>
            </Select>
          </FormControl>
        </header>
        <MiniMap size={panelItemSize} numberValues={numberValues} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {config[type].map((x) => (
            <TextField
              key={x.id}
              label={x.name}
              InputLabelProps={{ shrink: true }}
              size="small"
              sx={{ minWidth: panelItemSize, paddingBottom: "5px" }}
              variant="filled"
              value={values[x.id]}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setValues({ ...values, [x.id]: event.target.value });
              }}
            />
          ))}
          <InfoBox info={info} width={panelItemSize} />
        </div>
        <footer>
          <p>
            Created by <a href="/">Paul Nechifor</a>.
          </p>
          <small>
            <a href="/">Home</a> • <a href="/projects/">Projects</a> •{" "}
            <a href="https://gitlab.com/paul-nechifor/nechifor-web/-/tree/master/projects/catenary/">
              Git source
            </a>
          </small>
        </footer>
      </div>
      <div style={{ flexGrow: 1 }}>
        <Viewer
          panelSize={panelSize}
          numberValues={numberValues}
          setInfo={setInfo}
          renderBricks={renderTypes[type]}
        />
      </div>
    </div>
  );
}

function resetValues(conf: ConfigItem[]) {
  return Object.fromEntries(conf.map((x) => [x.id, `${x.initial}`]));
}
