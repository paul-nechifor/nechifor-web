import {
  Group,
  AmbientLight,
  Color,
  DirectionalLight,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  Clock,
  Mesh,
  MeshPhongMaterial,
} from "three";
import { STLLoader } from "three/examples/jsm/loaders/STLLoader.js";

import * as THREE from "three";
import CameraControls from "camera-controls";

CameraControls.install({ THREE: THREE });

export default class World {
  scene: Scene;

  camera: PerspectiveCamera;

  renderer: WebGLRenderer;

  cameraControls: CameraControls;

  clock: Clock;

  parts: (Mesh | Group)[];

  shouldStop: boolean = false;

  man: Mesh | null = null;

  constructor(width: number, height: number) {
    this.scene = new Scene();
    this.scene.background = new Color(0xe1e1e1);

    const fov = 35;
    const aspect = width / height;
    const near = 0.3;
    const far = 100;
    this.camera = new PerspectiveCamera(fov, aspect, near, far);
    this.camera.position.set(0, 2, 14);

    this.renderer = new WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.scene.add(new AmbientLight(0xffffff, 1));
    const light2 = new DirectionalLight(0xffffff, 1.4);
    light2.position.set(0, 10, 0);
    light2.target.position.set(-5, -4, -3);
    this.scene.add(light2);
    this.scene.add(light2.target);

    this.renderer.render(this.scene, this.camera);

    this.cameraControls = new CameraControls(
      this.camera,
      this.renderer.domElement,
    );

    this.clock = new Clock();
    this.parts = [];

    this.loadMan();
  }

  loadMan() {
    const loader = new STLLoader();
    loader.load(
      "man.stl",
      (geometry) => {
        // const material = new MeshNormalMaterial();
        const material = new MeshPhongMaterial({ color: 0xffceb4 });
        this.man = new Mesh(geometry, material);
        this.scene.add(this.man);
        this.man.rotateX(-Math.PI / 2);
        this.triggerRender();
      },
      undefined,
      (error) => {
        console.error("An error happened", error);
      },
    );
  }

  start(div: HTMLDivElement) {
    div.append(this.renderer.domElement);

    const anim = () => {
      if (this.shouldStop) {
        return;
      }
      const delta = this.clock.getDelta();
      const hasControlsUpdated = this.cameraControls.update(delta);
      requestAnimationFrame(anim);
      if (hasControlsUpdated) {
        this.renderer.render(this.scene, this.camera);
      }
    };

    anim();
  }

  stop() {
    this.shouldStop = true;
  }

  addObject(object: Mesh | Group) {
    this.parts.push(object);
    this.scene.add(object);
  }

  clearScene() {
    for (const part of this.parts) {
      this.scene.remove(part);
    }
  }

  updateSize(width: number, height: number) {
    this.renderer.setSize(width, height, true);
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.renderer.render(this.scene, this.camera);
  }

  triggerRender() {
    this.renderer.render(this.scene, this.camera);
  }

  setManHeight(height: number, depth: number): void {
    if (!this.man) {
      return;
    }

    const scale = height / 1.75;

    this.man.scale.set(scale, scale, scale);
    this.man.position.setY(-depth);
  }
}
