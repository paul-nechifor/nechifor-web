import { useEffect, useRef } from "react";
import { Info, NumberValues, RenderBricksArgs } from "./types";
import World from "./World";

interface ViewerProps {
  panelSize: number;
  numberValues: NumberValues;
  setInfo(info: Info): void;
  renderBricks(args: RenderBricksArgs): void;
}

export default function Viewer({
  panelSize,
  numberValues,
  setInfo,
  renderBricks,
}: ViewerProps) {
  const ref = useRef<HTMLDivElement>(null);
  const worldRef = useRef<World | null>(null);

  useEffect(() => {
    function handleResize() {
      if (worldRef.current) {
        worldRef.current.updateSize(
          window.innerWidth - panelSize,
          window.innerHeight,
        );
      }
    }

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [panelSize]);

  useEffect(() => {
    const div = ref.current;
    if (div) {
      const world = new World(
        window.innerWidth - panelSize,
        window.innerHeight,
      );
      worldRef.current = world;
      world.start(div);
    }

    return () => {
      if (worldRef.current) {
        worldRef.current.stop();
      }
      if (div) {
        div.innerHTML = "";
      }
    };
  }, [panelSize]);

  useEffect(() => {
    if (worldRef.current) {
      worldRef.current.clearScene();
      renderBricks({ setInfo, numberValues, world: worldRef.current });
      worldRef.current.triggerRender();
    }
  }, [numberValues, setInfo, renderBricks]);

  return <div ref={ref} />;
}
