import World from "./World";

export interface Info {
  [key: string]: number | string;
}

export interface ConfigItem {
  id: string;
  name: string;
  initial: number;
  min: number;
  max: number;
  defaultValue: number;
}

export interface NumberValues {
  [key: string]: number;
}

export interface RenderBricksArgs {
  setInfo(info: Info): void;
  numberValues: NumberValues;
  world: World;
}
