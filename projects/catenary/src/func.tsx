export function catenary(a: number, x: number): number {
  return a * Math.cosh(x / a) - a;
}

export function findA(w: number, h: number): number {
  let left = 0.3;
  let right = 1000;
  let middle = 1;

  for (let i = 0; i < 30; i++) {
    middle = (right + left) / 2;
    const middleHeight = catenary(middle, w / 2);
    if (Math.abs(h - middleHeight) < 0.0000001) {
      return middle;
    }
    if (middleHeight < h) {
      right = middle;
    } else {
      left = middle;
    }
  }
  return middle;
}

function getSpacingDistance(
  a: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  brickHeight: number,
  brickWidth: number,
): number {
  const a1 = getAngleAt(a, x1);
  const a2 = getAngleAt(a, x2);

  if (y2 === Infinity) {
    return Infinity;
  }

  // Move point 1 up by half the brick height.
  let x1new = x1 + (brickHeight / 2) * Math.cos(a1);
  let y1new = y1 + (brickHeight / 2) * Math.sin(a1);

  // Move the point 1 right by half the brick width.
  x1new += (brickWidth / 2) * Math.cos(a1 + Math.PI / 2);
  y1new += (brickWidth / 2) * Math.sin(a1 + Math.PI / 2);

  // Move point 2 down by half the brick height.
  let x2new = x2 + (brickHeight / 2) * Math.cos(a2 + Math.PI);
  let y2new = y2 + (brickHeight / 2) * Math.sin(a2 + Math.PI);

  // Move the point 2 right by half the brick width.
  x2new += (brickWidth / 2) * Math.cos(a2 + Math.PI / 2);
  y2new += (brickWidth / 2) * Math.sin(a2 + Math.PI / 2);

  let ret = Math.sqrt(Math.pow(y2new - y1new, 2) + Math.pow(x2new - x1new, 2));

  if (x1new > x2new) {
    ret = -ret;
  }

  return ret;
}

export function getIncrement(
  a: number,
  x1: number,
  brickHeight: number,
  brickWidth: number,
  brickSpacing: number,
): number {
  const y1 = catenary(a, x1);
  let totalInc = 0;

  for (let k = 0; k < 20; k++) {
    let inc = k % 2 === 0 ? Number.EPSILON : -Number.EPSILON;
    let i;

    for (i = 0; i < 500; i++) {
      let x2 = x1 + totalInc + inc;
      let y2 = catenary(a, x2);
      const dist = getSpacingDistance(
        a,
        x1,
        y1,
        x2,
        y2,
        brickHeight,
        brickWidth,
      );
      if (k % 2 === 0 ? dist > brickSpacing : dist < brickSpacing) {
        break;
      }
      inc *= 2;
    }

    totalInc += inc;
  }

  return totalInc;
}

export function getNoMiddleBrickStart(
  a: number,
  brickHeight: number,
  brickWidth: number,
  brickSpacing: number,
): number {
  let start = brickHeight + brickWidth;

  for (let k = 0; k < 20; k++) {
    let inc = k % 2 === 0 ? -Number.EPSILON : Number.EPSILON;
    let i;

    for (i = 0; i < 500; i++) {
      let x2 = start + inc;
      let x1 = -x2;
      let y1 = catenary(a, x1);
      let y2 = catenary(a, x2);
      const dist = getSpacingDistance(
        a,
        x1,
        y1,
        x2,
        y2,
        brickHeight,
        brickWidth,
      );
      if (k % 2 === 0 ? dist < brickSpacing : dist > brickSpacing) {
        break;
      }
      inc *= 2;
    }

    start += inc;
  }

  return start;
}

export function getAngleAt(a: number, x: number): number {
  const d = 0.001;
  const x1 = x - d;
  const x2 = x + d;
  const y1 = catenary(a, x1);
  const y2 = catenary(a, x2);
  return Math.atan2(y2 - y1, x2 - x1);
}

export function getUpsideDownAngle(a: number, x: number): number {
  return -getAngleAt(a, x) - Math.PI / 2;
}

export function getArchPositions(
  wantedWidth: number,
  wantedHeight: number,
  a: number,
  brickHeight: number,
  brickSpacing: number,
  brickWidth: number,
  useMiddleBrick: boolean,
) {
  let increment;
  const ret = [];

  if (useMiddleBrick) {
    for (let x = 0; x <= wantedWidth / 2; x += increment) {
      const y = -catenary(a, x) + wantedHeight;
      const r = getUpsideDownAngle(a, x);
      increment = getIncrement(a, x, brickHeight, brickWidth, brickSpacing);

      ret.push({ x, y, r });
      if (x !== 0) {
        ret.unshift({ x: -x, y, r: -r });
      }
    }
  } else {
    const start = getNoMiddleBrickStart(
      a,
      brickHeight,
      brickWidth,
      brickSpacing,
    );
    for (let x = start; x <= wantedWidth / 2; x += increment) {
      const y = -catenary(a, x) + wantedHeight;
      const r = getUpsideDownAngle(a, x);
      increment = getIncrement(a, x, brickHeight, brickWidth, brickSpacing);

      ret.push({ x, y, r });
      ret.unshift({ x: -x, y, r: -r });
    }
  }

  return ret;
}
