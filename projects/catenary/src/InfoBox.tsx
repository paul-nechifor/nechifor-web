import { Info } from "./types";

interface InfoBoxProps {
  info: Info;
  width: number;
}

export default function InfoBox({ info, width }: InfoBoxProps) {
  return (
    <div style={{ minWidth: width, fontSize: "1rem" }}>
      {Object.entries(info).map(([label, value]) => (
        <p key={label}>
          {label}: <b>{value}</b>
        </p>
      ))}
    </div>
  );
}
