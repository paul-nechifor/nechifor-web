const async = require("async");
const fs = require("fs");
const _ = require("lodash");
const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const mkdirp = require("mkdirp");
const path = require("path");
const pug = require("gulp-pug");
const sharp = require("sharp");
const webserver = require("gulp-webserver");
const yaml = require("js-yaml");
const { execSync } = require("child_process");

const projectsDir = path.join(__dirname, "projects");
const upperProjectsDir = path.join(__dirname, "..");
const overrideScreenshots = path.join(__dirname, "override-screenshots");
const screenshotDir = path.join(__dirname, "dist", "screenshots");
const screenshots = {};
let info = null;
const currentYear = new Date().getFullYear();

gulp.task("default", ["html", "webserver", "watch"]);

gulp.task("build", ["html"]);

gulp.task("projects", (done) => {
  info = loadInfo();
  copyScreenshots((err) => {
    if (err) {
      return done(err);
    }

    for (const p of info) {
      if (!(!!p.gitUrl ^ !!p.local)) {
        return done("Either gitUrl or local for " + p.code);
      }
      if (p.gitUrl) {
        getOrUpdate(p.gitUrl, p.skipClone);
      }
    }
    done();
  });
});

gulp.task("html", ["projects"], () => {
  let k = 0;
  const years = _.sortBy(_.toPairs(_.groupBy(info, "date")), (x) => -x[0]).map(
    ([year, projects]) => ({
      year,
      projects: projects.map((project) => ({
        ...project,
        cummulativeIndex: k++,
      })),
    }),
  );
  return gulp
    .src("index.pug")
    .pipe(pug({ locals: { years, screenshots, currentYear } }))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("dist"));
});

gulp.task("webserver", () => {
  const port = parseInt(process.env.port || "8080", 10);
  return gulp
    .src("dist")
    .pipe(webserver({ livereload: true, open: true, port, host: "0.0.0.0" }));
});

gulp.task("watch", () => {
  return gulp.watch(["index.pug"], ["html"]);
});

function getOrUpdate(url, skipClone) {
  const parts = url.split("/");
  const name = parts[parts.length - 1];
  const dir = path.join(projectsDir, name);

  if (skipClone) {
    console.log("Skipping ‘%s’.", name);
    execSync(`mkdir -p '${dir}'`);
  } else {
    console.log("Getting ‘%s’.", name);
  }

  if (fs.existsSync(dir)) {
    execSync(`cd '${dir}'; git pull`);
  } else {
    execSync(`git clone '${url}' '${dir}'`);
  }
}

function loadInfo() {
  mkdirp.sync(projectsDir, { mode: 0o755 });
  const infoFile = path.join(__dirname, "info.yaml");
  const yamlData = fs.readFileSync(infoFile, "utf8");
  return yaml.safeLoad(yamlData);
}

function copyScreenshots(cb) {
  mkdirp.sync(screenshotDir, { mode: 0o755 });
  async.map(info, copyScreenshot, cb);
}

function copyScreenshot(project, cb) {
  const id = project.code;
  const find = findScreenshot(project);
  screenshots[id] = {};
  if (!find) {
    return cb();
  }
  const [srcPath, format] = Array.from(find);
  const x1 = `screenshots/${id}.thumb.${format}`;
  const x2 = `screenshots/${id}.${format}`;
  screenshots[id] = {
    src: x1,
    srcset: `${x1}, ${x2} 2x`,
  };
  const dstPath = path.join(screenshotDir, `${id}.${format}`);
  const dstThumbPath = path.join(screenshotDir, `${id}.thumb.${format}`);

  fs.createReadStream(srcPath)
    .pipe(fs.createWriteStream(dstPath))
    .on("close", (err) => {
      if (err) {
        return cb(err);
      }
      let img = sharp(srcPath)
        .resize(740 / 2, 360 / 2)
        .sharpen();
      if (srcPath.endsWith(".png")) {
        img = img.png();
      } else {
        img = img.jpeg({ quality: 92, optimizeScans: true });
      }
      img
        .toFile(dstThumbPath)
        .then(() => cb())
        .catch((err) => cb(err));
    });
}

function findScreenshot(project) {
  const parentDir = project.local ? upperProjectsDir : projectsDir;
  for (const format of ["png", "jpg"]) {
    const overridePath = path.join(
      overrideScreenshots,
      `${project.code}.${format}`,
    );
    if (fs.existsSync(overridePath)) {
      return [overridePath, format];
    }
    const imagePath = path.join(
      parentDir,
      project.code,
      `screenshot.${format}`,
    );
    if (fs.existsSync(imagePath)) {
      return [imagePath, format];
    }
  }
  return [path.join(__dirname, "default-screenshot.png"), "png"];
}
