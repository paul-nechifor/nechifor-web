{NodeWrapper} = require 'chiplib'
songs = require '../tunes/data.json'

wrapper = null

main = ->
  loopIt = ->
    playRandomSong (err) ->
      console.log err if err
      setTimeout loopIt

  $('.play').on 'click', ->
    $('.play').hide();
    $('.next').show();
    $('.pause').show();
    loopIt()

  $('.next').on 'click', ->
    return if not wrapper
    wrapper.stop()
    wrapper = null

  $('.pause').on 'click', ->
    return if not wrapper
    if wrapper.paused
      wrapper.unpause()
      $('.pause').text('Pause')
    else
      wrapper.pause()
      $('.pause').text('Play')
    return

playRandomSong = (cb) ->
  index = (Math.random() * songs.length) | 0
  song = songs[index]
  $('.song-name').text "#{song[0]} - #{song[1]}"

  loadTune 'tunes/' + index, (err, tune) ->
    return cb "Error on loading song #{index}." if err
    context = new AudioContext
    wrapper = new NodeWrapper context, tune
    window.w = wrapper
    wrapper.start (err) ->
      return cb "Error on finishing song #{index}." if err
      cb()

loadTune = (path, cb) ->
  xhr = new XMLHttpRequest
  xhr.open 'GET', path, true
  xhr.responseType = 'arraybuffer'

  returned = false
  cb2 = (err, data) ->
    return if returned
    cb err, data

  xhr.onload = (e) ->
    return cb2 'loading error' unless xhr.status is 200 and e.total
    cb2 null, xhr.response

  xhr.onerror = xhr.onabort = ->
    cb2 'loading error'

  xhr.send()

$ main
