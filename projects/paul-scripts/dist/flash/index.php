<?php
require '../include/antet.php';
afiseazaAntet('Flash (Paul Scripts)', 'Programe si jocuri in Flash ActionScript create de Paul Nechiofr cum ar fi Gravitatie, Paddle', 'Paul Scripts, Paul Nechifor, Flash, ActionScript, jocuri, Gravitatie, Paddle');
?>

<div id="titlu">
	<h2 id="flash_titlu">Flash</h2>
</div>
<div id="scris">

<p>Prima versiune de Flash pe care am folosit-o este Flash MX. &Icirc;ns&#259; pe atunci nici nu &#351;tiam ce este un tween. Abia &icirc;n 2006 am &icirc;nceput s&#259; &icirc;nv&#259;&#355; Flash. Mai t&acirc;rziu am printat manualul oficial <em>&quot;Learning ActionScript 2.0 in Flash&quot;</em> ca s&#259; &icirc;nv&#259;&#355; ActionScript 2.</p>

<ul class="lista_sc">
	<li><a href="/college-website/proiecte/gravitatie/">Gravita&#355;ie</a> (simulator de gravita&#355;ie, la care po&#355;i adauga planete)</li>
	<li><a href="/college-website/proiecte/buggy/paddle/">Paddle</a> (un joc <em>ball-and-paddle</em> pe care l-am numit a&#351;a din lips&#259; de inspira&#355;ie)</li>
</ul>
</div>
<?php
require '../include/footer.php';
afiseazaFooter();
?>
