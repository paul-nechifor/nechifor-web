<?php
require 'include/antet.php';
afiseazaAntet('Despre sait (Paul Scripts)', '', '');
?>

<div id="titlu">
	<h2 id="despre_sait_titlu">Despre sait</h2>
</div>
<div id="scris">

<p>Am f&#259;cut saitul acesta, Paul Scripts, ca s&#259; pun pe internet ce fac eu pe acas&#259;. De c&acirc;teva luni am tot facut scripturi (prima dat&#259; PHP) care le l&#259;sam s&#259; stea pe calculator pe degeaba. Cele mai multe scripturi le-am f&#259;cut din curiozitate (chiar se pot citi cuvintele daca sunt &icirc;nlocuite literele din mijloc?, c&acirc;te nume de familie diferite sunt &icirc;n Rom&acirc;nia?), dar &#351;i ca s&#259;-mi testez cuno&#351;tin&#355;ele &#351;i s&#259; &icirc;nv&#259;&#355; ceva nou (jocul Paddle, pentru care n-am g&#259;sit un nume mai bun).</p>
<p>Nu sunt numai scripturi &icirc;ns&#259;, am pus imagini f&#259;cute de mine &#351;i programe adev&#259;rate. Denumirea saitului am ales-o-n grab&#259;.</p>
<p>Recent lucrez mai mult cu ActionScript &#351;i Python.</p>
	<h3>Cum am f&#259;cut saitul?</h3>
<p>Am f&#259;cut mai multe saituri p&acirc;n&#259; acum, dar nu au prea avut succes. &Icirc;nainte s&#259; &#351;tiu CSS f&#259;ceam (ca mul&#355;i al&#355;i) o sut&#259; de tabele &#351;i imagini t&#259;iate ca s&#259; se potriveasc&#259;. M-am obijnuit acum s&#259; folosesc numai CSS pentru aranjarea pagini. Asta este chiar metoda recomandat&#259; de <a href="http://en.wikipedia.org/wiki/W3C">W3C</a>. Problema este c&#259; nu arat&#259; bine &icirc;n Internet Explorer 6, browser-ul care este folosit de majoritatea oamenilor (chiar dac&#259; a aparut IE7). Navigatorul &#259;sta nu poate poate afi&#351;a PNG-uri cu transparen&#355;&#259; (cum sunt imaginile de la meniu din dreapta). Dac&#259; vrei s&#259; vezi pagina asta bine trebuie s&#259; folose&#351;ti Mozilla Firefox, Opera sau alte navigatoare care afi&#351;eaz&#259; corect pagini cu CSS. </p>
<p>&Icirc;n antet am folosit fontul <em>Ruritania</em> &#351;i &icirc;n meniu &#351;i titlurile paginilor principale  <em>Zapfino</em>. Imaginea din antet este o frunz&#259; uscat&#259;. Pe fundal este o pentagram&#259; invers&#259; pu&#355;in &icirc;nclinat&#259;.</p>
<p>Dac&#259; ai &icirc;ntreb&#259;ri sau vrei s&#259;-&#355;i spui parerea despre sait &#351;i programele, imaginile, etc. de pe el vezi pagina <a href="/paul-scripts/pareri/">P&#259;reri</a>. </p>

</div>
<?php
require 'include/footer.php';
afiseazaFooter();
?>
