const SIZE_MAX = 20;
const SIZE_MIN = 1;

const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
const url = `${location.protocol.replace("http", "ws")}//${
  location.hostname
}/graffiti/websocket/`;
// const url = 'ws://localhost:7000';
const ws = new WebSocket(url);

let users = {};
let mouseIsDown = false;
let myId;

function send(type, message) {
  ws.send(JSON.stringify([type, message]));
}

function onMouseMove(e) {
  if (!mouseIsDown) {
    return;
  }

  send("l", {
    x: e.pageX - canvas.offsetLeft,
    y: e.pageY - canvas.offsetTop
  });
}

function onMouseDown(e) {
  mouseIsDown = true;
  send("s", {
    x: e.pageX - canvas.offsetLeft,
    y: e.pageY - canvas.offsetTop
  });
}

function onMouseUp(e) {
  mouseIsDown = false;
}

function onWheel(e) {
  e.preventDefault();
  const delta = event.deltaY > 0 ? -1 : 1;
  const { brush } = users[myId];
  const size = Math.max(SIZE_MIN, Math.min(SIZE_MAX, brush.size + delta));
  if (size !== brush.size) {
    send("brushChange", { ...brush, size });
  }
}

const handlers = {
  state({ yourId, users: initialUsers }) {
    myId = yourId;
    users = initialUsers;
  },

  brushChange({ u, brush }) {
    users[u].brush = brush;
  },

  l({ u, x, y }) {
    const user = users[u];
    ctx.beginPath();
    ctx.moveTo(user.x, user.y);
    ctx.lineTo(x, y);
    ctx.strokeStyle = user.brush.color;
    ctx.lineWidth = user.brush.size;
    ctx.lineCap = "round";
    ctx.stroke();

    user.x = x;
    user.y = y;
  },

  s({ u, x, y }) {
    users[u].x = x;
    users[u].y = y;
  }
};

ws.addEventListener("open", event => {
  canvas.addEventListener("mousemove", onMouseMove);
  canvas.addEventListener("mousedown", onMouseDown);
  canvas.addEventListener("mouseup", onMouseUp);
  canvas.addEventListener("wheel", onWheel);
  document.querySelector(".palette").addEventListener("click", function(e) {
    const { brush } = users[myId];
    send("brushChange", { ...brush, color: e.target.dataset.color });
  });
});

ws.addEventListener("message", event => {
  const [type, data] = JSON.parse(event.data);
  if (typeof data === "object" && "u" in data && !(data.u in users)) {
    users[data.u] = { brush: { size: 4, color: "#000" } };
  }
  handlers[type]?.(data);
});
