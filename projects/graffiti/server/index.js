const express = require("express");
const ws = require("ws");
const _ = require("lodash");

const app = express();
const users = {};
const defaultBrush = { size: 4, color: "#000" };

const wsServer = new ws.Server({ noServer: true });

function log(data) {
  console.log(
    JSON.stringify({
      source: "graffiti",
      time: new Date().toISOString(),
      ...data
    })
  );
}

function bootPlayer(userId, goodState) {
  if (!goodState) {
    log({ msg: "Booting player", userId });
  }
  users[userId]?.socket.close();
  delete users[userId];
}

function broadcast(type, message) {
  const data = JSON.stringify([type, message]);
  for (const user of Object.values(users)) {
    user.socket.send(data);
  }
}

function sendUser(userId, type, message) {
  const data = JSON.stringify([type, message]);
  users[userId].socket.send(data);
}

function onUserMessage(userId, message) {
  let data;

  try {
    data = JSON.parse(message);
  } catch (e) {
    log({ msg: "Error parsing JSON.", userId });
    bootPlayer(userId);
    return;
  }

  if (!(data instanceof Array)) {
    log({ msg: "Not array message.", userId });
    bootPlayer(userId);
    return;
  }

  const handler = handlers[data[0]];
  if (typeof handler !== "function") {
    log({ msg: "Cannot handle message", data: data[0] });
    bootPlayer(userId);
    return;
  }

  handler(userId, data[1]);
}

function onBrushChange(userId, { size, color }) {
  const brush = {
    size: _.isNumber(size) ? size : defaultBrush.size,
    color: isColor(color) ? color : defaultBrush.color
  };
  users[userId].brush = brush;
  broadcast("brushChange", { u: userId, brush });
}

function onLine(userId, { x, y }) {
  if (!(_.isNumber(x) && _.isNumber(y))) {
    bootPlayer(id);
    return;
  }
  broadcast("l", { u: userId, x, y });
}

function onLineStart(userId, { x, y }) {
  if (!(_.isNumber(x) && _.isNumber(y))) {
    bootPlayer(id);
    return;
  }
  broadcast("s", { u: userId, x, y });
}

function isColor(string) {
  return _.isString(string) && /^#[0-9a-f]{3}([0-9a-f]{3})?$/i.test(string);
}

const handlers = {
  brushChange: onBrushChange,
  l: onLine,
  s: onLineStart
};

wsServer.on("connection", socket => {
  const id = _.uniqueId();

  users[id] = { id, socket, brush: { ...defaultBrush } };

  socket.on("message", message => {
    onUserMessage(id, message);
  });

  socket.on("error", () => {
    bootPlayer(id);
  });

  socket.on("close", () => {
    bootPlayer(id, true);
  });

  sendUser(id, "state", {
    yourId: id,
    users: _.mapValues(users, user => ({
      brush: user.brush
    }))
  });
});

const server = app.listen(7000);
server.on("upgrade", (request, socket, head) => {
  wsServer.handleUpgrade(request, socket, head, socket => {
    wsServer.emit("connection", socket, request);
  });
});
