import { useState, useEffect, useRef } from "react";
import World from "./World";

export default function Viewer() {
  const ref = useRef<HTMLDivElement>(null);
  const [showLoading, setShowLoading] = useState(true);
  const worldRef = useRef<World | null>(null);

  useEffect(() => {
    function handleResize() {
      if (worldRef.current) {
        worldRef.current.updateSize(window.innerWidth, window.innerHeight);
      }
    }

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    const div = ref.current;
    if (div) {
      const world = new World(window.innerWidth, window.innerHeight, () => {
        setShowLoading(false);
      });
      worldRef.current = world;
      world.start(div);
    }

    return () => {
      if (worldRef.current) {
        worldRef.current.stop();
      }
      if (div) {
        div.innerHTML = "";
      }
    };
  }, []);

  useEffect(() => {
    if (worldRef.current) {
      worldRef.current.triggerRender();
    }
  }, []);

  return (
    <div>
      <div ref={ref}></div>
      {showLoading && (
        <p
          style={{
            position: "fixed",
            top: "50%",
            left: 0,
            textAlign: "center",
            width: "100%",
            fontSize: "2rem",
          }}
        >
          Se încarcă...
        </p>
      )}
    </div>
  );
}
