import {
  Color,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  Clock,
  Mesh,
} from "three";

import * as THREE from "three";
import CameraControls from "camera-controls";

CameraControls.install({ THREE: THREE });

export default class World {
  scene: Scene;

  camera: PerspectiveCamera;

  renderer: WebGLRenderer;

  cameraControls: CameraControls;

  clock: Clock;

  shouldStop: boolean = false;

  man: Mesh | null = null;

  constructor(width: number, height: number, onLoad: () => void) {
    this.scene = new Scene();
    this.scene.background = new Color(0x444f4f);

    const fov = 35;
    const aspect = width / height;
    const near = 0.3;
    const far = 1000;
    this.camera = new PerspectiveCamera(fov, aspect, near, far);
    this.camera.position.set(0, 60, 100);
    this.camera.rotateZ(Math.PI * 0.2);

    this.renderer = new WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    // this.scene.add(new THREE.AmbientLight(0xffffff, 0.5));
    const light = new THREE.DirectionalLight(0xffffff, 2);
    light.position.set(10, 20, 10);
    light.castShadow = true;

    // Shadow properties
    light.shadow.mapSize.width = 2048; // Higher resolution for better quality
    light.shadow.mapSize.height = 2048;
    light.shadow.camera.near = 0.1;
    light.shadow.camera.far = 100;
    light.shadow.camera.left = -50;
    light.shadow.camera.right = 50;
    light.shadow.camera.top = 50;
    light.shadow.camera.bottom = -50;

    this.scene.add(light);

    this.triggerRender();

    this.cameraControls = new CameraControls(
      this.camera,
      this.renderer.domElement,
    );

    this.clock = new Clock();

    const textureLoader = new THREE.TextureLoader();
    const terrainTexture = textureLoader.load("texture.jpg", () => {
      this.triggerRender();
      onLoad();
    });
    const maskTexture = textureLoader.load("mask.png", () => {
      this.triggerRender();
    });

    const imageLoader = new THREE.ImageLoader();
    imageLoader.load("altitude.png", (image) => {
      const canvas = document.createElement("canvas");
      canvas.width = image.width;
      canvas.height = image.height;

      const ctx = canvas.getContext("2d");
      if (!ctx) {
        throw new Error("No context");
      }
      ctx.drawImage(image, 0, 0);

      const imageData = ctx.getImageData(
        0,
        0,
        canvas.width,
        canvas.height,
      ).data;

      // Create geometry
      const widthSegments = canvas.width - 1;
      const heightSegments = canvas.height - 1;
      const geometry = new THREE.PlaneGeometry(
        100,
        (canvas.height / canvas.width) * 100,
        widthSegments,
        heightSegments,
      );
      const position = geometry.attributes.position;

      // Modify vertices using heightmap data
      for (let i = 0; i < position.count; i++) {
        const x = i % canvas.width;
        const y = Math.floor(i / canvas.width);
        const stride = (y * canvas.width + x) * 4; // RGBA
        let height = imageData[stride] / 255; // Normalized
        if (height > 20) {
          height = 0;
        }
        position.setZ(i, height * 4);
      }

      position.needsUpdate = true;

      // Apply texture
      const material = new THREE.MeshStandardMaterial({
        map: terrainTexture,
        alphaMap: maskTexture,
        roughness: 1,
        metalness: 0,
        transparent: true,
      });
      const terrain = new THREE.Mesh(geometry, material);

      terrain.rotation.x = -Math.PI / 2; // Rotate to lay flat
      // terrain.castShadow = false; // Terrain usually doesn’t cast shadows on itself
      terrain.receiveShadow = true;
      this.scene.add(terrain);
      this.triggerRender();
    });
  }

  start(div: HTMLDivElement) {
    div.append(this.renderer.domElement);

    const anim = () => {
      if (this.shouldStop) {
        return;
      }
      const delta = this.clock.getDelta();
      const hasControlsUpdated = this.cameraControls.update(delta);
      requestAnimationFrame(anim);
      if (hasControlsUpdated) {
        this.triggerRender();
      }
    };

    anim();
  }

  stop() {
    this.shouldStop = true;
  }

  updateSize(width: number, height: number) {
    this.renderer.setSize(width, height, true);
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.triggerRender();
  }

  triggerRender() {
    this.renderer.render(this.scene, this.camera);
  }
}
