# Circuits

Generate seamless circuit board tiles. Uses my [circuit-boards][cb] package. You
can [play with the settings][site] on my web site.

![circuits screenshot](screenshot.png)

## Usage

Install requirements:

    sudo apt-get install libjpeg-dev -y

Start the server and watch for changes:

    yarn start

[cb]: https://gitlab.com/paul-nechifor/circuit-boards
[site]: http://nechifor.net/circuits
