const axios = require("axios");
const cheerio = require("cheerio");
const { promises: fs } = require("fs");
const path = require("path");
const { minify } = require('html-minifier');

async function main() {
  const requirements = new Set();
  const dump = await axios.get("http://localhost/dump");
  await fs.writeFile("dist/dump.json", JSON.stringify(dump.data));

  const games = await axios.get("http://localhost/games");
  const pages = [];

  await loadAndWrite(requirements, games.data, "dist/index.html", ($) => {
    $('a').map((i, a) => {
      const code = path.basename($(a).attr('href'));
      pages.push(code);
      $(a).attr("href", `${code}.html`);
    });

    $('h1').text('Aoire');
    $('h1').after(`
      <p>Aoire is a server for playing games with bots. The <a href="https://gitlab.com/paul-nechifor/aoire">source code is on GitLab</a>.</p>
      <p>This is the archive of the Gomoku games our bots played. You can also download the <a href="dump.json">JSON dump</a>.</p>

      <p>The public bots are:</p>

      <ul>
        <li><a href="https://github.com/Greatlemer/react-gomoku-bot">Greatlemer/react-gomoku-bot</a></li>
        <li><a href="https://github.com/dprgarner/balthazar">dprgarner/balthazar</a></li>
        <li><a href="https://gitlab.com/paul-nechifor/coaie">paul-nechifor/coaie</a></li>
        <li><a href="https://github.com/dprgarner/oapy">dprgarner/oapy</a></li>
      </ul>

      <h2>List of the games</h2>
    `);
  });

  await Promise.all(pages.map(async page => {
    const data = await axios.get("http://localhost/game/" + page);
    await loadAndWrite(requirements, data.data, `dist/${page}.html`, ($) => {
      $('h1').after('<p><a href="./">See the other games.</a></p>')
    });
  }));

  await Promise.all(Array.from(requirements).map(async url => {
    let data = (await axios.get(url)).data;
    if (url.indexOf("lumen") >= 0) {
      data = data.replace(/@import url\([^)]+\);/, '');
    }

    await fs.writeFile('dist/' + getNameForUrl(url), data);
  }));
}

async function loadAndWrite(requirements, data, outFile, evaluate) {
  const $ = cheerio.load(data);

  $('link[href]').each((i, el) => {
    const href = $(el).attr('href')
    requirements.add(href);
    $(el).attr('href', getNameForUrl(href));
  });

  $('script[src]').each((i, el) => {
    const href = $(el).attr('src')
    requirements.add(href);
    $(el).attr('src', getNameForUrl(href));
  });

  const ret = evaluate($);

  $('.col-md-12').append("<p style='text-align: center'>Created by <a href='/'>Paul Nechifor</a>. See other <a href='/projects/'>projects</a>.</p>")

  await fs.writeFile(outFile, minify($.html()));

  return ret;
}

function getNameForUrl(url) {
  let name = path.basename(url);
  if (url.indexOf('lumen') >= 0) {
    name = name.replace('bootstrap', 'bootstrap-lumen');
  }
  return name;
}

main()
