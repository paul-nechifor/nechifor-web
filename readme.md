# Nechifor Web

## Usage

You need to have Docker installed.

Rebuild all the projects:

    ./scripts/build projects

Build the image:

    ./scripts/build image

Start the latest built image with:

    ./scripts/build run

Export the image to `images/`:

    ./scripts/build export

Start a Vagrant VM:

    (cd vagrant; vagrant up)

Release the exported image number `$n` to the vagrant remote:

    ./scripts/remote release $n

All `./scripts/remote` commands take `--prod` to use production instead of
Vagrant.

Get the logs from the remote (saved in `logs/`):

    ./scripts/remote get-logs

## Sites left to add.

- multilatex
- negura-server
- thunder tactics
- cv
- 1930
- tinutok
- space-hoarder os package

## TODO

- Add memory limits to supervizord and restart if too much.

- Set the with/height on the webpage map img

- Add site to some directories. See
  https://news.ycombinator.com/item?id=31999259

- Eliminate all CDNs. I just witnesed a Bootstrap CDN take 6.72 s to load.
  That's absurd. Include Bootstrap and jQuery for now.

- Add a blog page about movie histograms with the small package I created. Show
  it for some popular films.

- Add a page about my historical screenshots.

- Configure Nginx better, including file expiration times.

- See some example docker compose files:
  https://news.ycombinator.com/item?id=34940920

## License

AGPL
